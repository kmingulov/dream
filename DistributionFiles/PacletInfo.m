Paclet[
	Name -> "DREAM",
	Version -> "2.0",
	MathematicaVersion -> "10+",
	Description -> "DREAM is a package performing high-precision calculation of multiple sums with factorized summand, arising in DRA method, a method of multiloop calculations.",
    Creator -> "Kirill T. Mingulov, Roman N. Lee",
	Extensions -> {
		{
			"Kernel",
			"Context" -> {"DREAM`"}
		},
		{
			"Documentation",
			Language -> "English",
			LinkBase -> "DREAM"
		}
	}
]
