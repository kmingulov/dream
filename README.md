# DREAM

**DREAM** is a *Wolfram Mathematica* package performing the high-precision calculation of the multiple sums with factorized summand.
**DREAM** stands for **D**imensional **RE**currence & **A**nalyticity **M**ethod.

**DREAM** is a free software and is distributed under the terms of GNU GPL v2.

*Official repository:* [bitbucket.org/kmingulov/dream/](https://bitbucket.org/kmingulov/dream/)

*Version:* 2.0r (23 September 2018)

*Authors:* Kirill T. Mingulov & Roman N. Lee,
Budker Institute of Nuclear Physics, Novosibirsk

***

## Requirements

**DREAM** requires:

* Wolfram Mathematica (version 10 or higher)
* [Objects package](https://bitbucket.org/kmingulov/objects/downloads)
* [GMP library](https://gmplib.org/) and GNU OpenMP (optional --- for better performance in case of convergence acceleration) 

***

## Installation
There are two ways to install **DREAM**.

### 1. Automatic online installation
You can install **DREAM** using our installation script. Type in *Mathematica* session the following command:

```mathematica
Import["https://bitbucket.org/kmingulov/dream/raw/master/Install.m"];
```

It will download the latest release of **DREAM** and install it. After that, you can load the package by the following command:

```mathematica
<<DREAM`
```

### 2. Manual installation
Download the archive with the latest version of the package from the [downloads page](https://bitbucket.org/kmingulov/dream/downloads) of our repository. Follow instructions in *INSTALLATION NOTES.txt* file from the archive.

Please note, that you will also need to install [**Objects**](https://bitbucket.org/kmingulov/objects/downloads), an object-oriented package, on which **DREAM** depends.

***

## Documentation
**DREAM** has the complete documentation, accesible via built-in Documentation Center.

***

## Bugs and feature requests
If you experience any problems with the package or have encountered a bug or just want something to be added into the package, please, leave an issue on our [issue tracker](https://bitbucket.org/kmingulov/dream/issues?status=new&status=open).
