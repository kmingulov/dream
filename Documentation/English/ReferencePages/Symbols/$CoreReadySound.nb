(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     24434,        820]
NotebookOptionsPosition[     19647,        665]
NotebookOutlinePosition[     21363,        712]
CellTagsIndexPosition[     21278,        707]
WindowTitle->$CoreReadySound
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["$CoreReadySound", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]",
     "is an option which controls a sound played after computation is \
complete."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this variable, you first need to load ",
 "the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is a numerical variable, meaning that a finish sound should be played, if \
and only if computation took more than \"",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " seconds\"."
}], "Notes",
 CellID->187944676],

Cell[TextData[{
 "If ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is to ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Infinity"]],"ref/Infinity"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", sound will be never played."
}], "Notes",
 CellID->1359938890],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is used only by ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["mEvaluate"]],"paclet:DREAM/ref/mEvaluate"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " method of ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TMasters"]],"paclet:DREAM/ref/TMasters"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 "."
}], "Notes",
 CellID->1858776299],

Cell[TextData[{
 "Sound, played by ",
 StyleBox["DREAM",
  FontWeight->"Bold"],
 ":"
}], "Notes",
 CellID->1933040847],

Cell[BoxData[
 InterpretationBox[
  GraphicsBox[{
    {GrayLevel[0.9], RectangleBox[{0, 0}, {360, -164}]}, {InsetBox[
      GraphicsBox[
       {Hue[-0.14074811787423214`, 0.9, 0.7], 
        StyleBox[{
          RectangleBox[{0., 0.43198696602798653`}, \
{0.95, 0.4438917279327484}], 
          RectangleBox[{0., 0.5153202993613198}, {0.95, 0.5272250612660818}]},
         
         Antialiasing->True]},
       AspectRatio->NCache[
         Rational[6, 35], 0.17142857142857143`],
       Background->GrayLevel[1],
       Frame->True,
       FrameStyle->GrayLevel[0.75],
       FrameTicks->False,
       ImagePadding->None,
       PlotRange->All,
       PlotRangePadding->{Automatic, 0.05}], {180, -35}, 
      ImageScaled[{0.5, 0.5}], {350, 60},
      Background->GrayLevel[0.9]], InsetBox[
      GraphicsBox[
       {GrayLevel[0.6], RectangleBox[{0, 0}]},
       AspectRatio->NCache[
         Rational[6, 35], 0.17142857142857143`],
       Background->GrayLevel[1],
       Frame->True,
       FrameStyle->GrayLevel[0.75],
       FrameTicks->False,
       ImagePadding->None], {180, -100}, ImageScaled[{0.5, 0.5}], {350, 60},
      Background->GrayLevel[0.9]]}, InsetBox[
     StyleBox[
      TemplateBox[{
       StyleBox[
        "\" | \"", FontSize -> 14, FontColor -> GrayLevel[0.75], StripOnInput -> 
         False],TemplateBox[{"1", "\" s\""}, "RowDefault"]},
       "RowWithSeparator"], "DialogStyle",
      StripOnInput->False], {355, -159}, Scaled[{1, 0}]], 
    TagBox[
     TooltipBox[
      TagBox[{
        {GrayLevel[0.9], RectangleBox[{5, -158}, {29, -135}]}, 
        {GrayLevel[0.3], 
         PolygonBox[
          NCache[{{13, -153}, {13, -140}, {21, Rational[-293, 2]}, {
            13, -153}}, {{13, -153}, {13, -140}, {21, -146.5}, {13, -153}}]]}, 
        {GrayLevel[0.5], 
         StyleBox[
          LineBox[{{5, -158}, {5, -135}, {29, -135}, {29, -158}, {5, -158}}],
          Antialiasing->False]}},
       EventHandlerTag[{"MouseClicked" :> (Sound`EmitMIDI[
            Sound`MIDISequence[{
              Sound`MIDITrack[{
                Sound`MIDIEvent[0, "SetTempo", "Tempo" -> 1000000], 
                Sound`MIDIEvent[
                0, "ProgramCommand", "Channel" -> 0, "Value" -> 6], 
                Sound`MIDIEvent[
                0, "NoteOn", "Note" -> 60, "Channel" -> 0, "Velocity" -> 127], 
                Sound`MIDIEvent[
                0, "NoteOn", "Note" -> 67, "Channel" -> 0, "Velocity" -> 127], 
                Sound`MIDIEvent[
                48, "NoteOff", "Note" -> 60, "Channel" -> 0, "Velocity" -> 0], 
                Sound`MIDIEvent[
                48, "NoteOff", "Note" -> 67, "Channel" -> 0, "Velocity" -> 
                 0]}]}, "DivisionType" -> "PPQ", "Resolution" -> 48]]; 
          Sound`EmitSampledSound[None, "Preemptive"]), Method -> "Preemptive",
          PassEventsDown -> Automatic, PassEventsUp -> True}]],
      DynamicBox[
       ToBoxes[
        FEPrivate`FrontEndResource["FEStrings", "playText"], StandardForm]]],
     Annotation[#, 
      Dynamic[
       FEPrivate`FrontEndResource["FEStrings", "playText"]], "Tooltip"]& ], 
    TagBox[
     TooltipBox[
      TagBox[{
        {GrayLevel[0.9], RectangleBox[{34, -158}, {58, -135}]}, 
        {GrayLevel[0.3], RectangleBox[{42, -150}, {50, -143}]}, 
        {GrayLevel[0.5], 
         StyleBox[
          LineBox[{{34, -158}, {34, -135}, {58, -135}, {58, -158}, {
           34, -158}}],
          Antialiasing->False]}},
       EventHandlerTag[{"MouseClicked" :> (Sound`EmitSampledSound[
            SampledSoundList[{{0.}}, 8000], "Preemptive"]; Sound`StopMIDI[]), 
         Method -> "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
         True}]],
      DynamicBox[
       ToBoxes[
        FEPrivate`FrontEndResource["FEStrings", "stopText"], StandardForm]]],
     Annotation[#, 
      Dynamic[
       FEPrivate`FrontEndResource["FEStrings", "stopText"]], "Tooltip"]& ], 
    {GrayLevel[0.75], 
     StyleBox[LineBox[{{0, 0}, {360, 0}, {360, -164}, {0, -164}, {0, 0}}],
      Antialiasing->False]}},
   ContentSelectable->False,
   ImageSize->250,
   PlotRange->{{0, 360}, {-164, 0}},
   PlotRangePadding->1],
  Sound[
   SoundNote[{"C", "G"}, 1, "Harpsichord"], SoundVolume -> 1]]], "Output",
 CellChangeTimes->{3.704604437365822*^9},
 CellID->669460120],

Cell[TextData[{
 "By default, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is set to ",
 StyleBox["180", "InlineFormula"],
 " seconds."
}], "Notes",
 CellID->1205110490],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "DREAM`"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create a simple master-integrals system with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CreateMasters"]],"paclet:DREAM/ref/CreateMasters"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->1068934427],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"CreateMasters", "[", 
   RowBox[{"\"\<J\>\"", ",", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {
         FractionBox[
          RowBox[{"\[Nu]", "+", "1"}], 
          RowBox[{"\[Nu]", "+", "2"}]], "0", "0"},
        {
         FractionBox["1", 
          SuperscriptBox["\[Nu]", "5"]], 
         FractionBox[
          RowBox[{"\[Nu]", "+", "2"}], 
          RowBox[{"\[Nu]", "+", "3"}]], "0"},
        {
         FractionBox["1", 
          SuperscriptBox["\[Nu]", "3"]], 
         FractionBox["1", 
          SuperscriptBox["\[Nu]", "4"]], 
         FractionBox[
          RowBox[{"\[Nu]", "+", "3"}], 
          RowBox[{"\[Nu]", "+", "4"}]]}
       },
       GridBoxAlignment->{
        "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
         "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]], ",", "\[Nu]"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.704604042577703*^9, 3.70460406388344*^9}},
 CellLabel->"In[2]:=",
 CellID->442668381],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Diagonal blocks: \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "1", "}"}], "\[InvisibleSpace]", "\<\", \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", "2", "}"}], "\[InvisibleSpace]", "\<\", \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", "3", "}"}], "\[InvisibleSpace]", "\<\".\"\>"}],
  SequenceForm["Diagonal blocks: ", {1}, ", ", {2}, ", ", {3}, "."],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->2090918465],

Cell[BoxData["\<\"Reducing diagonal blocks\[Ellipsis]\"\>"], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1405208383],

Cell[BoxData["\<\"Done!\"\>"], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1778664367],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "1", "}"}]}],
  SequenceForm["Block ", {1}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1657483064],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "2", "}"}]}],
  SequenceForm["Block ", {2}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->730831647],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J2\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J2", " \[Rule] ", $CellContext`J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->350239179],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "3", "}"}]}],
  SequenceForm["Block ", {3}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->690755793],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1406746295],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J2$J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J2$J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1707727965],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J2$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J2$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->707741468]
}, Open  ]]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"J", "[", 
   RowBox[{"mSetSolution", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"J1", "\[Rule]", 
       FractionBox["1", 
        RowBox[{"\[Nu]", "+", "1"}]]}], ",", 
      RowBox[{"J2", "\[Rule]", 
       FractionBox["1", 
        RowBox[{"\[Nu]", "+", "2"}]]}], ",", 
      RowBox[{"J3", "\[Rule]", 
       FractionBox["1", 
        RowBox[{"\[Nu]", "+", "3"}]]}]}], "}"}], ",", "\[Nu]"}], "]"}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.704604111303368*^9, 3.70460411909592*^9}},
 CellLabel->"In[3]:=",
 CellID->1452999868],

Cell["Evaluate master-integrals:", "ExampleText",
 CellID->1342459413],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"J", "[", 
    RowBox[{"mEvaluate", ",", "All", ",", 
     RowBox[{"2", "-", "\[Epsilon]"}], ",", "3", ",", "200"}], "]"}], ";"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.704603026514656*^9, 3.7046030314071712`*^9}, 
   3.704603080736597*^9, {3.704603683223569*^9, 3.7046036894513283`*^9}, 
   3.704603732897031*^9, {3.704604137099077*^9, 3.7046041623811274`*^9}},
 CellLabel->"In[4]:=",
 CellID->1381655011],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"7.712`", ",", "Null"}], "}"}]], "Output",
 CellLabel->"Out[4]=",
 CellID->394590332]
}, Open  ]],

Cell[TextData[{
 "They were evaluated too fast. Set ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " to a lower value:"
}], "ExampleText",
 CellID->983327917],

Cell[BoxData[
 RowBox[{
  RowBox[{"$CoreReadySound", "=", "3"}], ";"}]], "Input",
 CellChangeTimes->{{3.7046030378633537`*^9, 3.704603038558799*^9}, {
  3.704604171761568*^9, 3.70460417480707*^9}},
 CellLabel->"In[5]:=",
 CellID->2117440160],

Cell["Now, sound will be played:", "ExampleText",
 CellID->2122595460],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"J", "[", 
    RowBox[{"mEvaluate", ",", "All", ",", 
     RowBox[{"2", "-", "\[Epsilon]"}], ",", "3", ",", "200"}], "]"}], ";"}], 
  "]"}]], "Input",
 CellLabel->"In[6]:=",
 CellID->356899190],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"7.652`", ",", "Null"}], "}"}]], "Output",
 CellLabel->"Out[6]=",
 CellID->1994592512]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"J", "[", "mPurge", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->27512931],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"J1$h\"\>", ",", "\<\"J2$h\"\>", ",", "\<\"J2$J1$adapter\"\>", 
   ",", "\<\"J2$J1$h\"\>", ",", "\<\"J3$h\"\>", ",", "\<\"J3$J1$adapter\"\>", 
   ",", "\<\"J3$J1$h\"\>", ",", "\<\"J3$J2$adapter\"\>", 
   ",", "\<\"J3$J2$J1$h\"\>", ",", "\<\"J3$J2$h\"\>", ",", "\<\"J1\"\>", 
   ",", "\<\"J2\"\>", ",", "\<\"J3\"\>", ",", "\<\"J\"\>"}], "}"}]], "Output",\

 CellLabel->"Out[7]=",
 CellID->1623079723]
}, Open  ]]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreDebug"]],"paclet:DREAM/ref/$CoreDebug"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreDeveloperTools"]],"paclet:DREAM/ref/$CoreDeveloperTools"},
   
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreOrderIncrease"]],"paclet:DREAM/ref/$CoreOrderIncrease"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreParallel"]],"paclet:DREAM/ref/$CoreParallel"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreProgressBar"]],"paclet:DREAM/ref/$CoreProgressBar"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"$CoreReadySound",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "$CoreReadySound is an option which controls a sound played after \
computation is complete.", "synonyms" -> {}, "title" -> "$CoreReadySound", 
    "type" -> "Symbol", "uri" -> "DREAM/ref/$CoreReadySound"}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[8881, 288, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 21134, 700}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[587, 21, 110, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[700, 26, 247, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[972, 40, 537, 16, 96, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1534, 60, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2274, 86, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2308, 88, 211, 8, 36, "Notes",
 CellID->718278630],
Cell[2522, 98, 530, 15, 84, "Notes",
 CellID->187944676],
Cell[3055, 115, 419, 15, 39, "Notes",
 CellID->1359938890],
Cell[3477, 132, 582, 20, 39, "Notes",
 CellID->1858776299],
Cell[4062, 154, 118, 6, 38, "Notes",
 CellID->1933040847],
Cell[4183, 162, 4310, 105, 136, "Output",
 CellID->669460120],
Cell[8496, 269, 302, 11, 39, "Notes",
 CellID->1205110490],
Cell[8801, 282, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8881, 288, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[9221, 305, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[9331, 309, 94, 3, 33, "Input",
 CellID->2007368699],
Cell[9428, 314, 278, 9, 43, "ExampleText",
 CellID->1068934427],
Cell[CellGroupData[{
Cell[9731, 327, 1367, 39, 125, "Input",
 CellID->442668381],
Cell[CellGroupData[{
Cell[11123, 370, 500, 11, 29, "Print",
 CellID->2090918465],
Cell[11626, 383, 134, 2, 29, "Print",
 CellID->1405208383],
Cell[11763, 387, 104, 2, 29, "Print",
 CellID->1778664367],
Cell[11870, 391, 239, 7, 29, "Print",
 CellID->1657483064],
Cell[12112, 400, 238, 7, 29, "Print",
 CellID->730831647],
Cell[12353, 409, 414, 9, 29, "Print",
 CellID->350239179],
Cell[12770, 420, 238, 7, 29, "Print",
 CellID->690755793],
Cell[13011, 429, 415, 9, 29, "Print",
 CellID->1406746295],
Cell[13429, 440, 421, 9, 29, "Print",
 CellID->1707727965],
Cell[13853, 451, 414, 9, 29, "Print",
 CellID->707741468]
}, Open  ]]
}, Open  ]],
Cell[14294, 464, 573, 18, 61, "Input",
 CellID->1452999868],
Cell[14870, 484, 70, 1, 40, "ExampleText",
 CellID->1342459413],
Cell[CellGroupData[{
Cell[14965, 489, 471, 11, 33, "Input",
 CellID->1381655011],
Cell[15439, 502, 124, 4, 42, "Output",
 CellID->394590332]
}, Open  ]],
Cell[15578, 509, 287, 9, 43, "ExampleText",
 CellID->983327917],
Cell[15868, 520, 241, 6, 33, "Input",
 CellID->2117440160],
Cell[16112, 528, 70, 1, 40, "ExampleText",
 CellID->2122595460],
Cell[CellGroupData[{
Cell[16207, 533, 254, 8, 33, "Input",
 CellID->356899190],
Cell[16464, 543, 125, 4, 42, "Output",
 CellID->1994592512]
}, Open  ]],
Cell[CellGroupData[{
Cell[16626, 552, 101, 3, 33, "Input",
 CellID->27512931],
Cell[16730, 557, 442, 9, 42, "Output",
 CellID->1623079723]
}, Open  ]]
}, Open  ]],
Cell[17199, 570, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[17288, 576, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[17556, 590, 1522, 44, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[19115, 639, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[19396, 653, 31, 0, 14, "SectionHeaderSpacer"],
Cell[19430, 655, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[19620, 663, 23, 0, 47, "FooterCell"]
}
]
*)

