(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6104,        227]
NotebookOptionsPosition[      3875,        162]
NotebookOutlinePosition[      5377,        202]
CellTagsIndexPosition[      5334,        199]
WindowTitle->mSendOrderRequests
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["mSendOrderRequests", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["mSendOrderRequests"]],
        "paclet:DREAM/ref/mSendOrderRequests"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]is a method of several ",
     StyleBox["DREAM",
      FontWeight->"Bold"],
     " classes."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this symbol, you first need to load ",
 "the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "All fields, defined in ",
 StyleBox["DREAM",
  FontWeight->"Bold"],
 ", start with letter ",
 StyleBox["f",
  FontSlant->"Italic"],
 ". All methods start with letter ",
 StyleBox["m",
  FontSlant->"Italic"],
 "."
}], "Notes",
 CellID->1013605324],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["mSendOrderRequests"]],"paclet:DREAM/ref/mSendOrderRequests"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is a method of ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " and all its derived classes."
}], "Notes",
 CellID->1036050656],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"mSendOrderRequests",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "mSendOrderRequests is a method of several DREAM classes.", 
    "synonyms" -> {}, "title" -> "mSendOrderRequests", "type" -> "Symbol", 
    "uri" -> "DREAM/ref/mSendOrderRequests"}, "SearchTextTranslated" -> "", 
  "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[590, 21, 110, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[703, 26, 250, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[978, 40, 559, 18, 98, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1562, 62, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2302, 88, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2336, 90, 209, 8, 36, "Notes",
 CellID->718278630],
Cell[2548, 100, 264, 12, 38, "Notes",
 CellID->1013605324],
Cell[2815, 114, 445, 14, 60, "Notes",
 CellID->1036050656],
Cell[3263, 130, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3343, 136, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[3624, 150, 31, 0, 14, "SectionHeaderSpacer"],
Cell[3658, 152, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[3848, 160, 23, 0, 47, "FooterCell"]
}
]
*)

