(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     23991,        877]
NotebookOptionsPosition[     18566,        708]
NotebookOutlinePosition[     20501,        761]
CellTagsIndexPosition[     20416,        756]
WindowTitle->TFunction
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["TFunction", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]is a derived class of ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     ", which evaluates using a pure function."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this class, you first need to load ",
 "the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "For its evaluation, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " uses a pure function, provided by the user."
}], "Notes",
 CellID->1242063151],

Cell[TextData[{
 "Function, used by ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", must have four arguments: value of ",
 Cell[BoxData["\[Nu]"], "InlineFormula"],
 ", regularization parameter ",
 Cell[BoxData["\[Epsilon]"], "InlineFormula"],
 ", expansion order ",
 Cell[BoxData["o"], "InlineFormula"],
 " and precision ",
 Cell[BoxData["p"], "InlineFormula"],
 "."
}], "Notes",
 CellID->466431252],

Cell[TextData[{
 "An instance of ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " does not depend on any other object."
}], "Notes",
 CellID->613535401],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Fields"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[TextData[{
 "This class derives all fields from ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ". Additionally to them, this class also has the following fields."
}], "ExampleText",
 CellID->131214019],

Cell[CellGroupData[{

Cell["fAsymN", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->821428554],

Cell[TextData[{
 StyleBox["Full name:",
  FontWeight->"Bold"],
 " ",
 StyleBox["DREAM`Hidden`fAsymN", "InlineFormula"]
}], "ExampleText",
 CellID->1946199998],

Cell[TextData[{
 StyleBox["Visibility:",
  FontWeight->"Bold"],
 " private"
}], "ExampleText",
 CellID->1732113239],

Cell[TextData[{
 StyleBox["Description:",
  FontWeight->"Bold"],
 "\nAsymptotics of the object on the negative infinity."
}], "ExampleText",
 CellID->1412733780]
}, Closed]],

Cell[CellGroupData[{

Cell["fAsymP", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->12047041],

Cell[TextData[{
 StyleBox["Full name:",
  FontWeight->"Bold"],
 " ",
 StyleBox["DREAM`Hidden`fAsymP", "InlineFormula"]
}], "ExampleText",
 CellID->1750488375],

Cell[TextData[{
 StyleBox["Visibility:",
  FontWeight->"Bold"],
 " private"
}], "ExampleText",
 CellID->296083430],

Cell[TextData[{
 StyleBox["Description:",
  FontWeight->"Bold"],
 "\nAsymptotics of the object on the positive infinity."
}], "ExampleText",
 CellID->1569435649]
}, Closed]],

Cell[CellGroupData[{

Cell["fFunction", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->776156793],

Cell[TextData[{
 StyleBox["Full name:",
  FontWeight->"Bold"],
 " ",
 StyleBox["DREAM`Hidden`fFunction", "InlineFormula"]
}], "ExampleText",
 CellID->662555149],

Cell[TextData[{
 StyleBox["Visibility:",
  FontWeight->"Bold"],
 " private"
}], "ExampleText",
 CellID->1530739515],

Cell[TextData[{
 StyleBox["Description:",
  FontWeight->"Bold"],
 "\nFunction, used for computation.\n\nIt must have four arguments: value of ",
 Cell[BoxData["\[Nu]"], "InlineFormula"],
 ", regularization parameter ",
 Cell[BoxData["\[Epsilon]"], "InlineFormula"],
 ", expansion order ",
 Cell[BoxData["o"], "InlineFormula"],
 " and precision ",
 Cell[BoxData["p"], "InlineFormula"],
 "."
}], "ExampleText",
 CellID->1950520128]
}, Closed]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Constructor"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->2035661198],

Cell[TextData[{
 StyleBox["Arguments:",
  FontWeight->"Bold"],
 "\n\t\[Bullet] ",
 Cell[BoxData["func"], "InlineFormula"],
 " \[LongDash] pure function of four arguments (value of ",
 Cell[BoxData["\[Nu]"], "InlineFormula"],
 ", regularization parameter ",
 Cell[BoxData["\[Epsilon]"], "InlineFormula"],
 ", expansion order ",
 Cell[BoxData["o"], "InlineFormula"],
 " and precision ",
 Cell[BoxData["p"], "InlineFormula"],
 ")\n\t\[Bullet] ",
 Cell[BoxData[
  RowBox[{"asymP", "\[Rule]", "Null"}]], "InlineFormula"],
 " \[LongDash] asymptotics of the function on the positive infinity\n\t\
\[Bullet] ",
 Cell[BoxData[
  RowBox[{"asymN", "\[Rule]", "Null"}]], "InlineFormula"],
 " \[LongDash] asymptotics of the function on the negative infinity"
}], "ExampleText",
 CellID->216586852],

Cell[TextData[{
 StyleBox["Description:",
  FontWeight->"Bold"],
 "\nCreates an instance of ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ".\n\nThe argument ",
 Cell[BoxData["func"], "InlineFormula"],
 " should be a pure function of four arguments: value of ",
 Cell[BoxData["\[Nu]"], "InlineFormula"],
 ", regularization parameter ",
 Cell[BoxData["\[Epsilon]"], "InlineFormula"],
 ", expansion order ",
 Cell[BoxData["o"], "InlineFormula"],
 " and precision ",
 Cell[BoxData["p"], "InlineFormula"],
 ".\n\nIf both arguments ",
 Cell[BoxData["asymP"], "InlineFormula"],
 " and ",
 Cell[BoxData["asymN"], "InlineFormula"],
 " are given, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " saves them as asymptotics on positive and negative infinity. Both ",
 Cell[BoxData["asymP"], "InlineFormula"],
 " and ",
 Cell[BoxData["asymN"], "InlineFormula"],
 " should be tuples ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{"q", ",", "\[Alpha]", ",", "\[Beta]"}], "}"}]], "InlineFormula"],
 " or lists of such tuples (see ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["FindAsymptotics"]],"paclet:DREAM/ref/FindAsymptotics"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " for more details on asymptotics notation).\n\nIf only one of ",
 Cell[BoxData["asymP"], "InlineFormula"],
 " and ",
 Cell[BoxData["asymN"], "InlineFormula"],
 " is given, then it is assumed as asymptotics on positive and negative \
infinity.\n\n",
 StyleBox["NOTE!",
  FontSlant->"Italic",
  FontColor->RGBColor[1, 0, 0]],
 " If both ",
 Cell[BoxData["asymP"], "InlineFormula"],
 " and ",
 Cell[BoxData["asymN"], "InlineFormula"],
 " are ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Null"]],"ref/Null"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", a created ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TFunction"]],"paclet:DREAM/ref/TFunction"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " object will not have any information about its asymptotics. It will be \
usable in computation, but (when summed by a ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TSum"]],"paclet:DREAM/ref/TSum"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " instance, for example) it will lead to precision loss or even incorrect \
results. Specify asymptotics during creation or set asymptotics after \
creation by ",
 Cell[BoxData["mSetAsymptotics"], "InlineFormula"],
 " method."
}], "ExampleText",
 CellID->1017986699],

Cell[TextData[{
 StyleBox["Errors:\n",
  FontWeight->"Bold"],
 "\t\[Bullet] ",
 Cell[BoxData[
  RowBox[{"TFunction", "::", "BadArgument"}]], "InlineFormula"],
 " \[LongDash] \n\t\twhen ",
 Cell[BoxData["func"], "InlineFormula"],
 " is not a pure function of four arguments;\n\t\twhen ",
 Cell[BoxData["asymP"], "InlineFormula"],
 " or ",
 Cell[BoxData["asymN"], "InlineFormula"],
 " is neither a tuple nor a list of tuples."
}], "ExampleText",
 CellID->248080402],

Cell["", "SectionFooterSpacer",
 CellID->216114652]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Methods"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->2049439106],

Cell[TextData[{
 "This class derives all methods from ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ". Additionally to them, this class also has the following methods."
}], "ExampleText",
 CellID->2031187357],

Cell[CellGroupData[{

Cell["mSetAsymptotics", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->247986176],

Cell[TextData[{
 StyleBox["Full name:",
  FontWeight->"Bold"],
 " ",
 StyleBox["DREAM`mSetAsymptotics", "InlineFormula"]
}], "ExampleText",
 CellID->1345187260],

Cell[TextData[{
 StyleBox["Visibility:",
  FontWeight->"Bold"],
 " public"
}], "ExampleText",
 CellID->445172630],

Cell[TextData[{
 StyleBox["Arguments:\n",
  FontWeight->"Bold"],
 "\t\[Bullet] ",
 StyleBox["type", "InlineFormula"],
 " \[LongDash] either ",
 Cell[BoxData["\"\<+\>\""], "InlineFormula",
  FormatType->"StandardForm"],
 " or ",
 Cell[BoxData["\"\<-\>\""], "InlineFormula",
  FormatType->"StandardForm"],
 " (meaning the positive or negative infinity)",
 StyleBox["\n",
  FontWeight->"Bold"],
 "\t\[Bullet] ",
 StyleBox["asymp", "InlineFormula"],
 " \[LongDash] asymptotics or ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Null"]],"ref/Null"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " "
}], "ExampleText",
 CellID->1455086665],

Cell[TextData[{
 StyleBox["Description:",
  FontWeight->"Bold"],
 "\nSets the asymptotics of the object on the specified infinity (either \
positive or negative).\n\nThe argument ",
 Cell[BoxData["asymp"], "InlineFormula",
  FormatType->"StandardForm"],
 " should be a tuple ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{"q", ",", "\[Alpha]", ",", "\[Beta]"}], "}"}]], "InlineFormula",
  FormatType->"StandardForm"],
 " or a list of such tuples. See ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["FindAsymptotics"]],"paclet:DREAM/ref/FindAsymptotics"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " for more details on asymptotics notation.\n\n",
 Cell[BoxData["asymp"], "InlineFormula",
  FormatType->"StandardForm"],
 " can be also ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Null"]],"ref/Null"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", meaning that no asymptotics is known."
}], "ExampleText",
 CellID->574073631],

Cell[TextData[{
 StyleBox["Errors:\n",
  FontWeight->"Bold"],
 "\t\[Bullet] ",
 Cell[BoxData[
  RowBox[{"THomo", "::", "BadHomogeneousSolution"}]], "InlineFormula"],
 " \[LongDash] when given ",
 Cell[BoxData["asymp"], "InlineFormula",
  FormatType->"StandardForm"],
 " is neither a tuple nor a list of tuples"
}], "ExampleText",
 CellID->1307756083]
}, Closed]],

Cell["", "SectionFooterSpacer",
 CellID->1641234787]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Errors and messages"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->842712355],

Cell[TextData[{
 "This class derives all messages from ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ". This class has no other messages."
}], "ExampleText",
 CellID->1979120877],

Cell["", "SectionFooterSpacer",
 CellID->2045614076]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TCert"]],"paclet:DREAM/ref/TCert"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TDummy"]],"paclet:DREAM/ref/TDummy"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TExpr"]],"paclet:DREAM/ref/TExpr"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["THomo"]],"paclet:DREAM/ref/THomo"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TRecurrence"]],"paclet:DREAM/ref/TRecurrence"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TShift"]],"paclet:DREAM/ref/TShift"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TSum"]],"paclet:DREAM/ref/TSum"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TTimes"]],"paclet:DREAM/ref/TTimes"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TUnity"]],"paclet:DREAM/ref/TUnity"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1920, 1007},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"TFunction",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "TFunction is a derived class of TComputable, which evaluates using a \
pure function.", "synonyms" -> {}, "title" -> "TFunction", "type" -> "Symbol",
     "uri" -> "DREAM/ref/TFunction"}, "SearchTextTranslated" -> "", 
  "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3824, 149, 313, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388],
  Cell[6567, 276, 318, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->2035661198],
  Cell[10953, 420, 314, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->2049439106]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 20031, 743}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[581, 21, 110, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[694, 26, 241, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[960, 40, 696, 20, 99, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1681, 64, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2421, 90, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2455, 92, 208, 8, 36, "Notes",
 CellID->718278630],
Cell[2666, 102, 282, 9, 39, "Notes",
 CellID->1242063151],
Cell[2951, 113, 518, 17, 39, "Notes",
 CellID->466431252],
Cell[3472, 132, 269, 9, 39, "Notes",
 CellID->613535401],
Cell[3744, 143, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3824, 149, 313, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[4140, 164, 327, 9, 43, "ExampleText",
 CellID->131214019],
Cell[CellGroupData[{
Cell[4492, 177, 99, 2, 33, "ExampleSection",
 CellID->821428554],
Cell[4594, 181, 158, 6, 42, "ExampleText",
 CellID->1946199998],
Cell[4755, 189, 115, 5, 42, "ExampleText",
 CellID->1732113239],
Cell[4873, 196, 161, 5, 69, "ExampleText",
 CellID->1412733780]
}, Closed]],
Cell[CellGroupData[{
Cell[5071, 206, 98, 2, 27, "ExampleSection",
 CellID->12047041],
Cell[5172, 210, 158, 6, 42, "ExampleText",
 CellID->1750488375],
Cell[5333, 218, 114, 5, 42, "ExampleText",
 CellID->296083430],
Cell[5450, 225, 161, 5, 69, "ExampleText",
 CellID->1569435649]
}, Closed]],
Cell[CellGroupData[{
Cell[5648, 235, 102, 2, 27, "ExampleSection",
 CellID->776156793],
Cell[5753, 239, 160, 6, 42, "ExampleText",
 CellID->662555149],
Cell[5916, 247, 115, 5, 42, "ExampleText",
 CellID->1530739515],
Cell[6034, 254, 429, 13, 124, "ExampleText",
 CellID->1950520128]
}, Closed]],
Cell[6478, 270, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[6567, 276, 318, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->2035661198],
Cell[6888, 291, 784, 22, 126, "ExampleText",
 CellID->216586852],
Cell[7675, 315, 2721, 81, 334, "ExampleText",
 CellID->1017986699],
Cell[10399, 398, 463, 14, 126, "ExampleText",
 CellID->248080402],
Cell[10865, 414, 51, 1, 29, "SectionFooterSpacer",
 CellID->216114652]
}, Open  ]],
Cell[CellGroupData[{
Cell[10953, 420, 314, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->2049439106],
Cell[11270, 435, 330, 9, 43, "ExampleText",
 CellID->2031187357],
Cell[CellGroupData[{
Cell[11625, 448, 108, 2, 33, "ExampleSection",
 CellID->247986176],
Cell[11736, 452, 160, 6, 42, "ExampleText",
 CellID->1345187260],
Cell[11899, 460, 113, 5, 42, "ExampleText",
 CellID->445172630],
Cell[12015, 467, 660, 24, 98, "ExampleText",
 CellID->1455086665],
Cell[12678, 493, 983, 29, 178, "ExampleText",
 CellID->574073631],
Cell[13664, 524, 350, 11, 70, "ExampleText",
 CellID->1307756083]
}, Closed]],
Cell[14029, 538, 52, 1, 29, "SectionFooterSpacer",
 CellID->1641234787]
}, Open  ]],
Cell[CellGroupData[{
Cell[14118, 544, 280, 12, 39, "SeeAlsoSection",
 CellID->842712355],
Cell[14401, 558, 300, 9, 43, "ExampleText",
 CellID->1979120877],
Cell[14704, 569, 52, 1, 29, "SectionFooterSpacer",
 CellID->2045614076]
}, Open  ]],
Cell[CellGroupData[{
Cell[14793, 575, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[15061, 589, 2936, 88, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[18034, 682, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[18315, 696, 31, 0, 14, "SectionHeaderSpacer"],
Cell[18349, 698, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[18539, 706, 23, 0, 47, "FooterCell"]
}
]
*)

