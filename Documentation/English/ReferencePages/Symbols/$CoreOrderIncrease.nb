(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26336,        863]
NotebookOptionsPosition[     21431,        703]
NotebookOutlinePosition[     23176,        751]
CellTagsIndexPosition[     23091,        746]
WindowTitle->$CoreOrderIncrease
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["$CoreOrderIncrease", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["$CoreOrderIncrease"]],
        "paclet:DREAM/ref/$CoreOrderIncrease"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]is an option which controls the behaviour of ",
     StyleBox["DREAM",
      FontWeight->"Bold"],
     " in case of order expansion loss."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this variable, you first need to load ",
 "the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "When computable objects have singularities for chosen value of dimensional \
variable ",
 Cell[BoxData["\[Nu]"], "InlineFormula"],
 ", order expansion loss can happen. In order to avoid it, ",
 StyleBox["DREAM",
  FontWeight->"Bold"],
 " utilizies order-requests system. When ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreOrderIncrease"]],"paclet:DREAM/ref/$CoreOrderIncrease"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is set to ",
 StyleBox["False", "InlineFormula"],
 ", this system is switched off."
}], "Notes",
 CellID->187944676],

Cell[TextData[{
 "By default, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreOrderIncrease"]],"paclet:DREAM/ref/$CoreOrderIncrease"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is set to ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["False"]],"ref/False"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", since the order-request system does not work well for recurrence \
relations systems with diagonal blocks."
}], "Notes",
 CellID->1205110490],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "DREAM`"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create a simple ",
 "master-integrals",
 " system with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CreateMasters"]],"paclet:DREAM/ref/CreateMasters"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->1068934427],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"CreateMasters", "[", 
    RowBox[{"\"\<J\>\"", ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {
          FractionBox["1", "2"], "0", "0"},
         {
          SuperscriptBox["5", 
           RowBox[{"-", "\[Nu]"}]], 
          FractionBox["1", "7"], "0"},
         {
          FractionBox[
           SuperscriptBox[
            RowBox[{"(", 
             FractionBox["2", "5"], ")"}], "\[Nu]"], 
           SuperscriptBox["\[Nu]", "3"]], 
          SuperscriptBox["7", 
           RowBox[{"-", "\[Nu]"}]], 
          FractionBox["1", "5"]}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[BoxForm`e$]]], ",", "\[Nu]"}], "]"}], ";"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.704510187694511*^9, 3.7045102202810183`*^9}},
 CellLabel->"In[2]:=",
 CellID->1262067781],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Diagonal blocks: \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "1", "}"}], "\[InvisibleSpace]", "\<\", \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", "2", "}"}], "\[InvisibleSpace]", "\<\", \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", "3", "}"}], "\[InvisibleSpace]", "\<\".\"\>"}],
  SequenceForm["Diagonal blocks: ", {1}, ", ", {2}, ", ", {3}, "."],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->371825490],

Cell[BoxData["\<\"Reducing diagonal blocks\[Ellipsis]\"\>"], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->532679420],

Cell[BoxData["\<\"Done!\"\>"], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->917810082],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "1", "}"}]}],
  SequenceForm["Block ", {1}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->2040750494],

Cell[BoxData[
 RowBox[{"J1", "\[Rule]", 
  RowBox[{"{", "}"}]}]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1847587056],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "2", "}"}]}],
  SequenceForm["Block ", {2}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->864876806],

Cell[BoxData[
 RowBox[{"J2", "\[Rule]", 
  RowBox[{"{", "J1", "}"}]}]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1243175437],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J2\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J2", " \[Rule] ", $CellContext`J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->516024327],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Block \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "3", "}"}]}],
  SequenceForm["Block ", {3}],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->931389326],

Cell[BoxData[
 RowBox[{"J3", "\[Rule]", 
  RowBox[{"{", 
   RowBox[{"J1", ",", "J2"}], "}"}]}]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->714528911],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1552835977],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J2$J1$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J2$J1$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->396551651],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\t\"\>", "\[InvisibleSpace]", "\<\"J3\"\>", 
   "\[InvisibleSpace]", "\<\" \[Rule] \"\>", "\[InvisibleSpace]", "J2$h", 
   "\[InvisibleSpace]", "\<\": summation direction +\"\>"}],
  SequenceForm[
  "\t", "J3", " \[Rule] ", $CellContext`J2$h, ": summation direction +"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[2]:=",
 CellID->1270990196]
}, Open  ]]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"J", "[", 
   RowBox[{"mSetSolution", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"J1", "\[Rule]", 
       SuperscriptBox["2", 
        RowBox[{"-", "\[Nu]"}]]}], ",", 
      RowBox[{"J2", "\[Rule]", 
       SuperscriptBox["7", 
        RowBox[{"-", "\[Nu]"}]]}], ",", 
      RowBox[{"J3", "\[Rule]", 
       SuperscriptBox["5", 
        RowBox[{"-", "\[Nu]"}]]}]}], "}"}], ",", "\[Nu]"}], "]"}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.70451022566409*^9, 3.704510236214141*^9}},
 CellLabel->"In[3]:=",
 CellID->1471299360],

Cell[TextData[{
 "By default ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreOrderIncrease"]],"paclet:DREAM/ref/$CoreOrderIncrease"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is disabled:"
}], "ExampleText",
 CellID->477905686],

Cell[CellGroupData[{

Cell[BoxData["$CoreOrderIncrease"], "Input",
 CellChangeTimes->{{3.7045954980048656`*^9, 3.704595499684722*^9}},
 CellLabel->"In[4]:=",
 CellID->546264252],

Cell[BoxData["False"], "Output",
 CellLabel->"Out[4]=",
 CellID->57172081]
}, Open  ]],

Cell["\<\
And there is expansion order loss, if computable objects have singularities:\
\>", "ExampleText",
 CellID->1616010458],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"J", "[", 
  RowBox[{"mEvaluate", ",", "All", ",", 
   RowBox[{"0", "-", "\[Epsilon]"}], ",", "3", ",", "10"}], "]"}]], "Input",
 CellChangeTimes->{{3.704595442919014*^9, 3.704595480337372*^9}},
 CellLabel->"In[5]:=",
 CellID->710926726],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"J1", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{"1.`17.", "+", 
      RowBox[{
      "0.69314718055994530937448036556070007919`17.", " ", "\[Epsilon]"}], 
      "+", 
      RowBox[{"0.24022650695910071230391800409900964329`17.", " ", 
       SuperscriptBox["\[Epsilon]", "2"]}], "+", 
      RowBox[{"0.05550410866482157994287215878300284668`17.", " ", 
       SuperscriptBox["\[Epsilon]", "3"]}], "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "4"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, 0, 4, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 0, {
      1.`17., 0.69314718055994530937448036556070007919`17., 
       0.24022650695910071230391800409900964329`17., 
       0.05550410866482157994287215878300284668`17.}, 0, 4, 1],
     Editable->False]}], ",", 
   RowBox[{
    RowBox[{"J2", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{
      RowBox[{
      "-", "22.33333333134105787019480168976391306511`9.111186459555741"}], 
      "-", 
      RowBox[{
      "51.78107534955170220508964890957503312891`9.114577574181327", " ", 
       "\[Epsilon]"}], "-", 
      RowBox[{
      "59.96219479620165354185842092893181838381`9.117427593896705", " ", 
       SuperscriptBox["\[Epsilon]", "2"]}], "-", 
      RowBox[{
      "46.24778084792906709583750015315990837627`9.119824986641984", " ", 
       SuperscriptBox["\[Epsilon]", "3"]}], "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "4"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, 0, 4, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 
      0, {-22.33333333134105787019480168976391306511`9.111186459555741, \
-51.78107534955170220508964890957503312891`9.114577574181327, \
-59.96219479620165354185842092893181838381`9.117427593896705, \
-46.24778084792906709583750015315990837627`9.119824986641984}, 0, 4, 1],
     Editable->False]}], ",", 
   RowBox[{
    RowBox[{"J3", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{
      FractionBox["5.`9.999999348558767", 
       SuperscriptBox["\[Epsilon]", "3"]], "+", 
      FractionBox[
       "8.04718956217050187300379666613093819745`9.999998787438612", 
       SuperscriptBox["\[Epsilon]", "2"]], "+", 
      FractionBox[
       "6.47572598495058736295042751248163723819`9.999997581890787", 
       "\[Epsilon]"], "+", "118.53665225345962`", "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "1"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, -3, 1, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 0, {
      5.`9.999999348558767, 
       8.04718956217050187300379666613093819745`9.999998787438612, 
       6.47572598495058736295042751248163723819`9.999997581890787, 
       118.53665225345962`}, -3, 1, 1],
     Editable->False]}]}], "}"}]], "Output",
 CellLabel->"Out[5]=",
 CellID->2126188174]
}, Open  ]],

Cell["If you enable this option, expansion order will not be lost:", \
"ExampleText",
 CellID->1194658393],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Block", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"$CoreOrderIncrease", "=", "True"}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"J", "[", 
    RowBox[{"mEvaluate", ",", "All", ",", 
     RowBox[{"0", "-", "\[Epsilon]"}], ",", "3", ",", "10"}], "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellLabel->"In[6]:=",
 CellID->1858043107],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"J1", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{"1.`17.", "+", 
      RowBox[{
      "0.69314718055994530937448036556070007919`17.", " ", "\[Epsilon]"}], 
      "+", 
      RowBox[{"0.24022650695910071230391800409900964329`17.", " ", 
       SuperscriptBox["\[Epsilon]", "2"]}], "+", 
      RowBox[{"0.05550410866482157994287215878300284668`17.", " ", 
       SuperscriptBox["\[Epsilon]", "3"]}], "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "4"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, 0, 4, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 0, {
      1.`17., 0.69314718055994530937448036556070007919`17., 
       0.24022650695910071230391800409900964329`17., 
       0.05550410866482157994287215878300284668`17.}, 0, 4, 1],
     Editable->False]}], ",", 
   RowBox[{
    RowBox[{"J2", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{
      RowBox[{
      "-", "22.33333333134105787019480168976391306511`9.214516770531112"}], 
      "-", 
      RowBox[{
      "51.78107534955170220508964890957503312891`9.218012513225636", " ", 
       "\[Epsilon]"}], "-", 
      RowBox[{
      "59.96219479620165354185842092893181838381`9.220951120034997", " ", 
       SuperscriptBox["\[Epsilon]", "2"]}], "-", 
      RowBox[{
      "46.24778084792906709583750015315990837627`9.223423496510541", " ", 
       SuperscriptBox["\[Epsilon]", "3"]}], "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "4"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, 0, 4, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 
      0, {-22.33333333134105787019480168976391306511`9.214516770531112, \
-51.78107534955170220508964890957503312891`9.218012513225636, \
-59.96219479620165354185842092893181838381`9.220951120034997, \
-46.24778084792906709583750015315990837627`9.223423496510541}, 0, 4, 1],
     Editable->False]}], ",", 
   RowBox[{
    RowBox[{"J3", "[", 
     RowBox[{"-", "\[Epsilon]"}], "]"}], "\[Rule]", 
    InterpretationBox[
     RowBox[{
      FractionBox["5.`9.999999348558767", 
       SuperscriptBox["\[Epsilon]", "3"]], "+", 
      FractionBox[
       "8.04718956217050187300379666613093819745`9.999998787438612", 
       SuperscriptBox["\[Epsilon]", "2"]], "+", 
      FractionBox[
       "6.47572598495058736295042751248163723819`9.999997581890787", 
       "\[Epsilon]"], "+", "118.53665225345962`", "+", 
      RowBox[{"489.2141804220557`", " ", "\[Epsilon]"}], "+", 
      RowBox[{"1028.4476597536125`", " ", 
       SuperscriptBox["\[Epsilon]", "2"]}], "+", 
      RowBox[{"1425.7539844531452`", " ", 
       SuperscriptBox["\[Epsilon]", "3"]}], "+", 
      InterpretationBox[
       SuperscriptBox[
        RowBox[{"O", "[", "\[Epsilon]", "]"}], "4"],
       SeriesData[$CellContext`\[Epsilon], 0, {}, -3, 4, 1],
       Editable->False]}],
     SeriesData[$CellContext`\[Epsilon], 0, {
      5.`9.999999348558767, 
       8.04718956217050187300379666613093819745`9.999998787438612, 
       6.47572598495058736295042751248163723819`9.999997581890787, 
       118.53665225345962`, 489.2141804220557, 1028.4476597536125`, 
       1425.7539844531452`}, -3, 4, 1],
     Editable->False]}]}], "}"}]], "Output",
 CellLabel->"Out[6]=",
 CellID->1355465432]
}, Open  ]],

Cell["Clean up:", "ExampleText",
 CellID->1931349357],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"J", "[", "mPurge", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->1116057215],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"J1$h\"\>", ",", "\<\"J2$h\"\>", ",", "\<\"J2$J1$adapter\"\>", 
   ",", "\<\"J2$J1$h\"\>", ",", "\<\"J3$h\"\>", ",", "\<\"J3$J1$adapter\"\>", 
   ",", "\<\"J3$J1$h\"\>", ",", "\<\"J3$J2$adapter\"\>", 
   ",", "\<\"J3$J2$J1$h\"\>", ",", "\<\"J3$J2$h\"\>", ",", "\<\"J1\"\>", 
   ",", "\<\"J2\"\>", ",", "\<\"J3\"\>", ",", "\<\"J\"\>"}], "}"}]], "Output",\

 CellLabel->"Out[7]=",
 CellID->1246490550]
}, Open  ]]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreDebug"]],"paclet:DREAM/ref/$CoreDebug"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreDeveloperTools"]],"paclet:DREAM/ref/$CoreDeveloperTools"},
   
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreParallel"]],"paclet:DREAM/ref/$CoreParallel"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreProgressBar"]],"paclet:DREAM/ref/$CoreProgressBar"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$CoreReadySound"]],"paclet:DREAM/ref/$CoreReadySound"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1920, 1007},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"$CoreOrderIncrease",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "$CoreOrderIncrease is an option which controls the behaviour of DREAM in \
case of order expansion loss.", "synonyms" -> {}, "title" -> 
    "$CoreOrderIncrease", "type" -> "Symbol", "uri" -> 
    "DREAM/ref/$CoreOrderIncrease"}, "SearchTextTranslated" -> "", 
  "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3787, 143, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 22947, 739}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[590, 21, 110, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[703, 26, 250, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[978, 40, 605, 18, 98, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1608, 62, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2348, 88, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2382, 90, 211, 8, 36, "Notes",
 CellID->718278630],
Cell[2596, 100, 594, 17, 60, "Notes",
 CellID->187944676],
Cell[3193, 119, 511, 16, 39, "Notes",
 CellID->1205110490],
Cell[3707, 137, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3787, 143, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[4127, 160, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[4237, 164, 94, 3, 33, "Input",
 CellID->2007368699],
Cell[4334, 169, 288, 11, 43, "ExampleText",
 CellID->1068934427],
Cell[CellGroupData[{
Cell[4647, 184, 1336, 39, 169, "Input",
 CellID->1262067781],
Cell[CellGroupData[{
Cell[6008, 227, 499, 11, 29, "Print",
 CellID->371825490],
Cell[6510, 240, 133, 2, 29, "Print",
 CellID->532679420],
Cell[6646, 244, 103, 2, 29, "Print",
 CellID->917810082],
Cell[6752, 248, 239, 7, 29, "Print",
 CellID->2040750494],
Cell[6994, 257, 139, 4, 29, "Print",
 CellID->1847587056],
Cell[7136, 263, 238, 7, 29, "Print",
 CellID->864876806],
Cell[7377, 272, 145, 4, 29, "Print",
 CellID->1243175437],
Cell[7525, 278, 414, 9, 29, "Print",
 CellID->516024327],
Cell[7942, 289, 238, 7, 29, "Print",
 CellID->931389326],
Cell[8183, 298, 169, 5, 29, "Print",
 CellID->714528911],
Cell[8355, 305, 415, 9, 29, "Print",
 CellID->1552835977],
Cell[8773, 316, 420, 9, 29, "Print",
 CellID->396551651],
Cell[9196, 327, 415, 9, 29, "Print",
 CellID->1270990196]
}, Open  ]]
}, Open  ]],
Cell[9638, 340, 567, 18, 37, "Input",
 CellID->1471299360],
Cell[10208, 360, 265, 9, 43, "ExampleText",
 CellID->477905686],
Cell[CellGroupData[{
Cell[10498, 373, 155, 3, 33, "Input",
 CellID->546264252],
Cell[10656, 378, 74, 2, 42, "Output",
 CellID->57172081]
}, Open  ]],
Cell[10745, 383, 128, 3, 40, "ExampleText",
 CellID->1616010458],
Cell[CellGroupData[{
Cell[10898, 390, 260, 6, 33, "Input",
 CellID->710926726],
Cell[11161, 398, 3158, 78, 101, "Output",
 CellID->2126188174]
}, Open  ]],
Cell[14334, 479, 106, 2, 40, "ExampleText",
 CellID->1194658393],
Cell[CellGroupData[{
Cell[14465, 485, 377, 11, 80, "Input",
 CellID->1858043107],
Cell[14845, 498, 3476, 84, 125, "Output",
 CellID->1355465432]
}, Open  ]],
Cell[18336, 585, 53, 1, 40, "ExampleText",
 CellID->1931349357],
Cell[CellGroupData[{
Cell[18414, 590, 103, 3, 33, "Input",
 CellID->1116057215],
Cell[18520, 595, 442, 9, 42, "Output",
 CellID->1246490550]
}, Open  ]]
}, Open  ]],
Cell[18989, 608, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[19078, 614, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[19346, 628, 1516, 44, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[20899, 677, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[21180, 691, 31, 0, 14, "SectionHeaderSpacer"],
Cell[21214, 693, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[21404, 701, 23, 0, 47, "FooterCell"]
}
]
*)

