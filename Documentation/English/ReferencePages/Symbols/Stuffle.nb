(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     13027,        483]
NotebookOptionsPosition[      9110,        363]
NotebookOutlinePosition[     11065,        413]
CellTagsIndexPosition[     10980,        408]
WindowTitle->Stuffle
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM`UTILITIES SUBPACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["Stuffle", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["Stuffle"]],"paclet:DREAM/ref/Stuffle"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       RowBox[{
        SubscriptBox[
         StyleBox["l", "TI"], "1"], ",", 
        SubscriptBox[
         StyleBox["l", "TI"], "2"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]generates a list of all stuffle permutations of ",
     Cell[BoxData[
      FormBox[
       SubscriptBox[
        StyleBox["l", "TI"], "1"], TraditionalForm]], "InlineFormula"],
     " and ",
     Cell[BoxData[
      FormBox[
       SubscriptBox[
        StyleBox["l", "TI"], "2"], TraditionalForm]], "InlineFormula"],
     " for multiple polylogarithms."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package and then call ",
 Cell[BoxData[
  RowBox[{
   TemplateBox[{Cell[
      TextData["Needs"]],"ref/Needs"},
    "RefLink",
    BaseStyle->"InlineFormula"], "[", "\"\<DREAM`Utilities`\>\"", "]"}]], 
  "InlineFormula"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["l", "TI"], "1"], TraditionalForm]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    StyleBox["l", "TI"], "2"], TraditionalForm]], "InlineFormula"],
 " have to be lists of pairs."
}], "Notes",
 CellID->1581673783],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Stuffle"]],"paclet:DREAM/ref/Stuffle"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns a list of lists of pairs."
}], "Notes",
 CellID->392734785],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Stuffle"]],"paclet:DREAM/ref/Stuffle"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " generates permutations, which are stuffle relations for multiple \
polylogarithms."
}], "Notes",
 CellID->1929500842],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic examples", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "DREAM`"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[BoxData[
 RowBox[{
  RowBox[{"Needs", "[", "\"\<DREAM`Utilities`\>\"", "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->1378172001],

Cell["Generate a stuffle permutation:", "ExampleText",
 CellID->477905686],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Stuffle", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"2", ",", "1"}], "}"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"2", ",", "1"}], "}"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.714955701585845*^9, 3.714955751172969*^9}, {
   3.71495586437393*^9, 3.714955877972849*^9}, 3.714955920663033*^9, 
   3.714955995404962*^9},
 CellLabel->"In[3]:=",
 CellID->1421753520],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"4", ",", "1"}], "}"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.714955723214303*^9, 3.714955751643051*^9}, {
   3.7149558660862083`*^9, 3.714955878334983*^9}, 3.7149559212638187`*^9, 
   3.714955995878209*^9, 3.7149565010670967`*^9},
 CellLabel->"Out[3]=",
 CellID->134870774]
}, Open  ]],

Cell[TextData[{
 "This relation states ",
 Cell[BoxData[
  RowBox[{
   SuperscriptBox[
    RowBox[{"\[Zeta]", "[", "2", "]"}], "2"], "\[Equal]", 
   RowBox[{
    RowBox[{"\[Zeta]", "[", "4", "]"}], "+", 
    RowBox[{"2", 
     RowBox[{"\[Zeta]", "[", 
      RowBox[{"2", ",", "2"}], "]"}]}]}]}]], "InlineFormula",
  FormatType->"StandardForm"],
 ":"
}], "ExampleText",
 CellID->1077239643],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"2", 
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["k", 
       RowBox[{"-", "2"}]], 
      SuperscriptBox["n", 
       RowBox[{"-", "2"}]]}], ",", 
     RowBox[{"{", 
      RowBox[{"k", ",", "1", ",", "\[Infinity]"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", 
       RowBox[{"k", "+", "1"}], ",", "\[Infinity]"}], "}"}]}], "]"}]}], "+", 
  RowBox[{"Zeta", "[", "4", "]"}]}]], "Input",
 CellChangeTimes->{{3.714955770068434*^9, 3.7149558337241898`*^9}, {
  3.714955893560914*^9, 3.714955927605455*^9}, {3.714956001605569*^9, 
  3.714956005941619*^9}},
 CellLabel->"In[4]:=",
 CellID->1814348720],

Cell[BoxData[
 FractionBox[
  SuperscriptBox["\[Pi]", "4"], "36"]], "Output",
 CellChangeTimes->{{3.714955789882421*^9, 3.71495579389637*^9}, {
   3.71495583152308*^9, 3.714955835231914*^9}, 3.7149559182080593`*^9, {
   3.714955994112423*^9, 3.714956006264049*^9}, 3.7149565694590597`*^9},
 CellLabel->"Out[4]=",
 CellID->964203794]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 SuperscriptBox[
  RowBox[{"Zeta", "[", "2", "]"}], "2"]], "Input",
 CellChangeTimes->{{3.714955913717051*^9, 3.7149559325559893`*^9}, {
  3.714956009389578*^9, 3.714956009989607*^9}},
 CellLabel->"In[5]:=",
 CellID->367166046],

Cell[BoxData[
 FractionBox[
  SuperscriptBox["\[Pi]", "4"], "36"]], "Output",
 CellChangeTimes->{3.7149560102554626`*^9, 3.714956570340465*^9},
 CellLabel->"Out[5]=",
 CellID->1580516115]
}, Open  ]]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Shuffle"]],"paclet:DREAM/ref/Shuffle"},
  "RefLink",
  BaseStyle->{
   "InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"Stuffle",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "Stuffle[\!\(\*SubscriptBox[\nStyleBox[\"l\", \"TI\"], \(1\)]\), \!\(\*\n\
StyleBox[SubscriptBox[\nStyleBox[\"l\", \"TI\"], \"2\"],\nFontSlant->\"Italic\
\"]\)] generates a list of all stuffle permutations of \!\(\*SubscriptBox[\n\
StyleBox[\"l\", \"TI\"], \(1\)]\) and \!\(\*SubscriptBox[\nStyleBox[\"l\", \
\"TI\"], \(2\)]\) for multiple polylogarithms.", "synonyms" -> {}, "title" -> 
    "Stuffle", "type" -> "Symbol", "uri" -> "DREAM/ref/Stuffle"}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4092, 160, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 10836, 401}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[579, 21, 123, 3, 58, "AnchorBarGrid",
 CellID->1],
Cell[705, 26, 239, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[969, 40, 963, 30, 97, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1957, 74, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2697, 100, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2731, 102, 423, 15, 60, "Notes",
 CellID->718278630],
Cell[3157, 119, 312, 12, 37, "Notes",
 CellID->1581673783],
Cell[3472, 133, 242, 8, 39, "Notes",
 CellID->392734785],
Cell[3717, 143, 292, 9, 60, "Notes",
 CellID->1929500842],
Cell[4012, 154, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[4092, 160, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[4432, 177, 108, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[4543, 181, 94, 3, 33, "Input",
 CellID->2007368699],
Cell[4640, 186, 143, 4, 33, "Input",
 CellID->1378172001],
Cell[4786, 192, 74, 1, 40, "ExampleText",
 CellID->477905686],
Cell[CellGroupData[{
Cell[4885, 197, 440, 13, 33, "Input",
 CellID->1421753520],
Cell[5328, 212, 703, 22, 42, "Output",
 CellID->134870774]
}, Open  ]],
Cell[6046, 237, 389, 14, 41, "ExampleText",
 CellID->1077239643],
Cell[CellGroupData[{
Cell[6460, 255, 671, 20, 41, "Input",
 CellID->1814348720],
Cell[7134, 277, 332, 7, 70, "Output",
 CellID->964203794]
}, Open  ]],
Cell[CellGroupData[{
Cell[7503, 289, 241, 6, 37, "Input",
 CellID->367166046],
Cell[7747, 297, 187, 5, 70, "Output",
 CellID->1580516115]
}, Open  ]]
}, Open  ]],
Cell[7961, 306, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[8050, 312, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[8318, 326, 223, 6, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[8578, 337, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[8859, 351, 31, 0, 14, "SectionHeaderSpacer"],
Cell[8893, 353, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[9083, 361, 23, 0, 47, "FooterCell"]
}
]
*)

