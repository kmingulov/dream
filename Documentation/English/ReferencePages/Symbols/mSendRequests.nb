(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6044,        226]
NotebookOptionsPosition[      3836,        161]
NotebookOutlinePosition[      5318,        201]
CellTagsIndexPosition[      5275,        198]
WindowTitle->mSendRequests
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["DREAM PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["mSendRequests", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["mSendRequests"]],"paclet:DREAM/ref/mSendRequests"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]is a method of several ",
     StyleBox["DREAM",
      FontWeight->"Bold"],
     " classes."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this symbol, you first need to load ",
 "the ",
 ButtonBox["DREAM",
  BaseStyle->"Link",
  ButtonData->"paclet:DREAM/guide/DREAMPackage"],
 " package."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "All fields, defined in ",
 StyleBox["DREAM",
  FontWeight->"Bold"],
 ", start with letter ",
 StyleBox["f",
  FontSlant->"Italic"],
 ". All methods start with letter ",
 StyleBox["m",
  FontSlant->"Italic"],
 "."
}], "Notes",
 CellID->1013605324],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["mSendRequests"]],"paclet:DREAM/ref/mSendRequests"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " is a method of ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["TComputable"]],"paclet:DREAM/ref/TComputable"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " and all its derived classes."
}], "Notes",
 CellID->1036050656],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["DREAM Package"]],"paclet:DREAM/guide/DREAMPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"mSendRequests",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "DREAM`", "keywords" -> {}, "index" -> True, "label" -> 
    "DREAM Package Symbol", "language" -> "en", "paclet" -> "DREAM Package", 
    "status" -> "", "summary" -> 
    "mSendRequests is a method of several DREAM classes.", "synonyms" -> {}, 
    "title" -> "mSendRequests", "type" -> "Symbol", "uri" -> 
    "DREAM/ref/mSendRequests"}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> 
       "UTF-8"]]], 
   Cell[
    StyleData["NotesText", StyleDefinitions -> None], Editable -> False, 
    ShowCellBracket -> Automatic, CellMargins -> {{40, 24}, {9, 7}}, 
    CellFrameMargins -> 0, MenuSortingValue -> None, MenuCommandKey -> "8", 
    FontFamily -> "Verdana", FontSize -> 11, FontWeight -> "Plain"]}, Visible -> 
  False, FrontEndVersion -> "10.4 for Linux x86 (64-bit) (April 11, 2016)", 
  StyleDefinitions -> "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[585, 21, 110, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[698, 26, 245, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[968, 40, 540, 17, 98, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1533, 61, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2273, 87, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2307, 89, 209, 8, 36, "Notes",
 CellID->718278630],
Cell[2519, 99, 264, 12, 38, "Notes",
 CellID->1013605324],
Cell[2786, 113, 435, 14, 39, "Notes",
 CellID->1036050656],
Cell[3224, 129, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3304, 135, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[3585, 149, 31, 0, 14, "SectionHeaderSpacer"],
Cell[3619, 151, 175, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[3809, 159, 23, 0, 47, "FooterCell"]
}
]
*)

