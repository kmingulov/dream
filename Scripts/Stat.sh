# Chaning the directory.
cd "../Source"

# Getting all files and counting lines in them.
find . \( -name "*.m" -o -name "*.c" -o -name "*.h" \) | xargs wc -l | sort -n -r
