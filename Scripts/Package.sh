# Checking whether the version is given.
if [[ $1 = "" ]]; then
    echo "Usage: $0 version"
    exit
fi

# Working directory.
workdir=`pwd`

# Echoing the destination folder.
echo "Package version:    $1"
echo ""

# Creating directories.
mkdir "/tmp/DREAM/"
mkdir "/tmp/DREAM/DREAM-$1/"

# Copying package files.
echo -n "Copying package files... "
cd ../Source/
find . -name "*.m" | cpio -pdm "/tmp/DREAM/DREAM-$1"
find . -name "*.so" | cpio -pdm "/tmp/DREAM/DREAM-$1"
echo "Done."

# Copying distribution files.
echo -n "Copying distribution files... "
cp ../README.md "/tmp/DREAM/"
cp -r ../Documentation/ "/tmp/DREAM/"
cp ../DistributionFiles/* "/tmp/DREAM/"
echo "Done."

# Creating launcher file.
echo -n "Creating launcher... "
echo "SetDirectory@DirectoryName@\$InputFileName; << \"DREAM-$1/DREAM.m\"; ResetDirectory[];" > "/tmp/DREAM/DREAM.m"
echo "Done."

# Creating the archive.
echo -n "Creating the archive... "
cd "/tmp"
zip -r "$workdir/../$1.zip" "DREAM/"
echo "Done."

# Removing files.
rm -rf "/tmp/DREAM/"

# Everything's done.
echo ""
echo "Everything is done."