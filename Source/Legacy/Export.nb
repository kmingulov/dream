Notebook[{

Cell[CellGroupData[{
Cell["Legacy.Export", "Title"],

Cell[TextData[{
 "This file contains functions for export of ",
 StyleBox["TComputable",
  FontWeight->"Bold"],
 " objects into SummerTime format."
}], "Text"],

Cell[CellGroupData[{

Cell["Change log", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["September 15, 2017.",
  FontWeight->"Bold"],
 " Now adapter can be ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["March 24, 2017.",
  FontWeight->"Bold"],
 " Slightly rewrote. Adapted to the new interface of ",
 StyleBox["TSum",
  FontWeight->"Bold"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["March 13, 2017.",
  FontWeight->"Bold"],
 " Adapted to the new version of ",
 StyleBox["mGetSolution",
  FontSlant->"Italic"],
 " method."
}], "Item"],

Cell[TextData[{
 StyleBox["February 17, 2017.",
  FontWeight->"Bold"],
 " Now ",
 StyleBox["TUnity",
  FontWeight->"Bold"],
 " can be exported, too."
}], "Item"],

Cell[TextData[{
 StyleBox["January 19, 2017.",
  FontWeight->"Bold"],
 " Added checks for custom objects."
}], "Item"],

Cell[TextData[{
 StyleBox["November 1-2, 2016.",
  FontWeight->"Bold"],
 " Fixed a bug with zero homogeneous solution."
}], "Item"],

Cell[TextData[{
 StyleBox["October 28, 2016.",
  FontWeight->"Bold"],
 " Initial commit."
}], "Item"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Preamble", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", "}"}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["ExportToSummerTime", "Section"],

Cell["This procedure exports the objects into SummerTime format.", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExportToSummerTime", "::", "Failed"}], "=", 
   "\"\<Cannot export object `1` into the SummerTime format. Only objects of \
types \!\(\*
StyleBox[\"THomo\",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\",\",
FontWeight->\"Bold\"]\) \!\(\*
StyleBox[\"TSum\",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\",\",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"TExpr\",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"and\",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",
FontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"TUnity\",
FontWeight->\"Bold\"]\) are supported.\>\""}], ";"}]], "Code"],

Cell[CellGroupData[{

Cell[TextData[StyleBox["TExpr",
 FontWeight->"Bold"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportToSummerTime", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TExpr"}], "]"}]}], ":=", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"obj", "[", "fExpr", "]"}], "[", "\[Nu]", "]"}], ",", 
      RowBox[{"{", "}"}]}], "}"}], "}"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["TUnity",
 FontWeight->"Bold"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportToSummerTime", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TUnity"}], "]"}]}], ":=", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"1", ",", 
      RowBox[{"{", "}"}]}], "}"}], "}"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["THomo",
 FontWeight->"Bold"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportToSummerTime", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "THomo"}], "]"}]}], ":=", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"obj", "[", 
       RowBox[{"mGetSolution", ",", "\[Nu]"}], "]"}], ",", 
      RowBox[{"{", "}"}]}], "}"}], "}"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["TSum",
 FontWeight->"Bold"]], "Subsection"],

Cell["The procedure is split into:", "Text"],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["ExportHead",
  FontSlant->"Italic"],
 " \[LongDash] exports the head of the sum (can be called on ",
 StyleBox["THomo/TUnity/TExpr",
  FontWeight->"Bold"],
 ")."
}], "Subitem"],

Cell[TextData[{
 StyleBox["ExportAdapter",
  FontSlant->"Italic"],
 " \[LongDash] exports the adapter of the sum (can be called on ",
 StyleBox["TUnity/TExpr",
  FontWeight->"Bold"],
 ")."
}], "Subitem"],

Cell[TextData[{
 StyleBox["ExportTail",
  FontSlant->"Italic"],
 " \[LongDash] exports the tail of the sum (can be called on ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 "/",
 StyleBox["TSum/TUnity/TExpr",
  FontWeight->"Bold"],
 ")."
}], "Subitem"]
}, Open  ]],

Cell[TextData[{
 StyleBox["ExportSum",
  FontSlant->"Italic"],
 " exports the whole hierarchy into SummerTime format, using these two \
functions."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportToSummerTime", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TSum"}], "]"}]}], ":=", 
   RowBox[{"{", 
    RowBox[{"ExportTail", "[", 
     RowBox[{"obj", ",", "\[Nu]", ",", "0"}], "]"}], "}"}]}], ";"}]], "Code"],

Cell[CellGroupData[{

Cell["ExportHead", "Subsubsection"],

Cell[TextData[{
 "This function exports head in form ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"S", "(", 
     RowBox[{"\[Nu]", "+", "q"}], ")"}], 
    RowBox[{
     SuperscriptBox["S", 
      RowBox[{"-", "1"}]], "(", 
     RowBox[{"\[Nu]", "+", "k"}], ")"}]}], TraditionalForm]]],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportHead", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "q_", ",", "k_"}],
      "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "THomo"}], "]"}]}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"sol", ",", "x"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Getting", " ", "the", " ", 
       RowBox[{"solution", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"sol", "=", 
       RowBox[{"obj", "[", 
        RowBox[{"mGetSolution", ",", "x"}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Checking", " ", "whether", " ", "it", " ", "is", " ", 
        RowBox[{"set", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"sol", "===", "None"}], "||", 
         RowBox[{"sol", "===", "0"}]}], ",", "\[IndentingNewLine]", 
        RowBox[{"(*", " ", 
         RowBox[{
          RowBox[{
          "If", " ", "we", " ", "have", " ", "set", " ", "zero", " ", 
           "solution"}], ",", " ", 
          RowBox[{"we", " ", "guess", " ", "it", " ", "by", " ", 
           RowBox[{"HomogeneousSolution", "."}]}]}], " ", "*)"}], 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"sol", "=", 
          RowBox[{"HomogeneousSolution", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"obj", "[", "fCert", "]"}], "[", "x", "]"}], ",", "x"}], 
           "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"sol", "/.", 
            RowBox[{"x", "\[Rule]", 
             RowBox[{"\[Nu]", "+", "q"}]}]}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{"sol", "/.", 
            RowBox[{"x", "\[Rule]", 
             RowBox[{"\[Nu]", "+", "k"}]}]}], ")"}]}]}]}], 
       "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Returning", " ", "the", " ", "head", " ", 
        RowBox[{"expression", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"sol", "/.", 
         RowBox[{"x", "\[Rule]", 
          RowBox[{"\[Nu]", "+", "q"}]}]}], ")"}], "/", 
       RowBox[{"(", 
        RowBox[{"sol", "/.", 
         RowBox[{"x", "\[Rule]", 
          RowBox[{"\[Nu]", "+", "k"}]}]}], ")"}]}]}]}], "\[IndentingNewLine]",
     "]"}]}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportHead", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "q_", ",", "k_"}],
      "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TUnity"}], "]"}]}], ":=", "1"}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportHead", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "q_", ",", "k_"}],
      "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TExpr"}], "]"}]}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{"obj", "[", "fExpr", "]"}], "[", 
     RowBox[{"\[Nu]", "+", "q"}], "]"}], "/", 
    RowBox[{
     RowBox[{"obj", "[", "fExpr", "]"}], "[", 
     RowBox[{"\[Nu]", "+", "k"}], "]"}]}]}], ";"}]], "Code"],

Cell[TextData[{
 "Error form for cases, when ",
 StyleBox["obj",
  FontSlant->"Italic"],
 " is not exportable."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExportHead", "[", 
    RowBox[{"obj_", ",", "_", ",", "_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"ExportToSummerTime", "::", "Failed"}], ",", "obj"}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}]}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["ExportAdapter", "Subsubsection"],

Cell[TextData[{
 "This function exports ",
 StyleBox["TUnity",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportAdapter", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TUnity"}], "]"}]}], ":=", "1"}], ";"}]], "Code"],

Cell[TextData[{
 "This function exports ",
 StyleBox["TExpr",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportAdapter", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TExpr"}], "]"}]}], ":=", 
   RowBox[{
    RowBox[{"obj", "[", "fExpr", "]"}], "[", 
    RowBox[{"\[Nu]", "+", "k"}], "]"}]}], ";"}]], "Code"],

Cell[TextData[{
 "This function exports ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportAdapter", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "THomo"}], "]"}]}], ":=", 
   RowBox[{"obj", "[", 
    RowBox[{"mGetSolution", ",", 
     RowBox[{"\[Nu]", "+", "k"}]}], "]"}]}], ";"}]], "Code"],

Cell[TextData[{
 "Error form for cases, when ",
 StyleBox["obj",
  FontSlant->"Italic"],
 " is not exportable."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExportAdapter", "[", 
    RowBox[{"obj_", ",", "_", ",", "_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"ExportToSummerTime", "::", "Failed"}], ",", "obj"}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}]}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["ExportTail", "Subsubsection"],

Cell[TextData[{
 "This function exports ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 " in form ",
 Cell[BoxData[
  FormBox[
   RowBox[{"S", "(", 
    RowBox[{"\[Nu]", "+", "q"}], ")"}], TraditionalForm]]],
 ". For tail ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 " object we do not need to divide by homogeneous solution."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportTail", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "THomo"}], "]"}]}], ":=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"obj", "[", 
      RowBox[{"mGetSolution", ",", 
       RowBox[{"\[Nu]", "+", "k"}]}], "]"}], ",", 
     RowBox[{"{", "}"}]}], "}"}]}], ";"}]], "Code"],

Cell[TextData[{
 "This function exports ",
 StyleBox["TUnity",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportTail", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TUnity"}], "]"}]}], ":=", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     RowBox[{"{", "}"}]}], "}"}]}], ";"}]], "Code"],

Cell[TextData[{
 "This function exports ",
 StyleBox["TExpr",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportTail", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "k_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TExpr"}], "]"}]}], ":=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"obj", "[", "fExpr", "]"}], "[", 
      RowBox[{"\[Nu]", "+", "k"}], "]"}], ",", 
     RowBox[{"{", "}"}]}], "}"}]}], ";"}]], "Code"],

Cell[TextData[{
 "This function exports ",
 StyleBox["TSum",
  FontWeight->"Bold"],
 ". Important notes:"
}], "Text"],

Cell[CellGroupData[{

Cell["\<\
If summation goes into negative infinity, the corresponding variable in the \
expression and in the summation specification must be placed with minus sign.\
\
\>", "Subitem"],

Cell[TextData[{
 "SummerTime triangle functions calculate sums ",
 Cell[BoxData[
  FormBox[
   RowBox[{"k", "\[GreaterEqual]", "q"}], TraditionalForm]]],
 " and ",
 Cell[BoxData[
  FormBox[
   RowBox[{"k", "\[LessEqual]", "q"}], TraditionalForm]]],
 ", but we do calculate sums ",
 Cell[BoxData[
  FormBox[
   RowBox[{"k", "\[GreaterEqual]", "q"}], TraditionalForm]]],
 " and ",
 Cell[BoxData[
  FormBox[
   RowBox[{"k", "<", "q"}], TraditionalForm]]],
 ". So, if the sum goes down, we need to shift variables:\[LineSeparator]\t",
 Cell[BoxData[
  RowBox[{
   RowBox[{
    SubscriptBox["\[Sum]", 
     RowBox[{"k", "<", "0"}]], 
    RowBox[{"f", "[", "k", "]"}]}], "=", 
   RowBox[{
    SubscriptBox["\[Sum]", 
     RowBox[{"k", "<", "0"}]], 
    RowBox[{
     RowBox[{"f", "[", 
      RowBox[{"k", "-", "1"}], "]"}], "."}]}]}]], "DisplayFormula",
  TextAlignment->Center],
 "\[LineSeparator]So: if the variable in summation specification goes with \
plus sign, we perform substitution #\[Rule]#-1, but if it is present with \
minus sign, we have to substitute -#\[Rule]-#+1 (since we changed sign of the \
variable in the expression)."
}], "Subitem"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportTail", "[", 
     RowBox[{
      RowBox[{"obj_", "?", "ObjectQ"}], ",", "\[Nu]_", ",", "var_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"obj", ",", "TSum"}], "]"}]}], ":=", "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"s", ",", "tail", ",", "down", ",", "vars", ",", "rules"}], 
      "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Summation", " ", 
       RowBox[{"variable", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"s", "=", 
       RowBox[{"Unique", "[", "\"\<k\>\"", "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Whether", " ", "sum", " ", "goes", " ", 
        RowBox[{"down", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"down", "=", 
       RowBox[{
        RowBox[{"obj", "[", "fDir", "]"}], "===", "\"\<-\>\""}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{"down", ",", 
        RowBox[{"s", "=", 
         RowBox[{"-", "s"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Exporting", " ", 
        RowBox[{"tail", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"tail", "=", 
       RowBox[{"ExportTail", "[", 
        RowBox[{
         RowBox[{"obj", "[", "fTail", "]"}], ",", "\[Nu]", ",", "s"}], 
        "]"}]}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
        RowBox[{"Checking", " ", "the", " ", 
         RowBox[{"tail", ":", " ", 
          RowBox[{"if", " ", "it", " ", "is", " ", "zero"}]}]}], ",", " ", 
        RowBox[{
        "do", " ", "not", " ", "need", " ", "to", " ", "do", " ", "anything", 
         " ", 
         RowBox[{"more", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{
         RowBox[{
         "tail", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], "===",
          "0"}], ",", "\[IndentingNewLine]", 
        RowBox[{"Return", "@", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"{", "}"}]}], "}"}]}]}], "\[IndentingNewLine]", "]"}], ";",
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Forming", " ", "a", " ", "new", " ", "list", " ", "of", " ", 
        RowBox[{"summation", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"vars", "=", 
       RowBox[{"Prepend", "[", 
        RowBox[{
         RowBox[{
         "tail", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}], ",", 
         "s"}], "]"}]}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
        RowBox[{"If", " ", "sum", " ", "goes", " ", "down"}], ",", " ", 
        RowBox[{"we", " ", "need", " ", "to", " ", "shift", " ", 
         RowBox[{"variables", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"rules", "=", 
       RowBox[{"If", "[", 
        RowBox[{"down", ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{"MatchQ", "[", 
              RowBox[{"#", ",", "_Symbol"}], "]"}], ",", 
             RowBox[{"#", "\[Rule]", 
              RowBox[{"#", "-", "1"}]}], ",", 
             RowBox[{
              RowBox[{"-", "#"}], "\[Rule]", 
              RowBox[{
               RowBox[{"-", "#"}], "+", "1"}]}]}], "]"}], "&"}], "/@", 
          "vars"}], ",", "\[IndentingNewLine]", 
         RowBox[{"{", "}"}]}], "\[IndentingNewLine]", "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Finally", ",", " ", 
        RowBox[{"done", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"If", "[", 
           RowBox[{"down", ",", "1", ",", 
            RowBox[{"-", "1"}]}], "]"}], "\[Times]", 
          RowBox[{"ExportHead", "[", 
           RowBox[{
            RowBox[{"obj", "[", "fHead", "]"}], ",", "\[Nu]", ",", "var", ",",
             "s"}], "]"}], "\[Times]", 
          RowBox[{"ExportAdapter", "[", 
           RowBox[{
            RowBox[{"obj", "[", "fAdapter", "]"}], ",", "\[Nu]", ",", "s"}], 
           "]"}], "\[Times]", 
          RowBox[{
          "tail", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}]}], "/.",
          "rules"}], ",", "\[IndentingNewLine]", "vars"}], 
       "\[IndentingNewLine]", "}"}]}]}], "\[IndentingNewLine]", "]"}]}], 
  ";"}]], "Code"],

Cell[TextData[{
 "Error form for cases, when ",
 StyleBox["obj",
  FontSlant->"Italic"],
 " is not exportable."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExportTail", "[", 
    RowBox[{"obj_", ",", "_", ",", "_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"ExportToSummerTime", "::", "Failed"}], ",", "obj"}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}]}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["TMaster",
 FontWeight->"Bold"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"ExportToSummerTime", "[", 
     RowBox[{
      RowBox[{"master_", "?", "ObjectQ"}], ",", "\[Nu]_"}], "]"}], "/;", 
    RowBox[{"InstanceQ", "[", 
     RowBox[{"master", ",", "TMaster"}], "]"}]}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "l", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Exporting", " ", 
       RowBox[{"sums", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"l", "=", 
       RowBox[{
        RowBox[{
         RowBox[{"ExportToSummerTime", "[", 
          RowBox[{"#", ",", "\[Nu]"}], "]"}], "&"}], "/@", 
        RowBox[{"master", "[", "fObjects", "]"}]}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"l", "=", 
       RowBox[{"Join", "@@", "l"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Removing", ",", " ", 
        RowBox[{"what", " ", "is", " ", 
         RowBox[{"zero", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"l", "=", 
       RowBox[{"DeleteCases", "[", 
        RowBox[{"l", ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"{", "}"}]}], "}"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Returning", "."}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{"l", "===", 
         RowBox[{"{", "}"}]}], ",", "\[IndentingNewLine]", 
        RowBox[{"{", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"{", "}"}]}], "}"}], "}"}], ",", "\[IndentingNewLine]", 
        "l"}], "\[IndentingNewLine]", "]"}]}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Anything else", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExportToSummerTime", "[", 
    RowBox[{"obj_", ",", "_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"ExportToSummerTime", "::", "Failed"}], ",", "obj"}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}]}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["End", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}]], "Code"]
}, Open  ]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1920, 1006},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
PrivateNotebookOptions->{"FileOutlineCache"->False},
TrackCellChangeTimes->False,
Magnification:>1.2 Inherited,
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[StyleDefinitions -> FrontEnd`FileName[{
        ParentDirectory[]}, "Stylesheet.nb", CharacterEncoding -> 
       "WindowsANSI"]]]}, Visible -> False, FrontEndVersion -> 
  "10.4 for Linux x86 (64-bit) (April 11, 2016)", StyleDefinitions -> 
  "PrivateStylesheetFormatting.nb"]
]

