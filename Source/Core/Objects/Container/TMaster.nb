Notebook[{

Cell[CellGroupData[{
Cell["Core.Objects.Container.TMaster", "Title"],

Cell[TextData[{
 "Definition of ",
 StyleBox["TMaster",
  FontWeight->"Bold"],
 " class."
}], "Text"],

Cell[CellGroupData[{

Cell["Change log", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["December 7, 2017.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mSolutionSettableQ",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["September 1, 2017.",
  FontWeight->"Bold"],
 " Added \[OpenCurlyDoubleQuote]UnknownField\[CloseCurlyDoubleQuote] rule \
removal."
}], "Item"],

Cell[TextData[{
 StyleBox["March 28, 2017.",
  FontWeight->"Bold"],
 " Removed destructor (due to ",
 StyleBox["mPurge",
  FontSlant->"Italic"],
 " method of ",
 StyleBox["TMasters",
  FontWeight->"Bold"],
 ")."
}], "Item"],

Cell[TextData[{
 StyleBox["March 23, 2017.",
  FontWeight->"Bold"],
 " Significant changes of the interface."
}], "Item"],

Cell[TextData[{
 StyleBox["March 13, 2017.",
  FontWeight->"Bold"],
 " Changed interface of ",
 StyleBox["mGetSolution.",
  FontSlant->"Italic"]
}], "Item"],

Cell[TextData[{
 StyleBox["February 20, 2017.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mGetSolution",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["January 19, 2017.",
  FontWeight->"Bold"],
 " Possible bug fix."
}], "Item"],

Cell[TextData[{
 StyleBox["December 14, 2016.",
  FontWeight->"Bold"],
 " ",
 StyleBox["TMaster",
  FontWeight->"Bold"],
 " now can evaluate custom objects."
}], "Item"],

Cell[TextData[{
 StyleBox["November 1, 2016.",
  FontWeight->"Bold"],
 " There is now only one solution, which is a product of homogeneous and \
periodic ones."
}], "Item"],

Cell[TextData[{
 StyleBox["October 31, 2016.",
  FontWeight->"Bold"],
 " Bug fix in ",
 StyleBox["mSave",
  FontSlant->"Italic"],
 ": adapters were not saved."
}], "Item"],

Cell[TextData[{
 StyleBox["October 28, 2016.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mExport",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["October 26, 2016.",
  FontWeight->"Bold"],
 " Made ",
 StyleBox["fObjects",
  FontSlant->"Italic"],
 " public."
}], "Item"],

Cell[TextData[{
 StyleBox["October 25, 2016.",
  FontWeight->"Bold"],
 " Added check for seed=0. If it is so, objects which are multiplied by it, \
will not be evaluated."
}], "Item"],

Cell[TextData[{
 StyleBox["October 21, 2016.",
  FontWeight->"Bold"],
 " ",
 StyleBox["mSave",
  FontSlant->"Italic"],
 " now saves to a WDX file."
}], "Item"],

Cell[TextData[{
 StyleBox["October 17, 2016.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mTranscendentalSeed",
  FontSlant->"Italic"],
 " (shortcut for ",
 StyleBox["THomo",
  FontWeight->"Bold"],
 ")."
}], "Item"],

Cell[TextData[{
 StyleBox["October 12, 2016.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mSetSolutions",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["October 11, 2016. ",
  FontWeight->"Bold"],
 "Added ",
 StyleBox["mState",
  FontSlant->"Italic"],
 " (private for now)."
}], "Item"],

Cell[TextData[{
 StyleBox["October 10, 2016.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["mClear",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["October 6, 2016.",
  FontWeight->"Bold"],
 " Added multiplication by transcendental seed."
}], "Item"],

Cell[TextData[{
 StyleBox["September 15, 2016.",
  FontWeight->"Bold"],
 " Not-So-Great-Refactoring."
}], "Item"],

Cell[TextData[{
 StyleBox["September 5-6, 2016.",
  FontWeight->"Bold"],
 " Changed a request system."
}], "Item"],

Cell[TextData[{
 StyleBox["September 2, 2016.",
  FontWeight->"Bold"],
 " Added destructor."
}], "Item"],

Cell[TextData[{
 StyleBox["September 1, 2016.",
  FontWeight->"Bold"],
 " Rewrote ",
 StyleBox["mSave",
  FontSlant->"Italic"],
 " method."
}], "Item"],

Cell[TextData[{
 StyleBox["August 31, 2016.",
  FontWeight->"Bold"],
 " Renamed fields and methods. Added usage messages."
}], "Item"],

Cell[TextData[{
 StyleBox["August 29, 2016. ",
  FontWeight->"Bold"],
 "Initial version with ",
 StyleBox["SetDimension, Request,",
  FontSlant->"Italic"],
 " ",
 StyleBox["Evaluate, ",
  FontSlant->"Italic"],
 "and",
 StyleBox[" Save",
  FontSlant->"Italic"],
 " methods."
}], "Item"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Preamble", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", "TMaster", "}"}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Definition", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"TMaster", ",", "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", "FIELDS", " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]",
     "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Homogeneous", " ", 
       RowBox[{"solution", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"fHomo", ",", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"List", " ", "of", " ", "all", " ", 
        RowBox[{"objects", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "fObjects"}], "\[IndentingNewLine]", "}"}], ",", "\[IndentingNewLine]", 
    "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", "METHODS", " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]",
     "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"--", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"SET", " ", "SOLUTION"}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"--", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"$Method", "[", 
       RowBox[{"mSetSolution", ",", 
        RowBox[{"{", 
         RowBox[{"this", ",", "homo", ",", " ", "var"}], "}"}], ",", 
        "\[IndentingNewLine]", "\[IndentingNewLine]", 
        RowBox[{"(*", " ", 
         RowBox[{
         "Checking", " ", "whether", " ", "we", " ", "can", " ", "set", " ", 
          "a", " ", 
          RowBox[{"solution", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"TrueOrAbort", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"this", "[", "mSolutionSettableQ", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Message", "[", 
             RowBox[{"this", "::", "NoHomoObject"}], "]"}], ";"}]}], 
          "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{"Setting", " ", 
           RowBox[{"it", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"this", "[", "fHomo", "]"}], "[", 
          RowBox[{"mSetSolution", ",", "homo", ",", "var"}], "]"}], ";"}]}], 
       "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}], ",", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"GET", " ", "SOLUTION"}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"$Method", "[", 
       RowBox[{"mGetSolution", ",", 
        RowBox[{"{", 
         RowBox[{"this", ",", "var"}], "}"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"(*", " ", 
         RowBox[{"Checking", " ", "whether", " ", "there", " ", "is", " ", 
          RowBox[{"THomo", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"!", 
            RowBox[{"this", "[", "mSolutionSettableQ", "]"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Message", "[", 
             RowBox[{"this", "::", "NoHomoObject"}], "]"}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"Return", "@", "Null"}]}]}], "\[IndentingNewLine]", "]"}],
          ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{"Getting", " ", "the", " ", 
           RowBox[{"solution", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"this", "[", "fHomo", "]"}], "[", 
          RowBox[{"mGetSolution", ",", "var"}], "]"}]}]}], 
       "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}], ",", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"SOLUTION", " ", "SETTABLE"}], " ", "*)"}], 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"$Method", "[", 
       RowBox[{"mSolutionSettableQ", ",", 
        RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"this", "[", "fHomo", "]"}], "=!=", "None"}]}], 
       "\[IndentingNewLine]", "]"}], ",", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", "EXPORT", " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"--", 
        RowBox[{"--", 
         RowBox[{"--", 
          RowBox[{"--", 
           RowBox[{"--", 
            RowBox[{"--", 
             RowBox[{"--", 
              RowBox[{"--", 
               RowBox[{"--", 
                RowBox[{"--", 
                 RowBox[{"--", 
                  RowBox[{"--", 
                   RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{"--", 
                    RowBox[{
                    "--", "--"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}], " ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"$Method", "[", 
       RowBox[{"mExport", ",", 
        RowBox[{"{", 
         RowBox[{"this", ",", "\[Nu]"}], "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{"ExportToSummerTime", "[", 
         RowBox[{"this", ",", "\[Nu]"}], "]"}]}], "\[IndentingNewLine]", 
       "]"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", "}"}], ",", 
    "\[IndentingNewLine]", "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", "CONSTRUCTOR", " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]",
     "\[IndentingNewLine]", 
    RowBox[{"$Constructor", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"this", ",", "$objects"}], "}"}], ",", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"Module", "[", 
       RowBox[{
        RowBox[{"{", "l", "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{"(*", " ", 
         RowBox[{"Checking", " ", "the", " ", 
          RowBox[{"argument", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"TrueOrAbort", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"Head", "[", "$objects", "]"}], "===", "List"}], "&&", 
            RowBox[{"And", "@@", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{
                RowBox[{"InstanceQ", "[", 
                 RowBox[{"#", ",", "TComputable"}], "]"}], "&"}], "/@", 
               "$objects"}], ")"}]}]}], ",", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Message", "[", 
             RowBox[{
              RowBox[{"this", "::", "BadArgument"}], ",", 
              "\"\<TMaster::$Constructor\>\"", ",", "\"\<Argument \!\(\*
StyleBox[\"objects\",
FontSlant->\"Italic\"]\) has to be a list of TComputable objects.\>\""}], 
             "]"}], ";"}]}], "\[IndentingNewLine]", "]"}], ";", 
         "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
          "Checking", " ", "is", " ", "there", " ", "a", " ", "THomo", " ", 
           RowBox[{"object", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{"l", "=", 
          RowBox[{"Cases", "[", 
           RowBox[{"$objects", ",", 
            RowBox[{"_", "?", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{"InstanceQ", "[", 
                RowBox[{"#", ",", "THomo"}], "]"}], "&"}], ")"}]}]}], "]"}]}],
          ";", "\[IndentingNewLine]", 
         RowBox[{"TrueOrAbort", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{"Length", "@", "l"}], "\[LessEqual]", "1"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Message", "[", 
             RowBox[{
              RowBox[{"this", "::", "BadArgument"}], ",", 
              "\"\<TMaster::$Constructor\>\"", ",", "\"\<\!\(\*
StyleBox[\"objects\",
FontSlant->\"Italic\"]\) list has several THomo instances in it. This is \
forbidden.\>\""}], "]"}], ";"}]}], "\[IndentingNewLine]", "]"}], ";", 
         "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
          "Checking", " ", "that", " ", "all", " ", "sums", " ", "start", " ",
            "from", " ", "the", " ", "given", " ", 
           RowBox[{"node", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{"TrueOrAbort", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"Length", "@", "l"}], "\[Equal]", "0"}], "||", 
            RowBox[{
             RowBox[{
              RowBox[{"Length", "@", "l"}], "\[Equal]", "1"}], "&&", 
             RowBox[{"And", "@@", 
              RowBox[{"(", "\[IndentingNewLine]", 
               RowBox[{
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{"If", "[", 
                   RowBox[{
                    RowBox[{"InstanceQ", "[", 
                    RowBox[{"#", ",", "TSum"}], "]"}], ",", 
                    "\[IndentingNewLine]", 
                    RowBox[{
                    RowBox[{"#", "[", "fHead", "]"}], "===", 
                    RowBox[{"First", "@", "l"}]}], ",", "\[IndentingNewLine]",
                     "True"}], "]"}], ")"}], "&"}], "/@", "$objects"}], 
               "\[IndentingNewLine]", ")"}]}]}]}], ",", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Message", "[", 
             RowBox[{
              RowBox[{"this", "::", "BadArgument"}], ",", 
              "\"\<TMaster::$Constructor\>\"", ",", 
              RowBox[{"\"\<\!\(\*
StyleBox[\"objects\",
FontSlant->\"Italic\"]\) list has TSum objects, that do not have \>\"", "<>", 
               
               RowBox[{"ToString", "@", 
                RowBox[{"First", "@", "l"}]}], "<>", "\"\< as head.\>\""}]}], 
             "]"}], ";"}]}], "\[IndentingNewLine]", "]"}], ";", 
         "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
          "Custom", " ", "objects", " ", "are", " ", "kept", " ", "just", " ",
            "as", " ", "they", " ", 
           RowBox[{"are", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{"Saving", " ", "objects", " ", 
           RowBox[{"list", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"this", "[", "fObjects", "]"}], "=", "$objects"}], ";", 
         "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
           RowBox[{
           "Remembering", " ", "THomo", " ", "also", " ", "separately"}], ",",
            " ", 
           RowBox[{"if", " ", 
            RowBox[{"present", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
         
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Length", "@", "l"}], ">", "0"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"this", "[", "fHomo", "]"}], "=", 
            RowBox[{"First", "@", "l"}]}], ",", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{
             RowBox[{"this", "[", "fHomo", "]"}], "=", "None"}], ";", 
            RowBox[{"Message", "[", 
             RowBox[{"this", "::", "NoTHomoInstance"}], "]"}], ";"}]}], 
          "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{"This", " ", "is", " ", "a", " ", 
           RowBox[{"hack", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
           RowBox[{"Classes", " ", "and", " ", "objects"}], ",", " ", 
           RowBox[{"created", " ", "by", " ", "Objects", " ", "module"}], ",",
            " ", 
           RowBox[{
           "cannot", " ", "be", " ", "used", " ", "as", " ", "heads", " ", 
            "for", " ", "some", " ", 
            RowBox[{"expressions", ".", " ", "If"}], " ", "you", " ", "type", 
            " ", 
            RowBox[{"obj", "[", "stuff", "]"}]}], ",", " ", 
           RowBox[{"and", " ", "this", " ", "obj", " ", 
            RowBox[{"doesn", "'"}], "t", " ", "have", " ", "neither", " ", 
            "method", " ", "nor", " ", "field", " ", "with", " ", "name", 
            " ", "\"\<stuff\>\""}], ",", " ", 
           RowBox[{
            RowBox[{"this", " ", "will", " ", "trigger", " ", "an", " ", 
             RowBox[{"error", ".", " ", "This"}], " ", "is", " ", "quite", 
             " ", "logical", " ", "behaviour"}], ";", " ", 
            RowBox[{
            "it", " ", "allows", " ", "us", " ", "to", " ", "avoid", " ", 
             RowBox[{"errors", "."}]}]}]}], " ", "*)"}], 
         "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
           RowBox[{
           "But", " ", "this", " ", "behaviour", " ", "forbids", " ", "us", 
            " ", "to", " ", "do", " ", "the", " ", 
            RowBox[{"following", ".", " ", "We"}], " ", "want"}], ",", " ", 
           RowBox[{"that", " ", 
            RowBox[{"TMasters", "::", "mEvaluate"}], " ", "returns", " ", 
            "results", " ", "as", " ", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"J", "[", 
               RowBox[{"2", "-", "\[Epsilon]"}], "]"}], "\[Rule]", 
              "\[Ellipsis]"}], "}"}]}], ",", " ", 
           RowBox[{
           "but", " ", "there", " ", "is", " ", "no", " ", "way", " ", "to", 
            " ", "do", " ", 
            RowBox[{"it", "!"}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
           RowBox[{"Because", " ", "of", " ", "that"}], ",", " ", 
           RowBox[{"we", " ", "remove", " ", "the", " ", "rule"}], ",", " ", 
           RowBox[{"added", " ", "by", " ", "Objects", " ", "module"}], ",", 
           " ", 
           RowBox[{"so", " ", "that", " ", "combination", " ", "like", " ", 
            RowBox[{"J", "[", 
             RowBox[{"2", "-", "\[Epsilon]"}], "]"}], " ", "will", " ", "be", 
            " ", 
            RowBox[{"allowed", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
         
         RowBox[{
          RowBox[{"this", "[", 
           RowBox[{"f_", ",", "___"}], "]"}], "=."}], ";"}]}], 
       "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
     "\[IndentingNewLine]", "]"}], ",", "\[IndentingNewLine]", 
    "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{"CLASS", " ", "USAGE", " ", "MESSAGE"}], " ", "*)"}], 
    "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]",
     "\[IndentingNewLine]", 
    RowBox[{
    "\"\<Usage\>\"", "\[Rule]", 
     "\"\<TMaster is a class, representing a master-integral.\>\""}], ",", 
    "\[IndentingNewLine]", "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"(*", " ", 
     RowBox[{"OBJECT", " ", "MESSAGES"}], " ", "*)"}], "\[IndentingNewLine]", 
    
    RowBox[{"(*", " ", 
     RowBox[{
     "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "===", "===", "===", "===", "===", "===", "===",
       "===", "===", "===", "===", "=="}], " ", "*)"}], "\[IndentingNewLine]",
     "\[IndentingNewLine]", 
    RowBox[{"\"\<ObjectMessages\>\"", "\[Rule]", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
       "\"\<usage\>\"", "\[Rule]", "\"\<%object% is an instance of class \!\(\*
StyleBox[\"TMaster\",
FontSlant->\"Italic\"]\). For more information, use ?\!\(\*
StyleBox[\"TMaster\",
FontSlant->\"Italic\"]\).\n\nTo access fields or methods of the object, use \
square brackets:\n\tobj[\!\(\*
StyleBox[\"method\",
FontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"args\",
FontSlant->\"Italic\"]\)],\nwhere \!\(\*
StyleBox[\"args\",
FontSlant->\"Italic\"]\) are method arguments.\>\""}], ",", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"\"\<NoTHomoInstance\>\"", "\[Rule]", "\"\<Provided \!\(\*
StyleBox[\"objects\",
FontSlant->\"Italic\"]\) list does not contain a THomo instance. If you are \
creating a TMaster manually, this is just a reminder for you; but if this \
message occured during automatical object construction, this is most likely \
an error.\>\""}], ",", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{
       "\"\<NoHomoObject\>\"", "\[Rule]", 
        "\"\<This master does not contain a THomo object. All operations \
including it (like getting transcendental seed or setting the homogeneous \
solution) are not permitted.\>\""}]}], "\[IndentingNewLine]", "}"}]}]}], 
   "\[IndentingNewLine]", "]"}], ";"}]], "Code"]
}, Open  ]],

Cell[CellGroupData[{

Cell["End", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}]], "Code"]
}, Closed]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1920, 1007},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
PrivateNotebookOptions->{"FileOutlineCache"->False},
TrackCellChangeTimes->False,
Magnification:>1.25 Inherited,
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[StyleDefinitions -> FrontEnd`FileName[{
        ParentDirectory[
         ParentDirectory[
          ParentDirectory[]]]}, "Stylesheet.nb", CharacterEncoding -> 
       "WindowsANSI"]]]}, Visible -> False, FrontEndVersion -> 
  "10.4 for Linux x86 (64-bit) (April 11, 2016)", StyleDefinitions -> 
  "PrivateStylesheetFormatting.nb"]
]

