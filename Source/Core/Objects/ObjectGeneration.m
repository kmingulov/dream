(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



{CreateMaster,CreateMasters};


Begin["`Private`"];


CreateMaster::usage="CreateMaster[\!\(\*
StyleBox[\"obj\", \"TI\"]\), {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(0\)]\), {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(1\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(1\)]\)}, {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(2\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(2\)]\)}, \[Ellipsis]}, \!\(\*
StyleBox[\"var\", \"TI\"]\)] creates a new container of TMaster class using the provided lowering dimensional recurrence relation.
CreateMaster[\!\(\*
StyleBox[\"obj\", \"TI\"]\), {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(01\)]\), \!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(02\)]\), \[Ellipsis], \!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(0  n\)]\), {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(1\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(1\)]\)}, {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(2\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(2\)]\)}, \[Ellipsis]}, \!\(\*
StyleBox[\"var\", \"TI\"]\)] creates a new container of TMaster class using the provided lowering dimensional recurrence relation of \!\(\*
StyleBox[\"n\", \"TI\"]\)-th order.
CreateMaster[\!\(\*
StyleBox[\"obj\", \"TI\"]\), {{\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(1\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(1\)]\)}, {\!\(\*SubscriptBox[
StyleBox[\"c\", \"TI\"], \(2\)]\), \!\(\*SubscriptBox[
StyleBox[\"m\", \"TI\"], \(2\)]\)}, \[Ellipsis]}, \!\(\*
StyleBox[\"var\", \"TI\"]\)] creates a new container of TMaster class using expression of the master-integral in terms of other integrals.";


CreateMaster::BadSymbol="`1` is a bad symbol, some definitions are attached to it.";


CreateMaster::BadMasters="Some of given master-integrals are not instances of TMaster. Masters in question: `1`.";


CreateMaster::DivergentObjects="Failed to create master `1`. Object `2` appears to be divergent.";


CreateMaster::BadAppend="`1` is not valid value for the Append option: it should be either a list of computable objects or a single object.";


Options[CreateMaster]={Append->{}};


CreateMaster[obj_Symbol,l:{{_,_Symbol}...},var_Symbol,OptionsPattern[]]:=
Module[{name,masters,objs},

(* Checking the Append option. *)
TrueOrAbort[
MatchQ[OptionValue[Append],(_?(InstanceQ[#,TComputable]&))|({___?(InstanceQ[#,TComputable]&)})],
Message[CreateMaster::BadAppend,OptionValue[Append]];
];

(* Checking whether obj has some definitions. *)
TrueOrAbort[
UpValues[obj]==={}&&DownValues[obj]==={}&&Attributes[obj]==={},
Message[CreateMaster::BadSymbol,obj];
];

(* Getting name of the object. *)
name=SymbolName[obj];

(* Master-integrals. *)
masters=l[[All,2]];

(* Checking that masters are actually master-integrals. *)
TrueOrAbort[
And@@(InstanceQ[#,TMaster]&/@masters),
Message[CreateMaster::BadMasters,
StringJoin@@(
ToString/@Riffle[DeleteCases[masters,_?(InstanceQ[#,TMaster]&)],", "]
)
];
];

(* Creating TTimes objects, which will multiply masters by their prefactors. *)
objs={};
Function[{coef,m},Module[{pf},
(* Checking whether coefficient is non-zero. *)
If[coef=!=0,
(* Creating a prefactor object. *)
pf=ONew[
Symbol[name<>"$"<>SymbolName[m]<>"$pf"],
TExpr,coef,var
];
(* Iterating over master's objects. *)
AppendTo[objs,
ONew[
Symbol[name<>"$"<>SymbolName[#]],
TTimes,
{pf,#}
]
]&/@m[fObjects];
]
]]@@@l;

(* Creating a new master-integral and returning it. *)
Quiet[
ONew[obj,TMaster,
Flatten@{objs,OptionValue[Append]}
],
{obj::NoTHomoInstance}
]
];


CreateMaster[obj_Symbol,{coef0_,l:{_,_Symbol}...},var_Symbol,OptionsPattern[]]:=
Module[{name,coefs,masters,n,homo,sums,adapters},

(* Checking the Append option. *)
TrueOrAbort[
MatchQ[OptionValue[Append],(_?(InstanceQ[#,TComputable]&))|({___?(InstanceQ[#,TComputable]&)})],
Message[CreateMaster::BadAppend,OptionValue[Append]];
];

(* Checking whether obj has some definitions. *)
TrueOrAbort[
UpValues[obj]==={}&&DownValues[obj]==={}&&Attributes[obj]==={},
Message[CreateMaster::BadSymbol,obj];
];

(* Debug information. *)
Print[obj->{l}[[All,2]]];

(* Getting name of the object. *)
name=SymbolName[obj];

(* Preparing lists. *)
coefs={l}[[All,1]];
masters={l}[[All,2]];
n=Length@coefs;

(* Checking masters. *)
TrueOrAbort[
And@@(InstanceQ[#,TMaster]&/@masters),
Message[CreateMaster::BadMasters,
StringJoin@@(
ToString/@Riffle[DeleteCases[masters,_?(InstanceQ[#,TMaster]&)],", "]
)
];
];

(* Creating THomo object. *)
homo=ONew[Symbol[name<>"$h"],THomo,coef0,var];

(* Lists for sums and adapters. *)
sums={};
adapters={};

(* Creating sums. *)
Function[{i},
If[coefs[[i]]=!=0,
Module[{adapter,aexpr,acert},

(* Creating the adapter. *)
aexpr=coefs[[i]]/coef0;
If[RationalQ[aexpr,var],
(* ADAPTER IS RATIONAL *)
(* Using TExpr then. *)
adapter=ONew[Symbol[name<>"$"<>ToString@masters[[i]]<>"$adapter"],
TExpr,
aexpr,var
],

(* ADAPTER IS NON-RATIONAL *)
(* Using THomo instead. *)
acert=FindCertificate[aexpr,var];
adapter=ONew[Symbol[name<>"$"<>ToString@masters[[i]]<>"$adapter"],
THomo,
acert,var
];
adapter[mSetSolution,aexpr,var];
];
(* Saving the adapter. *)
AppendTo[adapters,adapter];

(* A bit dummy way to do what we want. *)
(* We want to create an appropriate object, which does not diverge. To do this, we are trying to create an object and then check whether it converges. If false, we are trying again with different summation direction. *)

With[{sym=Symbol[name<>"$"<>ToString@#]},
CheckAbort[
(* CREATING OBJECT WITH DEFAULT DIRECTION *)
(* If direction is bad, we will get an abort here. *)
Quiet[
ONew[sym,TSum,homo,adapter,#,"+"];
Print["\t",name," \[Rule] ",#,": summation direction +"],
(* Muting several messages, which can appear. *)
{sym::DivergentSum,AsymptoticsSimplify::Divergence,ONew::ErrorDuringConstruction}
],

(* CREATING OBJECT WITH OPPOSITE DIRECTION *)
(* If we are here, it means that we failed to create object with default direction. Creating with opposite. *)
(* Here we have CheckAbort and Quiet again. *)
CheckAbort[
Quiet[
ONew[sym,TSum,homo,adapter,#,"-"];
Print["\t",name," \[Rule] ",#,": summation direction -"],
(* Muting several messages, which can appear. *)
{sym::DivergentSum,AsymptoticsSimplify::Divergence,ONew::ErrorDuringConstruction}
],

(* FAILED TO CREATE THE OBJECT *)
(* Failed in both cases. Object cannot be created. *)
(* Deleting already created objects. *)
ODelete[sums];ODelete[adapters];ODelete[homo];
(* Printing the message. *)
Message[CreateMaster::DivergentObjects,obj,sym];
(* Throwing abort. *)
Abort[];
];
];

(* Appending object to the sums list. *)
AppendTo[sums,sym];
]&/@masters[[i]][fObjects]

]
]]/@Range[n];

(* Creating new master and returning it. *)
ONew[obj,TMaster,
Flatten@{sums,homo,OptionValue[Append]}
]

];


CreateMaster[obj_Symbol,{coefs0__,l:{_,_Symbol}...},var_Symbol,OptionsPattern[]]:=
Module[{name,coefs,masters,n,blockn,mat,cert,icert,sums,adapters,objs},

(* Checking the Append option. *)
TrueOrAbort[
MatchQ[OptionValue[Append],(_?(InstanceQ[#,TComputable]&))|({___?(InstanceQ[#,TComputable]&)})],
Message[CreateMaster::BadAppend,OptionValue[Append]];
];

(* Checking whether obj has some definitions. *)
TrueOrAbort[
UpValues[obj]==={}&&DownValues[obj]==={}&&Attributes[obj]==={},
Message[CreateMaster::BadSymbol,obj];
];

(* Debug information. *)
Print[obj->{l}[[All,2]]];

(* Getting name of the object. *)
name=SymbolName[obj];

(* Size of the block. *)
blockn=Length@{coefs0};

(* Prefactors of master-integrals. *)
coefs={l}[[All,1]];
(* Master-integrals. *)
masters={l}[[All,2]];
(* Number of them. *)
n=Length@coefs;

(* Checking that masters are actually master-integrals. *)
TrueOrAbort[
And@@(InstanceQ[#,TMaster]&/@masters),
Message[CreateMaster::BadMasters,
StringJoin@@(
ToString/@Riffle[DeleteCases[masters,_?(InstanceQ[#,TMaster]&)],", "]
)
];
];

(* Creating TCert objects. *)
mat={{coefs0},Sequence@@IdentityMatrix[{blockn-1,blockn}]};
cert=ONew[Symbol[name<>"$c"],TCert,mat,var];
icert=ONew[Symbol[name<>"$ic"],TCert,mat,var,True];

(* Lists for sums and adapters. *)
sums={};
adapters={};

(* Creating sums. *)
Function[{i},
If[coefs[[i]]=!=0,
Module[{adapter,aexpr,acert},

(* Creating the adapter. *)
aexpr=coefs[[i]]/Last[{coefs0}];
If[RationalQ[aexpr,var],
(* ADAPTER IS RATIONAL *)
(* Using TExpr then. *)
adapter=ONew[Symbol[name<>"$"<>ToString@masters[[i]]<>"$adapter"],
TExpr,
aexpr,var
],

(* ADAPTER IS NON-RATIONAL *)
(* Using THomo instead. *)
acert=FindCertificate[aexpr,var];
adapter=ONew[Symbol[name<>"$"<>ToString@masters[[i]]<>"$adapter"],
THomo,
acert,var
];
adapter[mSetSolution,aexpr,var];
];
(* Saving the adapter. *)
AppendTo[adapters,adapter];

(* A bit dummy way to do what we want. *)
(* We want to create an appropriate object, which does not diverge. To do this, we are trying to create an object and then check whether it converges. If false, we are trying again with different summation direction. *)

With[{sym=Symbol[name<>"$"<>ToString@#]},
CheckAbort[
(* CREATING OBJECT WITH DEFAULT DIRECTION *)
(* If direction is bad, we will get an abort here. *)
Quiet[
ONew[sym,TSum,cert,adapter,#,"+",icert];
Print["\t",name," \[Rule] ",#,": summation direction +"],
(* Muting several messages, which can appear. *)
{sym::DivergentSum,AsymptoticsSimplify::Divergence,ONew::ErrorDuringConstruction}
],

(* CREATING OBJECT WITH OPPOSITE DIRECTION *)
(* If we are here, it means that we failed to create object with default direction. Creating with opposite. *)
(* Here we have CheckAbort and Quiet again. *)
CheckAbort[
Quiet[
ONew[sym,TSum,cert,adapter,#,"-",icert];
Print["\t",name," \[Rule] ",#,": summation direction -"],
(* Muting several messages, which can appear. *)
{sym::DivergentSum,AsymptoticsSimplify::Divergence,ONew::ErrorDuringConstruction}
],

(* FAILED TO CREATE THE OBJECT *)
(* Failed in both cases. Object cannot be created. *)
(* Deleting already created objects. *)
ODelete[sums];ODelete[adapters];ODelete[cert];
(* Printing the message. *)
Message[CreateMaster::DivergentObjects,obj,sym];
(* Throwing abort. *)
Abort[];
];
];

(* Appending object to the sums list. *)
AppendTo[sums,sym];
]&/@masters[[i]][fObjects]

]
]]/@Range[n];

(* Creating a new master-integral. *)
Quiet[
ONew[obj,TMaster,
Flatten@{sums,OptionValue[Append]}
],
{obj::NoTHomoInstance}
];

(* Creating shift masters and shift objects. *)
objs={obj};
Function[{shift},
Module[{shiftObjects},
(* Iterating over objects in the newly created master-integral and creating shifted objects. *)
shiftObjects=Function[{o},
ONew[Symbol[
(* Inserting s# (where # is a shift) into the name of the existing object. *)
StringInsert[SymbolName[o],"s"<>ToString[shift],StringLength[name]+1]
],TShift,o,shift]
]/@obj[fObjects];
(* Creating shifted master. *)
With[{master=Symbol[name<>"s"<>ToString[shift]]},
(* This master-integral surely does not contain a THomo instance, so we suspend the message. *)
AppendTo[objs,Quiet[
ONew[master,TMaster,shiftObjects],
master::NoTHomoInstance
]];
]
];
]/@Range[1,blockn-1];

(* Returning the master-integrals. *)
objs
];


CreateMasters::usage="CreateMasters[\!\(\*
StyleBox[\"nameprefix\", \"TI\"]\)\!\(\*
StyleBox[\",\",\nFontSlant->\"Italic\"]\)\!\(\*
StyleBox[\" \",\nFontSlant->\"Italic\"]\)\!\(\*
StyleBox[\"mat\", \"TI\"]\)\!\(\*
StyleBox[\",\",\nFontSlant->\"Italic\"]\)\!\(\*
StyleBox[\" \",\nFontSlant->\"Italic\"]\)\!\(\*
StyleBox[\"var\", \"TI\"]\)] creates a new container of class TMasters with master-integrals specified by matrix \!\(\*
StyleBox[\"mat\", \"TI\"]\).";


CreateMasters::SingularMatrix="The given matrix has blocks. I tried to perform a reduction, but obtained a singular matrix. The reason can be that the matrix is not ordered (what was assumed).";


CreateMasters::BadAppend="`1` is not valid value for the Append option: it should be a list of rules J\[MediumSpace]->\[MediumSpace]objs, where J is a master's name and objs is either a list of computable objects or a single object.";


Options[CreateMasters]={Append->{}};


CreateMasters[nameprefix_String,mat_?SquareMatrixQ,var_Symbol,OptionsPattern[]]:=
With[{obj=Symbol[nameprefix],append=OptionValue[Append]},
Module[{j,js,blocks,T,Ti,M},

(* Checking the Append option. *)
TrueOrAbort[
MatchQ[append,
{(_Symbol->(_?(InstanceQ[#,TComputable]&))|({___?(InstanceQ[#,TComputable]&)}))...}],
Message[CreateMasters::BadAppend,append];
];

(* Checking whether obj has some definitions. *)
TrueOrAbort[
UpValues[obj]==={}&&DownValues[obj]==={}&&Attributes[obj]==={},
Message[CreateMaster::BadSymbol,obj];
];

(* Creating container. *)
j=ONew[obj,TMasters,{}];

(* Getting blocks. *)
blocks=DiagonalBlocks[mat];
Print["Diagonal blocks: ",Sequence@@Riffle[blocks,", "],"."];

(* Preparing names for masters. *)
js=Flatten[Function[{block},
{Symbol[nameprefix<>ToString@First@block],
Symbol[nameprefix<>ToString@First@block<>"s"<>ToString@#]&/@Range[Length@block-1]}
]/@blocks];

(* Creating substitution matrix. *)
Print["Reducing diagonal blocks\[Ellipsis]"];
T=ReduceDiagonalBlocks[mat,var];
TrueOrAbort[Det[T]=!=0,
Message[CreateMasters::SingularMatrix];ODelete[j];
];
Ti=Inverse[T];
(* Computing new matrix. *)
M=(T/.var->var+1).mat.Ti;
M=MapAt[
If[RationalQ[#,var],
Simplify[#],
(* FullSimplify is needed for simplification of gamma functions. *)
FullSimplify[#]
]&,M,{All,All}];
Print["Done!"];

(* Iterating through blocks and creating masters. *)
Function[{block},
(* Printing. *)
Print["Block ",block];

(* Checking whether it is a matrix block. *)
If[Length[block]==1,

(* SCALAR BLOCK *)
(* Everything is simple then. *)
Module[{id=First[block],coefs},
(* Getting the coefficients. *)
coefs=Transpose@{M[[id,1;;id-1]],js[[1;;id-1]]};
coefs=DeleteCases[coefs,{0,_}];
(* Adding the master-integral. *)
j[mAddMaster,
CreateMaster[
(* Master-integral name. *)
js[[id]],
(* Recurrence relation. *)
{M[[id,id]],Sequence@@coefs},
(* Variable. *)
var,
(* Additional objects (if any). *)
Append->If[MemberQ[append,js[[id]]->_],js[[id]]/.append,{}]
]
];
],

(* MATRIX BLOCK *)
(* Not so simple. *)
Module[{l,mis},
(* Size of the matrix block. *)
(*n=Length@block;*)
(* First (n-1) equations in this block a trivial identites (we reduced system to this form). *)
(* So we are interested in the last equation which is a recurrence relation of n-th order. *)
l=M[[Last@block]];

(* Creating a master-integral together with its shifted versions. *)
mis=CreateMaster[
(* Master-integral name. *)
js[[First[block]]],
(* Recurrence relation. *)
Join[
l[[Last@block;;First@block;;-1]],
DeleteCases[
Transpose@{l[[1;;First@block-1]],js[[1;;First@block-1]]},
{0,_}
]
],
(* Variable. *)
var,
(* Additional objects (if any). *)
Append->If[MemberQ[append,js[[First[block]]]->_],js[[First[block]]]/.append,{}]
];
j[mAddMaster,#]&/@mis;

(* Adding original master-integrals, expressed in terms of shifted ones. *)
Function[{id},
j[mAddMaster,
CreateMaster[Symbol[nameprefix<>ToString@id],
DeleteCases[Transpose@{Ti[[id]],js},{0,_}],
var
]
];
]/@Rest[block];
]
];
]/@blocks;

(* Done. *)
{j,T,M}
]
];


End[];
