(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



{TExpr};


Begin["`Private`"];


CNew[TExpr->TComputable,

(* ================================================================================ *)
(* FIELDS *)
(* ================================================================================ *)

{
(* Expression, which will be computed. *)
fExpr,
(* Expression's certificate. *)
fCert,
(* Expression's asymptotics. *)
fAsymP,fAsymN,
(* Expression's singularities. *)
fSingP,fSingN
},

(* ================================================================================ *)
(* METHODS *)
(* ================================================================================ *)

{

(* ------------------------------------------------------------ *)
(* WORKING PARAMETERS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE For TExpr working parameters are equal to required parameters. *)

$Method[mWorkingParameters,{this,type},

this[mRequiredParameters,type]

],

(* ------------------------------------------------------------ *)
(* SEND REQUESTS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE In case of TExpr, this method does nothing. *)

$Method[mSendRequests,{this},
Null
],

(* ------------------------------------------------------------ *)
(* SEND ORDER REQUESTS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE In case of TExpr, this method does nothing. *)

$Method[mSendOrderRequests,{this},
Null
],

(* ------------------------------------------------------------ *)
(* ASYMPTOTICS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mAsymptotics,{this,type},

Which[
type==="+",{this[fAsymP]},
type==="-",{this[fAsymN]}
]

],

(* ------------------------------------------------------------ *)
(* COMPUTE BY TYPE *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mComputeByType,{this,type},

Module[{n,p,o,expr,k,sd,sings,terms},
(* Getting working parameters. *)
{n,p,o}=this[mWorkingParameters,type];
If[n===Null||p===Null||o===Null,
Return@{};
];

(* Debug. *)
this[mDebug,FontColor->Blue,"Calculating (",type,") terms with working parameters ",{n,p,o},"."];

(* Calculating the expression. *)
expr=this[fExpr][this[fVar]+If[type==="+",k,-k]];
(* Its series. *)
sd=ZeroSeries[expr,{this[fEps],o}];
(* Getting the singularities. *)
sings=If[type==="+",this[fSingP],this[fSingN]];
sings=Cases[sings,{_,_?Negative}];
If[sings!={},
this[mDebug,FontColor->Blue,"Expression singularities: ",sings,"."];
];

(* Calculating terms. *)
CoreEval[Off[Power::infy]];
terms=N[#,p]&/@CoreTable@@{sd,{k,0,n-1}};
CoreEval[On[Power::infy]];
(* Now fixing singularities points. *)
Function[{sing},
(* Checking that this point is present at our array. *)
If[0<=sing<Length@terms,
(* Fixing. *)
terms[[sing+1]]=ZeroNSeries[expr/.k->sing,{this[fEps],o},p];
];
]/@(First/@sings);

(* Returning the result. *)
terms
]

],

(* ------------------------------------------------------------ *)
(* SET DIM *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* We reimplement this method, in order to compute and save singularities. *)

$Method[mSetDim,{this,var},

Module[{k},
this[$Super,var];

this[fSingP]=Cases[
FindSingularities[this[fExpr][this[fVar]+k/.this[fEps]->0],k],
{_?(#>=0&),_}
];

this[fSingN]=Cases[
FindSingularities[this[fExpr][this[fVar]-k/.this[fEps]->0],k],
{_?(#>=0&),_}
];
];

],

(* ------------------------------------------------------------ *)
(* LOWEST ORDER *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mLowestOrder,{this,type},

Module[{sings,points,left},
(* Getting the singularities. *)
sings=If[type==="+",this[fSingP],this[fSingN]];
(* If there are no singularities, the result is simple. *)
If[sings==={},
Return@{}
];
(* Preparing the array. *)
points=First/@sings;
left=Complement[Range[0,Max[points]+1],points];
OrderListSimplify[
Sort@Join[sings,{#,0}&/@left]
]
]

]

},

(* ================================================================================ *)
(* CONSTRUCTOR *)
(* ================================================================================ *)

$Constructor[{this,expr,var},

(* Arguments check. *)
TrueOrAbort[Head[var]===Symbol,
Message[this::BadArgument,"TExpr::$Constructor","Argument \!\(\*
StyleBox[\"var\",\nFontSlant->\"Italic\"]\) has to be a Symbol."]
];
TrueOrAbort[Head[expr]=!=List&&With[{vars=ExprVars[expr]},
vars==={var}||vars==={}
],
Message[this::BadArgument,"TExpr::$Constructor","Argument \!\(\*
StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) has to be a scalar expression, depending only on \!\(\*
StyleBox[\"var\",\nFontSlant->\"Italic\"]\)."]
];

(* Parent constructor. *)
this[$Super];

(* Saving expression. *)
this[fExpr]=ExprToFunction[expr,{var}];

(* Computing its certificate and asymptotics. *)
this[fCert]=FindCertificate[expr,var];
this[fAsymP]=FindAsymptotics[this[fCert],var->\[Infinity]];
this[fAsymN]=FindAsymptotics[this[fCert],var->\[Infinity]];
this[fCert]=ExprToFunction[this[fCert],{var}];

(* Default values. *)
this[fSingP]=this[fSingN]={};

],

(* ================================================================================ *)
(* CLASS USAGE MESSAGE *)
(* ================================================================================ *)

"Usage"->"TExpr is a derived class of TComputable, representing a symbolic expression.",

(* ================================================================================ *)
(* OBJECT MESSAGES *)
(* ================================================================================ *)

"ObjectMessages"->{}
];


End[];
