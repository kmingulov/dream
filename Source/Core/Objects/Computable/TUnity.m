(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



{TUnity};


Begin["`Private`"];


CNew[TUnity->TComputable,

(* ================================================================================ *)
(* FIELDS *)
(* ================================================================================ *)

{
},

(* ================================================================================ *)
(* METHODS *)
(* ================================================================================ *)

{

(* ------------------------------------------------------------ *)
(* WORKING PARAMETERS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE For TUnity working parameters are equal to required parameters. *)

$Method[mWorkingParameters,{this,type},
this[mRequiredParameters,type]
],

(* ------------------------------------------------------------ *)
(* SEND REQUESTS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE In case of TUnity, this method does nothing. *)

$Method[mSendRequests,{this},
Null
],

(* ------------------------------------------------------------ *)
(* SEND ORDER REQUESTS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

(* NOTE In case of TUnity, this method does nothing. *)

$Method[mSendOrderRequests,{this},
Null
],

(* ------------------------------------------------------------ *)
(* ASYMPTOTICS *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mAsymptotics,{this,type},
{{1,0,0}}
],

(* ------------------------------------------------------------ *)
(* COMPUTE BY TYPE *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mComputeByType,{this,type},

Module[{n,p,o},
(* Getting working parameters. *)
{n,p,o}=this[mWorkingParameters,type];
If[n===Null||p===Null||o===Null,
Return@{};
];

(* Computing. *)
If[this[fDimensions]==={},
(* Scalar case. *)
ConstantArray[ZeroNSeries[1,{this[fEps],o},p],{n}],
(* Matrix case. *)
ConstantArray[IdentityMatrix[this[fDimensions]]/. 1->ZeroNSeries[1,{this[fEps],o},p],{n}]
]
]

],

(* ------------------------------------------------------------ *)
(* LOWEST ORDER *)
(* (TComputable (re)implemented method) *)
(* ------------------------------------------------------------ *)

$Method[mLowestOrder,{this,type},
{}
]

},

(* ================================================================================ *)
(* CONSTRUCTOR *)
(* ================================================================================ *)

$Constructor[{this,dims->{}},
this[$Super,dims];
],

(* ================================================================================ *)
(* CLASS USAGE MESSAGE *)
(* ================================================================================ *)

"Usage"->"TUnity is a derived class of TComputable, which always evaluates to 1.",

(* ================================================================================ *)
(* OBJECT MESSAGES *)
(* ================================================================================ *)

"ObjectMessages"->{}
];


End[];
