Notebook[{

Cell[CellGroupData[{
Cell["Core.Certificates", "Title"],

Cell[TextData[{
 "The module ",
 StyleBox["Certificates",
  FontWeight->"Bold"],
 " is created to deal with certificates of functions. In this module, they \
are symbolic expressions (not pure functions)."
}], "Text"],

Cell[TextData[{
 "The ",
 StyleBox["certificate",
  FontWeight->"Bold"],
 " of the function ",
 Cell[BoxData[
  FormBox["f", TraditionalForm]]],
 " is:"
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", "n", "]"}], "=", 
  FractionBox[
   RowBox[{"f", "[", 
    RowBox[{"n", "+", "1"}], "]"}], 
   RowBox[{"f", "[", "n", "]"}]]}]], "DisplayFormula",
 TextAlignment->Center],

Cell[CellGroupData[{

Cell["Change log", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["November 15, 2017.",
  FontWeight->"Bold"],
 " Dropping zeroes in ",
 StyleBox["FindSingularities",
  FontSlant->"Italic"],
 " for matrices, if there are numeric elements (which means, that certificate \
is not a zero at this point)."
}], "Item"],

Cell[TextData[{
 StyleBox["May 22, 2017.",
  FontWeight->"Bold"],
 " Adapted usage-messages to the documentation."
}], "Item"],

Cell[TextData[{
 StyleBox["April 7, 2017.",
  FontWeight->"Bold"],
 " Now ",
 StyleBox["FindSingularities",
  FontSlant->"Italic"],
 " works with matrices."
}], "Item"],

Cell[TextData[{
 StyleBox["February 14, 2017.",
  FontWeight->"Bold"],
 " Now singularities of rational powers can be found too."
}], "Item"],

Cell[TextData[{
 StyleBox["November 22, 2016.",
  FontWeight->"Bold"],
 " Not a package anymore. Renamed functions."
}], "Item"],

Cell[TextData[{
 StyleBox["August 22, 2016.",
  FontWeight->"Bold"],
 " Changed the definition a bit."
}], "Item"],

Cell[TextData[{
 StyleBox["April 1, 2015.",
  FontWeight->"Bold"],
 " Small optimizations (removed Module)."
}], "Item"],

Cell[TextData[{
 StyleBox["November 14-22, 2014.",
  FontWeight->"Bold"],
 " Crucial perfomance and bug fixes in ",
 StyleBox["CertificateSingularities",
  FontSlant->"Italic"],
 " method:"
}], "Item"],

Cell[CellGroupData[{

Cell["Added Factor[] call in CertificateSingularities.", "Subitem"],

Cell["\<\
CertificateSingularities can be called on a list of factors (this can be \
helpful, because CertificateSingularities is often called with the same \
expression).\
\>", "Subitem"],

Cell["Changed _Symbol precondition to good _?SymbolQ.", "Subitem"]
}, Open  ]],

Cell[TextData[{
 StyleBox["October 17, 2014.",
  FontWeight->"Bold"],
 " Including borders for ",
 StyleBox["Range",
  FontSlant->"Italic"],
 " parameter of ",
 StyleBox["CertificateSingularities[]",
  FontSlant->"Italic"],
 " method."
}], "Item"],

Cell[TextData[{
 StyleBox["September 19, 2014.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["ExpandAllRule",
  FontSlant->"Italic"],
 " in the last case in ",
 StyleBox["CertificateSingularities[]",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["August 12, 2014.",
  FontWeight->"Bold"],
 " Added ",
 StyleBox["CertificateSingularities[]",
  FontSlant->"Italic"],
 " function. Added error message in ",
 StyleBox["CalculateCertificate[]",
  FontSlant->"Italic"],
 "."
}], "Item"],

Cell[TextData[{
 StyleBox["August 9, 2014.",
  FontWeight->"Bold"],
 " Initial release."
}], "Item"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Preamble", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"{", "\[IndentingNewLine]", 
   RowBox[{"FindCertificate", ",", "FindSingularities"}], 
   "\[IndentingNewLine]", "}"}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Begin", "[", "\"\<`Private`\>\"", "]"}], ";"}]], "Code"]
}, Open  ]],

Cell[CellGroupData[{

Cell["FindCertificate", "Section"],

Cell[TextData[{
 StyleBox["FindCertificate",
  FontWeight->"Bold"],
 " finds the certificate of the given expression. This function takes into \
account these particular cases:"
}], "Text"],

Cell[CellGroupData[{

Cell[TextData[{
 "For Gamma function ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalGamma]", "(", 
    RowBox[{"a", "+", "bx"}], ")"}], TraditionalForm]]],
 " certificate is Pochhammer symbol ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    RowBox[{"(", 
     RowBox[{"a", "+", 
      RowBox[{"b", " ", "x"}]}], ")"}], "b"], TraditionalForm]]],
 "."
}], "Item"],

Cell[TextData[{
 "If function doesn\[CloseCurlyQuote]t depend on ",
 Cell[BoxData[
  FormBox["x", TraditionalForm]]],
 ", the certificate of it (as the function of ",
 Cell[BoxData[
  FormBox["x", TraditionalForm]]],
 ") is simply 1."
}], "Item"],

Cell[TextData[{
 "For polynomial ",
 Cell[BoxData[
  FormBox[
   RowBox[{"P", "[", "x", "]"}], TraditionalForm]]],
 " certificate can be found in a straightforward way as ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{"P", "[", 
     RowBox[{"x", "+", "1"}], "]"}], 
    RowBox[{"P", "[", "x", "]"}]], TraditionalForm]]],
 ". For this case function Cancel[] is used to simplify the final result."
}], "Item"],

Cell[TextData[{
 "For any other case ",
 Cell[BoxData[
  FormBox[
   RowBox[{"f", "[", "x", "]"}], TraditionalForm]]],
 " certificate is found as ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{"f", "[", 
     RowBox[{"x", "+", "1"}], "]"}], 
    RowBox[{"f", "[", "x", "]"}]], TraditionalForm]]],
 "."
}], "Item"]
}, Open  ]],

Cell["\<\
By factoring the given expression, one can find certificate for each factor. \
Their product is the resulting certificate of the whole expression. \
\>", "Text"],

Cell[CellGroupData[{

Cell["Messages", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindCertificate", "::", "usage"}], "=", "\"\<FindCertificate[\!\(\*
StyleBox[\"expr\", \"TI\"]\), \!\(\*
StyleBox[\"x\", \"TI\"]\)] finds a certificate of the given expression \!\(\*
StyleBox[\"expr\", \"TI\"]\).\nFindCertificate[\!\(\*
StyleBox[\"l\", \"TI\"]\):{\[Ellipsis]}, \!\(\*
StyleBox[\"x\", \"TI\"]\)] finds asymptotics of the expression by its list of \
factors \!\(\*
StyleBox[\"l\", \"TI\"]\).\>\""}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FindCertificate", "::", "BadVariable"}], "=", 
  "\"\<`1` isn't a valid variable.\>\""}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Code", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindCertificate", "[", 
    RowBox[{"factors_List", ",", "var_Symbol", ",", 
     RowBox[{"funct_:", "FunctionExpand"}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"Factor", "@", 
    RowBox[{"(", 
     RowBox[{"Times", "@@", 
      RowBox[{"Power", "@@@", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Replace", "[", 
             RowBox[{"#1", ",", 
              RowBox[{"{", "\[IndentingNewLine]", 
               RowBox[{"(*", " ", 
                RowBox[{"1.", " ", "Gamma", " ", 
                 RowBox[{"function", "'"}], "s", " ", "certificate", " ", 
                 "is", " ", "Pochhammer", " ", 
                 RowBox[{"symbol", "."}]}], " ", "*)"}], 
               "\[IndentingNewLine]", 
               RowBox[{
                RowBox[{
                 RowBox[{"Gamma", "[", 
                  RowBox[{"a_", "+", 
                   RowBox[{
                    RowBox[{"(", 
                    RowBox[{"b_Integer:", "1"}], ")"}], "\[Times]", "var"}]}],
                   "]"}], "\[RuleDelayed]", 
                 RowBox[{"FunctionExpand", "@", 
                  RowBox[{"Pochhammer", "[", 
                   RowBox[{
                    RowBox[{"a", "+", 
                    RowBox[{"b", " ", "var"}]}], ",", "b"}], "]"}]}]}], ",", 
                "\[IndentingNewLine]", 
                RowBox[{"(*", " ", 
                 RowBox[{
                 "2.", " ", "Not", " ", "dependent", " ", "on", " ", "var", 
                  " ", 
                  RowBox[{"expression", "."}]}], " ", "*)"}], 
                "\[IndentingNewLine]", 
                RowBox[{
                 RowBox[{"_", "?", 
                  RowBox[{"(", 
                   RowBox[{
                    RowBox[{"FreeQ", "[", 
                    RowBox[{"#", ",", "var"}], "]"}], "&"}], ")"}]}], 
                 "\[RuleDelayed]", "1"}], ",", "\[IndentingNewLine]", 
                RowBox[{"(*", " ", 
                 RowBox[{"3.", " ", "Polynomial", " ", "of", " ", 
                  RowBox[{"var", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
                
                RowBox[{
                 RowBox[{"_", "?", 
                  RowBox[{"(", 
                   RowBox[{
                    RowBox[{"PolynomialQ", "[", 
                    RowBox[{"#", ",", "var"}], "]"}], "&"}], ")"}]}], 
                 "\[RuleDelayed]", 
                 RowBox[{"Cancel", "[", 
                  FractionBox[
                   RowBox[{"#", "/.", 
                    RowBox[{"var", "\[Rule]", 
                    RowBox[{"var", "+", "1"}]}]}], "#"], "]"}]}], ",", 
                "\[IndentingNewLine]", 
                RowBox[{"(*", " ", 
                 RowBox[{"4.", " ", "Any", " ", 
                  RowBox[{"other", "."}]}], " ", "*)"}], 
                "\[IndentingNewLine]", 
                RowBox[{"_", "\[RuleDelayed]", 
                 RowBox[{"funct", "[", 
                  RowBox[{
                   FractionBox[
                    RowBox[{"#", "/.", 
                    RowBox[{"var", "\[Rule]", 
                    RowBox[{"var", "+", "1"}]}]}], "#"], "/.", 
                   "ExpandAllRule"}], "]"}]}]}], "\[IndentingNewLine]", 
               "}"}]}], "\[IndentingNewLine]", "]"}], ",", 
            "\[IndentingNewLine]", "#2"}], "}"}], "&"}], "@@@", "factors"}], 
        "\[IndentingNewLine]", ")"}]}]}], ")"}]}]}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindCertificate", "[", 
    RowBox[{"expr_", ",", "var_Symbol"}], "]"}], ":=", "\[IndentingNewLine]", 
   
   RowBox[{"FindCertificate", "[", 
    RowBox[{
     RowBox[{"FactorList", "[", "expr", "]"}], ",", "var"}], "]"}]}], 
  ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindCertificate", "[", 
    RowBox[{"_", ",", "var_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"FindCertificate", "::", "BadVariable"}], ",", "var"}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}], ";"}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["FindSingularities", "Section"],

Cell[TextData[{
 "This function finds the singularities of the given certificate. We can \
factor given certificate and find singularities by factors like ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"a", "\[Times]", "n"}], "+", "b"}], TraditionalForm]]],
 ", because ",
 Cell[BoxData[
  FormBox["n", TraditionalForm]]],
 " (the variable, on which the certificate depends) is always an integer. The \
result is sorted in the ascending order."
}], "Text"],

Cell[CellGroupData[{

Cell["Messages", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindSingularities", "::", "usage"}], "=", 
   "\"\<FindSingularities[\!\(\*
StyleBox[\"cert\", \"TI\"]\), \!\(\*
StyleBox[\"x\", \"TI\"]\)] finds singularities of the given certificate \
\!\(\*
StyleBox[\"cert\", \"TI\"]\).\>\""}], ";"}]], "Code"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FindSingularities", "::", "BadVariable"}], "=", 
  "\"\<`1` isn't a valid variable.\>\""}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Options", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Options", "[", "FindSingularities", "]"}], "=", 
   RowBox[{"{", 
    RowBox[{"Range", "\[Rule]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], "}"}]}], 
  ";"}]], "Code"]
}, Closed]],

Cell[CellGroupData[{

Cell["Code", "Subsection"],

Cell["\<\
Matrix form. It calls the scalar form for each element and then returns the \
union of their results.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindSingularities", "[", 
    RowBox[{
     RowBox[{"cert_", "?", "SquareMatrixQ"}], ",", "var_Symbol", ",", 
     RowBox[{"opts", ":", 
      RowBox[{"OptionsPattern", "[", "]"}]}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "l", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
       RowBox[{
       "Here", " ", "we", " ", "find", " ", "singularities", " ", "for", " ", 
        "each", " ", "element"}], ",", " ", 
       RowBox[{"then", " ", "join", " ", 
        RowBox[{"them", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
       RowBox[{"After", " ", "that"}], " ", "\[LongDash]", " ", 
       RowBox[{
        RowBox[{"sort", ".", " ", "This"}], " ", "makes", " ", 
        "singularities", " ", "with", " ", "lower", " ", "orders", " ", 
        "appear", " ", "in", " ", "the", " ", "list", " ", 
        RowBox[{"first", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
      "After", " ", "we", " ", "delete", " ", "duplicates", " ", "by", " ", 
       RowBox[{"point", ".", " ", "Only"}], " ", "the", " ", "first", " ", 
       "one", " ", 
       RowBox[{"(", 
        RowBox[{"with", " ", "the", " ", "lowest", " ", "order"}], ")"}], " ", 
       RowBox[{"persists", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"l", "=", 
       RowBox[{"DeleteDuplicatesBy", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Sort", "[", "\[IndentingNewLine]", 
          RowBox[{"Join", "@@", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"FindSingularities", "[", 
               RowBox[{"#", ",", "var", ",", "opts"}], "]"}], "&"}], "/@", 
             RowBox[{"Flatten", "[", "cert", "]"}]}], ")"}]}], 
          "\[IndentingNewLine]", "]"}], ",", 
         RowBox[{
          RowBox[{"First", "@", "#"}], "&"}]}], "\[IndentingNewLine]", 
        "]"}]}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Checking", ",", " ", 
        RowBox[{"whether", " ", "there", " ", "are", " ", "numeric", " ", 
         RowBox[{"constants", ".", " ", "If"}], " ", "so"}], ",", " ", 
        RowBox[{"we", " ", "drop", " ", "all", " ", 
         RowBox[{"zeroes", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Otherwise", " ", "returning", " ", "l", " ", "as", " ", 
        RowBox[{"is", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{"MemberQ", "[", 
         RowBox[{"cert", ",", 
          RowBox[{"_", "?", "NumericQ"}], ",", "2"}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"DeleteCases", "[", 
         RowBox[{"l", ",", 
          RowBox[{"{", 
           RowBox[{"_", ",", 
            RowBox[{"_", "?", "Positive"}]}], "}"}]}], "]"}], ",", 
        "\[IndentingNewLine]", "l"}], "\[IndentingNewLine]", "]"}]}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}]], "Code"],

Cell["Scalar form.", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindSingularities", "[", 
    RowBox[{"cert_", ",", "var_Symbol", ",", 
     RowBox[{"OptionsPattern", "[", "]"}]}], "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"Sort", "@", 
    RowBox[{"Cases", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Replace", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"FactorList", "[", "cert", "]"}], ",", "\[IndentingNewLine]", 
        
        RowBox[{"{", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{
              RowBox[{
               RowBox[{"a_.", "var"}], "+", "b_."}], "/;", 
              RowBox[{"IntegerQ", "[", 
               RowBox[{"b", "/", "a"}], "]"}]}], ",", "k_"}], "}"}], 
           "\[RuleDelayed]", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{
              RowBox[{"-", "b"}], "/", "a"}], ",", "k"}], "}"}]}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{
              SuperscriptBox[
               RowBox[{"(", 
                RowBox[{
                 RowBox[{"a_.", "var"}], "+", "b_."}], ")"}], 
               RowBox[{"Rational", "[", 
                RowBox[{"1", ",", "q_"}], "]"}]], "/;", 
              RowBox[{"IntegerQ", "[", 
               RowBox[{"b", "/", "a"}], "]"}]}], ",", "k_"}], "}"}], 
           "\[RuleDelayed]", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{
              RowBox[{"-", "b"}], "/", "a"}], ",", 
             RowBox[{"k", "/", "q"}]}], "}"}]}], ",", "\[IndentingNewLine]", 
          RowBox[{"_", "\[RuleDelayed]", 
           RowBox[{"Unevaluated", "[", 
            RowBox[{"Sequence", "[", "]"}], "]"}]}]}], "\[IndentingNewLine]", 
         "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{"{", "1", "}"}]}], "\[IndentingNewLine]", "]"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"_", "?", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"OptionValue", "[", "Range", "]"}], "\[LeftDoubleBracket]",
            "1", "\[RightDoubleBracket]"}], "\[LessEqual]", 
          RowBox[{"#", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}],
           "\[LessEqual]", 
          RowBox[{
           RowBox[{"OptionValue", "[", "Range", "]"}], "\[LeftDoubleBracket]",
            "2", "\[RightDoubleBracket]"}]}], "&"}], ")"}]}]}], 
     "\[IndentingNewLine]", "]"}]}]}], ";"}]], "Code"],

Cell["Any other form.", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"FindSingularities", "[", 
    RowBox[{"_", ",", "var_", ",", 
     RowBox[{"OptionsPattern", "[", "]"}]}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Message", "[", 
      RowBox[{
       RowBox[{"FindSingularities", "::", "BadVariable"}], ",", "var"}], 
      "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"Abort", "[", "]"}], ";"}], "\[IndentingNewLine]", ")"}]}], 
  ";"}]], "Code"]
}, Closed]]
}, Open  ]],

Cell[CellGroupData[{

Cell["End", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{"End", "[", "]"}], ";"}]], "Code"]
}, Closed]]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1920, 1007},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
PrivateNotebookOptions->{"FileOutlineCache"->False},
TrackCellChangeTimes->False,
Magnification:>1.25 Inherited,
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[StyleDefinitions -> FrontEnd`FileName[{
        ParentDirectory[]}, "Stylesheet.nb", CharacterEncoding -> 
       "WindowsANSI"]]]}, Visible -> False, FrontEndVersion -> 
  "10.4 for Linux x86 (64-bit) (April 11, 2016)", StyleDefinitions -> 
  "PrivateStylesheetFormatting.nb"]
]

