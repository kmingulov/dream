/*******************************************************************************
HEADERS
*******************************************************************************/

#include "Complex.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <omp.h>

/*******************************************************************************
This file contains implementations of acceleration algorithm with GMP library.

Note, that it may be needed to set GMP default precision by:
    mpf_set_default_prec(prec)
Here prec is in bits. This number can be approximated by 10p/3, where p is 
precision in terms of decimal digits.

There are several implementations:
    gmpAccelerateReal       acceleration of array of real numbers (can support 
                            multiple asymptotics elements)
    gmpAccelerateComplex    acceleration of array of complex numbers (can 
                            support multiple asymptotics elements)
*******************************************************************************/

/*******************************************************************************
GMP ACCELERATE REAL
--------------------------------------------------------------------------------
Arguments:
    q           array with q parameters
    alpha       array with alpha parameters
    asymlen     length of q and alpha arrays
    arr         array with GMP numbers
    len         length of arr
    nacc        number of terms to be accelerated
    shift       shift for all indices (default is 0)
    monitor     function which is called after each acceleration cycle
*******************************************************************************/

void gmpAccelerateReal(mpf_t *q, mpf_t *alpha, int asymlen, mpf_t *arr, int len,
    int nacc, int shift, void (*monitor)())
{
    // Variables for current values of alpha.
    mpf_t a[asymlen];
    for (int i = 0; i < asymlen; i++)
    {
        mpf_init_set(a[i], alpha[i]);
    }

    // Current asymptotics.
    int current = 0;

    // Array for intermediate results.
    mpf_t buffer[len];
    for (int i = 0; i < len; i++)
    {
        mpf_init(buffer[i]);
    }

    // Iterating and accelerating.
    for (int i = 0; i < nacc; i++)
    {
        // Number of terms, which will be computed.
        int n = len-i-1;

        // Performing acceleration.
        #pragma omp parallel for
        for (int j = 0; j < n; j++)
        {
            // Temporary variables.
            mpf_t s1, s2, f1, f2;
            // Initializing them.
            mpf_init(f1); mpf_init(f2);
            mpf_init_set(s1, arr[j+1]);
            mpf_init_set(s2, arr[j]);
            // Applying acceleration formula.
            mpf_add_ui(f1, a[current], j + shift);
            mpf_mul_ui(f2, q[current], j + shift + 1);
            mpf_mul(s1, s1, f1);
            mpf_mul(s2, s2, f2);
            mpf_sub(s1, s1, s2);
            mpf_sub(f1, f1, f2);
            // Saving the value.
            mpf_div(buffer[j], s1, f1);
            // Freeing temporary variables.
            mpf_clear(s1); mpf_clear(s2);
            mpf_clear(f1); mpf_clear(f2);
        }

        // Copying results.
        for (int j = 0; j < n; j++)
        {
            mpf_set(arr[j], buffer[j]);
        }

        // Increasing alpha.
        mpf_add_ui(a[current], a[current], 1);
        // Moving to the next asymptotics.
        current++;
        if (current == asymlen)
        {
            current = 0;
        }

        // Calling the callback.
        if (monitor != NULL)
        {
            monitor();
        }
    }

    // Removing temporary variables.
    for (int i = 0; i < asymlen; i++)
    {
        mpf_clear(a[i]);
    }
    for (int i = 0; i < len; i++)
    {
        mpf_clear(buffer[i]);
    }
}

/*******************************************************************************
GMP ACCELERATE COMPLEX
--------------------------------------------------------------------------------
Arguments:
    q           array with q parameters
    alpha       array with alpha parameters
    asymlen     length of q and alpha arrays
    arr         array with GMP numbers
    len         length of arr
    nacc        number of terms to be accelerated
    shift       shift for all indices (default is 0)
    monitor     function which is called after each acceleration cycle
*******************************************************************************/

void gmpAccelerateComplex(mpc_t *q, mpc_t *alpha, int asymlen, mpc_t *arr, 
    int len, int nacc, int shift, void (*monitor)())
{
    // Variables for current values of alpha.
    mpc_t a[asymlen];
    for (int i = 0; i < asymlen; i++)
    {
        mpc_init_set(&(a[i]), alpha[i]);
    }

    // Current asymptotics.
    int current = 0;

    // Array for intermediate results.
    mpc_t buffer[len];
    for (int i = 0; i < len; i++)
    {
        mpc_init(&(buffer[i]));
    }

    // Iterating and accelerating.
    for (int i = 0; i < nacc; i++)
    {
        // Number of terms, which will be computed.
        int n = len-i-1;

        // Performing acceleration.
        #pragma omp parallel for
        for (int j = 0; j < n; j++)
        {
            // Temporary variables.
            mpc_t s1, s2, f1, f2;
            // Initializing them.
            mpc_init(&f1); mpc_init(&f2);
            mpc_init_set(&s1, arr[j+1]);
            mpc_init_set(&s2, arr[j]);
            // Applying acceleration formula.
            mpc_add_ui(&f1, a[current], j + shift);
            mpc_mul_ui(&f2, q[current], j + shift + 1);
            mpc_mul(&s1, s1, f1);
            mpc_mul(&s2, s2, f2);
            mpc_sub(&s1, s1, s2);
            mpc_sub(&f1, f1, f2);
            // Saving the value.
            mpc_div(&(buffer[j]), s1, f1);
            // Freeing temporary variables.
            mpc_clear(&s1); mpc_clear(&s2);
            mpc_clear(&f1); mpc_clear(&f2);
        }

        // Copying results.
        for (int j = 0; j < n; j++)
        {
            mpc_set(&(arr[j]), buffer[j]);
        }

        // Increasing alpha.
        mpc_add_ui(&(a[current]), a[current], 1);
        // Moving to the next asymptotics.
        current++;
        if (current == asymlen)
        {
            current = 0;
        }

        // Calling the callback.
        if (monitor != NULL)
        {
            monitor();
        }
    }

    // Removing temporary variables.
    for (int i = 0; i < asymlen; i++)
    {
        mpc_clear(&(a[i]));
    }
    for (int i = 0; i < len; i++)
    {
        mpc_clear(&(buffer[i]));
    }
}