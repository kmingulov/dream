#ifndef COMPLEX_H
#define COMPLEX_H

#include <gmp.h>

// mpc_t type.
typedef struct
{
    mpf_t re;
    mpf_t im;
} mpc_t;

// z
void mpc_init(mpc_t *z);
// z1 = z2
void mpc_set(mpc_t *z1, mpc_t z2);
// z1 = z2
void mpc_init_set(mpc_t *z1, mpc_t z2);
// z = str
void mpc_init_set_str(mpc_t *z, const char *restr, const char *imstr, unsigned long int base);
// z
void mpc_clear(mpc_t *z);

// z1 + z2
void mpc_add(mpc_t *z, mpc_t z1, mpc_t z2);
// z1 - z2
void mpc_sub(mpc_t *z, mpc_t z1, mpc_t z2);
// z + uint
void mpc_add_ui(mpc_t *res, mpc_t z, unsigned long int ui);
// z * uint
void mpc_mul_ui(mpc_t *res, mpc_t z, unsigned long int ui);
// z * mpf_t
void mpc_mul_mpf(mpc_t *res, mpc_t z, mpf_t f);
// z1 * z2
void mpc_mul(mpc_t *z, mpc_t z1, mpc_t z2);
// z / mpf_t
void mpc_div_mpf(mpc_t *res, mpc_t z, mpf_t f);
// z1 / z2
void mpc_div(mpc_t *z, mpc_t z1, mpc_t z2);

#endif