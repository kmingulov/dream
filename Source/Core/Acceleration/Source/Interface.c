/*******************************************************************************
HEADERS
*******************************************************************************/

#include "WolframLibrary.h"

#include "Acceleration.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <omp.h>

/*******************************************************************************
GLOBALS
*******************************************************************************/

// ID of the monitor callback (if any).
static mint monitor_callback_id;

// Wolfram Library Data.
WolframLibraryData ldata;

/*******************************************************************************
MATHEMATICA INTERFACE
--------------------------------------------------------------------------------
Special interface, in order to make this file compilable with CreateLibrary 
function.
*******************************************************************************/

DLLEXPORT mint WolframLibrary_getVersion()
{
	return WolframLibraryVersion;
}

DLLEXPORT mbool manage_monitor_callback(WolframLibraryData libData, mint id, MTensor argtypes)
{
	// Checking whether there is already a callback.
	if (monitor_callback_id) {
		(*libData->releaseLibraryCallbackFunction)(monitor_callback_id);
		monitor_callback_id = 0;
	}
	// Saving the callback.
	monitor_callback_id = id;
	// Saving library data.
	ldata = libData;
	// Done.
	return True;
}

DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData)
{
	// The monitor callback is not set yet.
	monitor_callback_id = 0;
	// Registering the callback.
	return (*libData->registerLibraryCallbackManager)("acceleration_monitor_callback", manage_monitor_callback);
}

DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData)
{
	// Unregistering the callback.
	(*libData->unregisterLibraryCallbackManager)("acceleration_monitor_callback");
}

void call_monitor_callback()
{
	mbool res;
	MArgument arg;
	arg.boolean = &res;
	(*ldata->callLibraryCallbackFunction)(monitor_callback_id, 0, NULL, arg);
}

/*******************************************************************************
STRING-ARRAY CONVERSION UTILS
*******************************************************************************/

void str2array(char *str, mpf_t *arr, int len)
{
	// Current number.
	int current = 0;
	// Current character.
	int index = 0;
	// Starting character of the current number.
	int start = 0;

	// Iterating over the string and converting numbers.
	while (current < len)
	{
        // Found a number.
        if (str[index] == '|' || str[index] == '\0')
        {
            // Changing | to \0.
            str[index] = '\0';
            // Creating GMP float.
            mpf_init_set_str(arr[current], &(str[start]), 10);
            // Changing the start.
            start = index+1;
            // Increasing the counters.
            current++;
        }
        // Increasing a counter.
        index++;
	}
}

void array2str(char *str, mpf_t *arr, int len, int prec)
{
	// Converting numbers into the string.
	int current = 0;
	for (int i = 0; i < len; i++)
	{
		current += gmp_sprintf(&(str[current]), 
			"%.*Ff|", prec, arr[i]
		);
	}

	// Killing trailing ``|''.
	str[current-1] = '\0';
}

void str2carray(char *str, mpc_t *arr, int len)
{
	// Current number.
	int current = 0;
	// Current character.
	int index = 0;
	// Starting character of current Re and Im.
	int re = 0;
	int im = 0;

	// Iterating over the string and converting numbers.
	while (current < len)
	{
		// Found an imaginary part.
		if (str[index] == '^')
		{
			// Setting a zero (for latter conversion).
			str[index] = '\0';
			// Remembering the index.
			im = index+1;
		}
        // Found a number.
        else if (str[index] == '|' || str[index] == '\0')
        {
            // Changing | to \0.
            str[index] = '\0';
            // Creating GMP float.
            mpc_init_set_str(&(arr[current]), &(str[re]), &(str[im]), 10);
            // Changing the start.
            re = index+1;
            im = index;
            // Increasing the counter.
            current++;
        }
        // Increasing the counter.
        index++;
	}
}

void carray2str(char *str, mpc_t *arr, int len, int prec)
{
	// Converting complex numbers into the string.
	int current = 0;
	for (int i = 0; i < len; i++)
	{
		current += gmp_sprintf(&(str[current]), "%.*Ff^", prec, arr[i].re);
		current += gmp_sprintf(&(str[current]), "%.*Ff|", prec, arr[i].im);
	}

	// Killing trailing ``|''.
	str[current-1] = '\0';
}

/*******************************************************************************
ACCELERATE REAL
*******************************************************************************/

DLLEXPORT int AccelerateReal(WolframLibraryData libData, mint argc, 
	MArgument *args, MArgument res)
{
	// Checking number of arguments.
	if (argc < 7)
		return LIBRARY_DIMENSION_ERROR;

	// Getting the string with terms.
	char *nums   = MArgument_getUTF8String(args[0]);

	// String with values of q.
	char *qstr   = MArgument_getUTF8String(args[1]);

	// String with values of alpha.
	char *astr   = MArgument_getUTF8String(args[2]);

	// Number of {q, alpha} pairs.
	mint asymlen = MArgument_getInteger(args[3]);

	// Getting the number of terms and number of terms to be accelerated.
	mint nterms  = MArgument_getInteger(args[4]);
	mint nacc    = MArgument_getInteger(args[5]);
	// Getting working precision.
	mint prec    = MArgument_getInteger(args[6]);
	// Getting shift.
	mint shift   = MArgument_getInteger(args[7]);

	// Setting up default precision.
	mpf_set_default_prec(10 * prec / 3);

	// Arrays of GMP numbers for q and alpha.
	mpf_t *q = malloc(sizeof(mpf_t) * asymlen);
	mpf_t *a = malloc(sizeof(mpf_t) * asymlen);
	str2array(qstr, q, asymlen);
	str2array(astr, a, asymlen);

	// Preparing array of GMP numbers.
	mpf_t *arr = malloc(sizeof(mpf_t) * nterms);
	str2array(nums, arr, nterms);

	// Evoking acceleration procedure.
	if (monitor_callback_id != 0)
	{
		// Calling the acceleration procedure.
		gmpAccelerateReal(q, a, asymlen, arr, nterms, nacc, shift, &call_monitor_callback);
		// Releasing the callback function.
		(*libData->releaseLibraryCallbackFunction)(monitor_callback_id);
		monitor_callback_id = 0;
	}
	else
	{
		// Calling the acceleration procedure.
		gmpAccelerateReal(q, a, asymlen, arr, nterms, nacc, shift, NULL);
	}

	// Converting the answer.
	// We returning all left terms, that is (nterms-nacc).
	char *rstr = malloc(sizeof(char) * (2*prec) * (nterms-nacc));
	array2str(rstr, arr, nterms-nacc, prec);

	// Saving the result.
	MArgument_setUTF8String(res, rstr);

	// Clearing the intermediate variables.
	for (int i = 0; i < asymlen; i++)
	{
		mpf_clear(q[i]); mpf_clear(a[i]);
	}
	for (int i = 0; i < nterms; i++)
	{
		mpf_clear(arr[i]);
	}
	free(q); free(a); free(arr);

	// Done.
	return LIBRARY_NO_ERROR;
}

/*******************************************************************************
ACCELERATE COMPLEX
*******************************************************************************/

DLLEXPORT int AccelerateComplex(WolframLibraryData libData, mint argc, 
	MArgument *args, MArgument res)
{
	// Checking number of arguments.
	if (argc < 7)
		return LIBRARY_DIMENSION_ERROR;

	// Getting the string with terms.
	char *nums   = MArgument_getUTF8String(args[0]);

	// String with values of q.
	char *qstr   = MArgument_getUTF8String(args[1]);

	// String with values of alpha.
	char *astr   = MArgument_getUTF8String(args[2]);

	// Number of {q, alpha} pairs.
	mint asymlen = MArgument_getInteger(args[3]);

	// Getting the number of terms and number of terms to be accelerated.
	mint nterms  = MArgument_getInteger(args[4]);
	mint nacc    = MArgument_getInteger(args[5]);
	// Getting working precision.
	mint prec    = MArgument_getInteger(args[6]);
	// Getting shift.
	mint shift   = MArgument_getInteger(args[7]);

	// Setting up default precision.
	mpf_set_default_prec(10 * prec / 3);

	// Arrays of GMP numbers for q and alpha.
	mpc_t *q = malloc(sizeof(mpc_t) * asymlen);
	mpc_t *a = malloc(sizeof(mpc_t) * asymlen);
	str2carray(qstr, q, asymlen);
	str2carray(astr, a, asymlen);

	// Preparing array of GMP numbers.
	mpc_t *arr = malloc(sizeof(mpc_t) * nterms);
	str2carray(nums, arr, nterms);

	// Evoking acceleration procedure.
	if (monitor_callback_id != 0)
	{
		// Calling the acceleration procedure.
		gmpAccelerateComplex(q, a, asymlen, arr, nterms, nacc, shift, &call_monitor_callback);
		// Releasing the callback function.
		(*libData->releaseLibraryCallbackFunction)(monitor_callback_id);
		monitor_callback_id = 0;
	}
	else
	{
		// Calling the acceleration procedure.
		gmpAccelerateComplex(q, a, asymlen, arr, nterms, nacc, shift, NULL);
	}

	// Converting the answer.
	// We returning all left terms, that is (nterms-nacc).
	char *rstr = malloc(sizeof(char) * (4*prec) * (nterms-nacc));
	carray2str(rstr, arr, nterms-nacc, prec);

	// Saving the result.
	MArgument_setUTF8String(res, rstr);

	// Clearing the intermediate variables.
	for (int i = 0; i < asymlen; i++)
	{
		mpc_clear(&(q[i])); mpc_clear(&(a[i]));
	}
	for (int i = 0; i < nterms; i++)
	{
		mpc_clear(&(arr[i]));
	}
	free(q); free(a); free(arr);

	// Done.
	return LIBRARY_NO_ERROR;
}