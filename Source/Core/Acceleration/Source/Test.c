/*******************************************************************************
HEADERS
*******************************************************************************/

#include "Acceleration.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <omp.h>

/*******************************************************************************
TESTS
*******************************************************************************/

void test1()
{
	int nterms = 50;
	int prec   = 50;

	mpf_set_default_prec(10*prec/3);

	mpf_t *q = malloc(sizeof(mpf_t) * 2);
	mpf_init_set_str(q[0], "-1.", 10);
	mpf_init_set_str(q[1], "1.", 10);

	mpf_t *a = malloc(sizeof(mpf_t) * 2);
	mpf_init_set_str(a[0], "3.", 10);
	mpf_init_set_str(a[1], "1.5", 10);

	mpf_t *arr = malloc(sizeof(mpf_t) * nterms);

	for (int i = 0; i < nterms; i++)
	{
		mpf_t t1, t2;
		mpf_init_set(t1, q[0]);
		mpf_pow_ui(t1, t1, i+1);
		mpf_init_set(t2, q[1]);
		mpf_pow_ui(t2, t2, i+1);
		mpf_t s1, s2, s3;
		mpf_init(s1); mpf_init(s2); mpf_init(s3);
		mpf_set_si(s1, i+1);
		mpf_pow_ui(s1, s1, 3);
		mpf_set_si(s2, i+1);
		mpf_sqrt(s3, s2);
		mpf_mul(s2, s2, s3);
		mpf_div(t1, t1, s1);
		mpf_div(t2, t2, s2);
		mpf_init(arr[i]);
		mpf_add(arr[i], t1, t2);
		mpf_clear(t1);
		mpf_clear(t2);
		mpf_clear(s1);
		mpf_clear(s2);
		mpf_clear(s3);
	}

	for (int i = nterms-2; i >= 0; i--)
	{
		mpf_add(arr[i], arr[i], arr[i+1]);
	}

	//for (int i = 0; i < nterms; i++)
	//{
	//	gmp_printf("%.*Ff\n", 2*prec, arr[i]);
	//}

	gmpAccelerateReal(q, a, 2, arr, nterms, nterms-1);

	gmp_printf("%.*Ff\n", 2*prec, arr[0]);

	for (int i = 0; i < 2; i++)
	{
		mpf_clear(q[i]); mpf_clear(a[i]);
	}
	for (int i = 0; i < nterms; i++)
	{
		mpf_clear(arr[i]);
	}
	free(arr); free(q); free(a);
}

void test2()
{
	int nterms = 50;
	int prec   = 50;

	mpf_set_default_prec(10*prec/3);

	mpc_t q;
	mpc_init_set_str(&q, "0.", "1.", 10);

	mpc_t a;
	mpc_init_set_str(&a, "2.", "0.", 10);

	mpc_t *arr = malloc(sizeof(mpc_t) * 2 * nterms);

	mpc_t t;
	mpc_init_set_str(&t, "1.", "0.", 10);

	for (int i = 0; i < nterms; i++)
	{
		mpc_mul(&t, t, q);
		mpf_t s;
		mpf_init(s);
		mpf_set_si(s, i+1);
		mpf_pow_ui(s, s, 2);
		mpc_init(&(arr[i]));
		mpc_div_mpf(&(arr[i]), t, s);
		mpf_clear(s);
	}

	for (int i = nterms-2; i >= 0; i--)
	{
		mpc_add(&(arr[i]), arr[i], arr[i+1]);
	}

	gmp_printf("%.*Ff\n", prec, arr[0].re);
	gmp_printf("%.*Ff\n", prec, arr[0].im);

	gmpAccelerateComplex(&q, &a, 1, arr, nterms, nterms-1);

	gmp_printf("%.*Ff\n", prec, arr[0].re);
	gmp_printf("%.*Ff\n", prec, arr[0].im);

	for (int i = 0; i < nterms; i++)
	{
		mpc_clear(&(arr[i]));
	}
	free(arr);
	mpc_clear(&q);
	mpc_clear(&a);
	mpc_clear(&t);

}

/*******************************************************************************
MAIN
*******************************************************************************/

void main()
{
	test1();
	test2();
}