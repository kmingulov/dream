#ifndef ACCELERATION_H
#define ACCELERATION_H

#include "Complex.h"

#include <gmp.h>

void gmpAccelerateReal(mpf_t *q, mpf_t *alpha, int asymlen, mpf_t *arr, int len,
    int nacc, int shift, void (*monitor)());

void gmpAccelerateComplex(mpc_t *q, mpc_t *alpha, int asymlen, mpc_t *arr, 
    int len, int nacc, int shift, void (*monitor)());

#endif