/*******************************************************************************
HEADERS
*******************************************************************************/

#include "Complex.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <omp.h>

/*******************************************************************************
This file contains simple implementation of complex numbers, based on GMP 
bignums.
*******************************************************************************/

/*******************************************************************************
ASSIGNMENT OF COMPLEX NUMBERS
*******************************************************************************/

// z
inline void mpc_init(mpc_t *z)
{
    mpf_init(z->re);
    mpf_init(z->im);
}

// z1 = z2
inline void mpc_set(mpc_t *z1, mpc_t z2)
{
    mpf_set(z1->re, z2.re);
    mpf_set(z1->im, z2.im);
}

// z1 = z2
inline void mpc_init_set(mpc_t *z1, mpc_t z2)
{
    mpf_init_set(z1->re, z2.re);
    mpf_init_set(z1->im, z2.im);
}

// z = str
inline void mpc_init_set_str(mpc_t *z, const char *restr, const char *imstr, unsigned long int base)
{
    mpf_init_set_str(z->re, restr, base);
    mpf_init_set_str(z->im, imstr, base);
}

// z
inline void mpc_clear(mpc_t *z)
{
    mpf_clear(z->re);
    mpf_clear(z->im);
}

/*******************************************************************************
OPERATIONS ON COMPLEX NUMBERS
*******************************************************************************/

// z1 + z2
inline void mpc_add(mpc_t *z, mpc_t z1, mpc_t z2)
{
    mpf_add(z->re, z1.re, z2.re);
    mpf_add(z->im, z1.im, z2.im);
}

// z1 - z2
inline void mpc_sub(mpc_t *z, mpc_t z1, mpc_t z2)
{
    mpf_sub(z->re, z1.re, z2.re);
    mpf_sub(z->im, z1.im, z2.im);
}

// z + uint
inline void mpc_add_ui(mpc_t *res, mpc_t z, unsigned long int ui)
{
    mpf_add_ui(res->re, z.re, ui);
    mpf_set(res->im, z.im);
}

// z * uint
inline void mpc_mul_ui(mpc_t *res, mpc_t z, unsigned long int ui)
{
    mpf_mul_ui(res->re, z.re, ui);
    mpf_mul_ui(res->im, z.im, ui);
}

// z * mpf_t
inline void mpc_mul_mpf(mpc_t *res, mpc_t z, mpf_t f)
{
    mpf_mul(res->re, z.re, f);
    mpf_mul(res->im, z.im, f);
}

// z1 * z2
inline void mpc_mul(mpc_t *z, mpc_t z1, mpc_t z2)
{
    // Intermediate variables.
    mpf_t f1, f2, f3;
    mpf_init(f1); mpf_init(f2); mpf_init(f3);
    // Computing product.
    mpf_mul(f1, z1.re, z2.re);
    mpf_mul(f2, z1.im, z2.im);
    mpf_sub(f1, f1, f2);
    mpf_mul(f2, z1.im, z2.re);
    mpf_mul(f3, z1.re, z2.im);
    mpf_add(f2, f2, f3);
    // Storing it into z.
    mpf_set(z->re, f1);
    mpf_set(z->im, f2);
    // Clearing intermediate variables.
    mpf_clear(f1); mpf_clear(f2); mpf_clear(f3);
}

// z / mpf_t
inline void mpc_div_mpf(mpc_t *res, mpc_t z, mpf_t f)
{
    mpf_div(res->re, z.re, f);
    mpf_div(res->im, z.im, f);
}

// z1 / z2
inline void mpc_div(mpc_t *z, mpc_t z1, mpc_t z2)
{
    // Intermediate variables.
    mpf_t f1, f2, f3, f4;
    mpf_init(f1); mpf_init(f2); mpf_init(f3); mpf_init(f4);
    // Computing division.
    mpf_mul(f1, z1.re, z2.re);
    mpf_mul(f2, z1.im, z2.im);
    mpf_add(f1, f1, f2);
    mpf_mul(f2, z1.im, z2.re);
    mpf_mul(f3, z1.re, z2.im);
    mpf_sub(f2, f2, f3);
    mpf_pow_ui(f3, z2.re, 2);
    mpf_pow_ui(f4, z2.im, 2);
    mpf_add(f3, f3, f4);
    // Storing it into z.
    mpf_div(z->re, f1, f3);
    mpf_div(z->im, f2, f3);
    // Clearing intermediate variables.
    mpf_clear(f1); mpf_clear(f2); mpf_clear(f3); mpf_clear(f4);
}