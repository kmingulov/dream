This directory contains C implementation of convergence acceleration algorithm, using GMP library.

There are three files:

	Acceleration.c
		Acceleration algorithm itself in terms of GMP numbers.

	Test.c
		Small program for manual tests of this algorithm.

	Interface.c
		Interface for Mathematica's LibraryLink.