(* ::Package:: *)

(* ::Section::Closed:: *)
(*FindAsymptotics*)


VerificationTest[
	FindAsymptotics[FindCertificate[3^k/k^3,k],k->\[Infinity]],
	{3,3,0},

	TestID -> "FindAsymptotics 01"
]


VerificationTest[
	FindAsymptotics[FindCertificate[3^k/(k^3 (k!)^7),k],k->\[Infinity]],
	{3,10,7},

	TestID -> "FindAsymptotics 02"
]


VerificationTest[
	FindAsymptotics[FindCertificate[3^-k/k^3,k],k->\[Infinity]],
	{1/3,3,0},

	TestID -> "FindAsymptotics 03"
]


VerificationTest[
	FindAsymptotics[FindCertificate[2^-k,k],k->\[Infinity]],
	{1/2,0,0},

	TestID -> "FindAsymptotics 04"
]


VerificationTest[
	FindAsymptotics[FindCertificate[1/q^3,q],q->\[Infinity]],
	{1,3,0},

	TestID -> "FindAsymptotics 05"
]


VerificationTest[
	FindAsymptotics[{1,-((2\[Nu])/(\[Nu]+1)),0},\[Nu]->\[Infinity]],
	{2,1,0},

	TestID -> "FindAsymptotics 06"
]


VerificationTest[
	FindAsymptotics[{1,-((2\[Nu])/(\[Nu]+1)),0},\[Nu]->-\[Infinity]],
	{2,1,0},

	TestID -> "FindAsymptotics 07"
]


VerificationTest[
	(* Only last number in this list is crucial. Others can be anything. *)
	FindAsymptotics[{1,\[Nu],0},\[Nu]->\[Infinity]],
	{-1,-1,-1},

	TestID -> "FindAsymptotics 08"
]


(* ::Section::Closed:: *)
(*AsymptoticsInverse*)


VerificationTest[
	AsymptoticsInverse[{3,5,2}],
	{1/3,-5,-2},

	TestID -> "AsymptoticsInverse 01"
]


VerificationTest[
	AsymptoticsInverse[{3,0,0}],
	{1/3,0,0},

	TestID -> "AsymptoticsInverse 02"
]


VerificationTest[
	AsymptoticsInverse[{1,3,0}],
	{1,-3,0},

	TestID -> "AsymptoticsInverse 03"
]


(* ::Section::Closed:: *)
(*AsymptoticsTimes*)


VerificationTest[
	AsymptoticsTimes[{1/2,2,0},{2,-3,0},{1,4,0}],
	{1,3,0},

	TestID -> "AsymptoticsTimes 01"
]


VerificationTest[
	AsymptoticsTimes[{{1,3,1},{1,3/2,0}},{{1,1,-1},{1,5/3,0}}],
	{{1,4,0},{1,14/3,1},{1,5/2,-1},{1,19/6,0}},

	TestID -> "AsymptoticsTimes 02"
]


VerificationTest[
	AsymptoticsTimes[{{1,1,1},{1,1,0}},{{1,5/3,0},{1,1,0}},{{1,7/2,0},{1,1/3,0},{1,2,0}}],
	{{1,37/6,1},{1,3,1},{1,14/3,1},{1,11/2,1},{1,7/3,1},{1,4,1},{1,37/6,0},{1,3,0},{1,14/3,0},{1,11/2,0},{1,7/3,0},{1,4,0}},

	TestID -> "AsymptoticsTimes 03"
]


(* ::Section::Closed:: *)
(*AsymptoticsSimplify*)


VerificationTest[
	AsymptoticsSimplify[{{1,5,3},{1/2,4,7}},\[Infinity]],
	{{1,5,3}},

	TestID -> "AsymptoticsSimplify 01"
]


VerificationTest[
	AsymptoticsSimplify[{{1/3,5,7},{1/2,4,7}},\[Infinity]],
	{{1/2,4,7}},

	TestID -> "AsymptoticsSimplify 02"
]


VerificationTest[
	AsymptoticsSimplify[{{1/2,5,7},{1/2,6,7}},\[Infinity]],
	{{1/2,5,7}},

	TestID -> "AsymptoticsSimplify 03"
]


VerificationTest[
	AsymptoticsSimplify[{{2,4,0},{3,5,0}},-\[Infinity]],
	{{2,4,0}},

	TestID -> "AsymptoticsSimplify 04"
]


VerificationTest[
	AsymptoticsSimplify[{{1,5,0},{-1,6,0}},\[Infinity]],
	{{1,5,0}},

	TestID -> "AsymptoticsSimplify 05"
]


VerificationTest[
	AsymptoticsSimplify[{{1,5/2,0},{-1,6,0},{1,3,0}},\[Infinity]],
	{{1,3,0},{1,5/2,0}},

	TestID -> "AsymptoticsSimplify 06"
]


VerificationTest[
	AsymptoticsSimplify[{{1,0,0},{1,3/2,0},{1,7/2,0}},\[Infinity]],
	{{1,0,0},{1,3/2,0},{1,7/2,0}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 07"
]


VerificationTest[
	AsymptoticsSimplify[{{1,2,0},{1,3/2,0},{1,7/2,0}},\[Infinity]],
	{{1,3/2,0},{1,2,0}},

	TestID -> "AsymptoticsSimplify 08"
]


VerificationTest[
	AsymptoticsSimplify[{{1,0,0},{1,1,0},{1,7/2,0}},\[Infinity]],
	{{1,0,0},{1,1,0},{1,7/2,0}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 09"
]


VerificationTest[
	AsymptoticsSimplify[{{1,2,0},{1,3,0},{1,7/2,0}},\[Infinity]],
	{{1,2,0},{1,7/2,0}},

	TestID -> "AsymptoticsSimplify 10"
]


VerificationTest[
	AsymptoticsSimplify[{{1,5,3},{2,4,7}},\[Infinity]],
	{{1,5,3}},

	TestID -> "AsymptoticsSimplify 11"
]


VerificationTest[
	AsymptoticsSimplify[{{1,5,3},{2^-1,4,7}},-\[Infinity]],
	{{1,5,3}},

	TestID -> "AsymptoticsSimplify 12"
]


VerificationTest[
	AsymptoticsSimplify[{{1,5,7},{6,5,7}},\[Infinity]],
	{{6,5,7}},

	TestID -> "AsymptoticsSimplify 13"
]


VerificationTest[
	AsymptoticsSimplify[{{2,0,0}},\[Infinity]],
	{{2,0,0}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 14"
]


VerificationTest[
	AsymptoticsSimplify[{{1,0,-2}},\[Infinity]],
	{{1,0,-2}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 15"
]


VerificationTest[
	AsymptoticsSimplify[{{1,1,0}},\[Infinity]],
	{{1,1,0}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 16"
]


VerificationTest[
	AsymptoticsSimplify[{{1,-7,0}},\[Infinity]],
	{{1,-7,0}},
	{AsymptoticsSimplify::Divergence},

	TestID -> "AsymptoticsSimplify 17"
]


VerificationTest[
	AsymptoticsSimplify[{{1/2,-3,0}},\[Infinity]],
	{{1/2,-3,0}},

	TestID -> "AsymptoticsSimplify 18"
]


(* ::Section::Closed:: *)
(*AsymptoticsConvergesQ*)


VerificationTest[
	AsymptoticsConvergesQ[{1,0,0},\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 01"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,1,0},\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 02"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,2,0},\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 03"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,2,0},-\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 04"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,1,0},\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 05"
]


VerificationTest[
	AsymptoticsConvergesQ[{2,1,0},\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 06"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,0,-1},\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 07"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,0,-1},-\[Infinity]],
	False,

	TestID -> "AsymptoticsConvergesQ 08"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,0,1},\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 09"
]


VerificationTest[
	AsymptoticsConvergesQ[{1,0,1},-\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 10"
]


VerificationTest[
	AsymptoticsConvergesQ[{5,0,1},\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 11"
]


VerificationTest[
	AsymptoticsConvergesQ[{5,0,1},-\[Infinity]],
	True,

	TestID -> "AsymptoticsConvergesQ 12"
]
