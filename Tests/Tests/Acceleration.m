(* ::Package:: *)

(* ::Section:: *)
(*ConvergenceAccelerate*)


(* ::Subsection::Closed:: *)
(*Partial acceleration*)


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[ConvergenceAccelerate[l,50,{1,2}]-Zeta[2],10^-74],

	0,

	TestID -> "ConvergenceAccelerate 01"
]


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[ConvergenceAccelerate[l,50,{1,2},Compiled->False]-Zeta[2],10^-74],

	0,

	TestID -> "ConvergenceAccelerate 02"
]


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[Last@Fold[DREAM`Private`NativeConvergenceAccelerate[#1,{1,2+#2}]&,l,Range[0,49]]-Zeta[2],10^-74],

	0,

	TestID -> "ConvergenceAccelerate 03"
]


(* ::Subsection::Closed:: *)
(*Matrix*)


VerificationTest[
	l=Accumulate@N[Table[{{1/n^2,1/n^3},{1/n^2,1/n^4}},{n,1,110}],163];
	Chop[ConvergenceAccelerate[l,All,{1,2}]-{{Zeta[2],Zeta[3]},{Zeta[2],Zeta[4]}},10^-98],

	{{0,0},{0,0}},

	TestID -> "ConvergenceAccelerate 04"
]


VerificationTest[
	l=Accumulate@DREAM`ZeroSeries`ZeroNSeries[Table[{{1/(n+\[Epsilon])^2,1/(n+\[Epsilon])^3},{1/(n+\[Epsilon])^2,1/(n+\[Epsilon])^4}},{n,1,110}],{\[Epsilon],0},163];
	Normal@Chop[
		MapAt[DREAM`ZeroSeries`ToSeriesData,ConvergenceAccelerate[l,All,{1,2}],{All,All}]-{{Zeta[2],Zeta[3]},{Zeta[2],Zeta[4]}},
		10^-98
	],

	{{0,0},{0,0}},

	TestID -> "ConvergenceAccelerate 05"
]


(* ::Subsection::Closed:: *)
(*Complete acceleration*)


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[ConvergenceAccelerate[l,All,{1,2}]-Zeta[2],10^-99],

	0,

	TestID -> "ConvergenceAccelerate 06"
]


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[
		Fold[DREAM`Private`NativeConvergenceAccelerate[#1,{1,2+#2}]&,l,Range[0,108]]-Zeta[2],
		10^-99
	],

	{0},

	TestID -> "ConvergenceAccelerate 07"
]


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[
		ConvergenceAccelerate[l,All,{1,2},Compiled->False]-Zeta[2],
		10^-99
	],

	0,

	TestID -> "ConvergenceAccelerate 08"
]


(* ::Subsection::Closed:: *)
(*Direction*)


l=N[Table[1/n^2,{n,1,110}],163];
l=Reverse@Accumulate@Reverse@l;


VerificationTest[
	Chop[First[l]-ConvergenceAccelerate[l,All,{1,2}]-Zeta[2], 10^-99],
	0,
	TestID -> "ConvergenceAccelerate Direction 01"
]


VerificationTest[
	Chop[First[l]-ConvergenceAccelerate[l,All,{1,2},Direction->Right]-Zeta[2], 10^-99],
	0,
	TestID -> "ConvergenceAccelerate Direction 02"
]


VerificationTest[
	Chop[First[l]-ConvergenceAccelerate[l,All,{1,2},Direction->Left]-Zeta[2], 10^-99],
	0,
	TestID -> "ConvergenceAccelerate Direction 03"
]


VerificationTest[
	First[l]-ConvergenceAccelerate[l,50,{1,2}]-Zeta[2],
	-9.142825933061198775619263508711269859702889889301411942057772947857026051552552584980746130508291810115079151484606117`88.54015409909707*^-75,
	TestID -> "ConvergenceAccelerate Direction 04"
]


VerificationTest[
	First[l]-ConvergenceAccelerate[l,50,{1,2},Direction->Right]-Zeta[2],
	-9.142825933061198775619263508711269859702889889301411942057772947857026051552552584980746130508291810115079151484606117`88.54015409909707*^-75,
	TestID -> "ConvergenceAccelerate Direction 05"
]


VerificationTest[
	First[l]-ConvergenceAccelerate[l,50,{1,2},Direction->Left]-Zeta[2],
	-7.4145612712755262610546049815074883060833578063516629141534060875207487924100504057316061486847771338591864712056107030508291810115079151484606117`116.44915910601036*^-47,
	TestID -> "ConvergenceAccelerate Direction 06"
]


r=Fold[DREAM`Private`NativeConvergenceAccelerate[#1,0,{1,#2}]&,l,Range[2,50]];


VerificationTest[
	Chop[ConvergenceAccelerate[l,49,{1,2},Direction->Right,Compiled->False]-Last[r], 10^-164],
	0,
	TestID -> "ConvergenceAccelerate Direction 07"
]


VerificationTest[
	Chop[ConvergenceAccelerate[l,49,{1,2},Direction->Left,Compiled->False]-First[r], 10^-164],
	0,
	TestID -> "ConvergenceAccelerate Direction 08"
]


(* ::Subsection:: *)
(*Monitor*)


(* ::Subsubsection::Closed:: *)
(*Numbers*)


l=Accumulate@N[Table[1/n^2, {n,1,500}], 815];


VerificationTest[
	r1=ConvergenceAccelerate[l, All, {1,2}, Compiled->False, Monitor:>n1];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 01"
]


VerificationTest[
	r2=ConvergenceAccelerate[l, All, {1,2}, Compiled->False, Monitor->None];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 02"
]


VerificationTest[
	r3=ConvergenceAccelerate[l, All, {1,2}, Compiled->True, Monitor:>n1];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 03"
]


VerificationTest[
	r4=ConvergenceAccelerate[l, All, {1,2}, Compiled->True, Monitor->None];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 04"
]


VerificationTest[
	Chop[{r1-r2,r2-r3,r3-r4},10^-500],

	{0, 0, 0},

	TestID -> "ConvergenceAccelerate Monitor 05"
]


(* ::Subsubsection::Closed:: *)
(*ZeroSeriesData*)


sd=DREAM`ZeroSeries`ZeroSeries[1/(n^2+\[Epsilon]),{\[Epsilon],3}];
l=Accumulate@Table[N[sd,815],{n,1,500}];


VerificationTest[
	r1=ConvergenceAccelerate[l, All, {1,2}, Compiled->False, Monitor:>n1];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 06"
]


VerificationTest[
	r2=ConvergenceAccelerate[l, All, {1,2}, Compiled->False, Monitor->None];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 07"
]


VerificationTest[
	r3=ConvergenceAccelerate[l, All, {1,2}, Compiled->True, Monitor:>n1];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 08"
]


VerificationTest[
	r4=ConvergenceAccelerate[l, All, {1,2}, Compiled->True, Monitor->None];,

	Null,

	TestID -> "ConvergenceAccelerate Monitor 09"
]


VerificationTest[
	Chop[{r1-r2,r2-r3,r3-r4},10^-500],

	Table[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon],{},4,4], {3}],

	TestID -> "ConvergenceAccelerate Monitor 10"
]


(* ::Subsection:: *)
(*Compiled*)


(* ::Subsubsection:: *)
(*Single \[Alpha]*)


(* ::Subsubsection:: *)
(*Single \[Alpha], several rounds*)


(* ::Subsubsection:: *)
(*Multiple \[Alpha]*)


(* ::Section::Closed:: *)
(*FullConvergenceAccelerate*)


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[FullConvergenceAccelerate[l,{1,2}]-Zeta[2],10^-99],

	0,

	TestID -> "FullConvergenceAccelerate 01"
]


VerificationTest[
	l=Accumulate@N[Table[1/n^2,{n,1,110}],163];
	Chop[FullConvergenceAccelerate[l,{1,2},Compiled->False]-Zeta[2],10^-99],

	0,

	TestID -> "FullConvergenceAccelerate 02"
]


(* ::Section::Closed:: *)
(*SumConvergenceAccelerate*)


VerificationTest[
	l=N[Table[1/n^2,{n,1,110}],163];
	Chop[SumConvergenceAccelerate[l,All,{1,2}]-Zeta[2],10^-99],

	0,

	TestID -> "SumConvergenceAccelerate 01"
]


VerificationTest[
	l=N[Table[(-1)^n/n,{n,1,110}],163];
	Chop[SumConvergenceAccelerate[l,All,{-1,1}]+Log[2],10^-99],

	0,

	TestID -> "SumConvergenceAccelerate 02"
]


VerificationTest[
	l=N[Table[Exp[(I \[Pi] n)/6] 1/n^3,{n,1,316}],185];
	Chop[SumConvergenceAccelerate[l,All,{Complex[Sqrt[3]/2,1/2],3}]-PolyLog[3,(-1)^(1/6)],10^-97],

	0,

	TestID -> "SumConvergenceAccelerate 03"
]
