(* ::Package:: *)

(* ::Section:: *)
(*Inverse*)


VerificationTest[
	Times[1, Power[DREAM`ZeroSeries`ZeroSeries[Plus[1, Times[20, x], Power[x, 3]], List[x, 6]], -1]]
	,
	DREAM`ZeroSeries`ZeroSeriesData[x, List[1, -20, 400, -8001, 160040, -3201200, 64032001], 0, 7],

	TestID -> "Inverse 01"
]

VerificationTest[
	Times[1, Power[DREAM`ZeroSeries`ZeroSeries[Plus[Times[20, x], Power[x, 3]], List[x, 3]], -1]]
	,
	DREAM`ZeroSeries`ZeroSeriesData[x, List[Times[1, Power[20, -1]], 0, Times[-1, Power[400, -1]]], -1, 2],

	TestID -> "Inverse 02"
]

VerificationTest[
	Times[1, Power[DREAM`ZeroSeries`ZeroSeries[Plus[x, Times[Times[7, Power[20, -1]], Power[x, 2]], Times[Times[1, Power[20, -1]], Power[x, 3]]], List[x, 11]], -1]]
	,
	DREAM`ZeroSeries`ZeroSeriesData[x, List[1, Times[-7, Power[20, -1]], Times[29, Power[400, -1]], Times[-63, Power[8000, -1]], Times[-139, Power[160000, -1]], Times[2233, Power[3200000, -1]], Times[-12851, Power[64000000, -1]], Times[45297, Power[1280000000, -1]], Times[-60059, Power[25600000000, -1]], Times[-485527, Power[512000000000, -1]], Times[4599869, Power[10240000000000, -1]]], -1, 10],

	TestID -> "Inverse 03"
]

VerificationTest[
	Times[1, Power[DREAM`ZeroSeries`ZeroSeries[Plus[Times[13, Power[Power[x, 3], -1]], 1, Times[17, Power[x, 2]]], List[x, 4]], -1]]
	,
	DREAM`ZeroSeries`ZeroSeriesData[x, List[Times[1, Power[13, -1]], 0, 0, Times[-1, Power[169, -1]], 0, Times[-17, Power[169, -1]], Times[1, Power[2197, -1]], 0], 3, 11],

	TestID -> "Inverse 04"
]


(* ::Section:: *)
(*ZeroSeriesCoefficient*)


VerificationTest[
	Set[zsd, DREAM`ZeroSeries`ZeroSeries[Times[Times[1, Power[Power[Plus[\[Epsilon], 1], 2], -1]], Times[1, Power[\[Epsilon], -1]]], List[\[Epsilon], 6]]]
	,
	DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[1, -2, 3, -4, 5, -6, 7, -8], -1, 7],

	TestID -> "ZeroSeriesCoefficient 01"
]

VerificationTest[
	Map[Function[List[Slot[1], DREAM`ZeroSeries`ZeroSeriesCoefficient[zsd, Slot[1]]]], Range[-10, 10]]
	,
	List[List[-10, 0], List[-9, 0], List[-8, 0], List[-7, 0], List[-6, 0], List[-5, 0], List[-4, 0], List[-3, 0], List[-2, 0], List[-1, 1], List[0, -2], List[1, 3], List[2, -4], List[3, 5], List[4, -6], List[5, 7], List[6, -8], List[7, Indeterminate], List[8, Indeterminate], List[9, Indeterminate], List[10, Indeterminate]],

	TestID -> "ZeroSeriesCoefficient 02"
]
