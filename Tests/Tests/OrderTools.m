(* ::Package:: *)

VerificationTest[
	DREAM`Private`OrderListSimplify[List[List[1, 0], List[2, 0], List[3, 0], List[4, -1], List[5, -2], List[6, -2], List[7, -2]]]
	,
	List[List[1, 0], List[4, -1], List[5, -2]],

	TestID -> "OrderTools 01"
]

VerificationTest[
	DREAM`Private`OrderListExpand[List[List[0, 0], List[2, -1], List[5, -2]], 12]
	,
	List[List[0, 0], List[1, 0], List[2, -1], List[3, -1], List[4, -1], List[5, -2], List[6, -2], List[7, -2], List[8, -2], List[9, -2], List[10, -2], List[11, -2], List[12, -2]],

	TestID -> "OrderTools 02"
]

VerificationTest[
	DREAM`Private`OrderListSimplify[DREAM`Private`OrderListExpand[List[List[0, 0], List[2, -1], List[5, -2]], 12]]
	,
	List[List[0, 0], List[2, -1], List[5, -2]],

	TestID -> "OrderTools 03"
]

VerificationTest[(* 55 *)
	DREAM`Private`OrderListExpand[List[], 10]
	,
	List[List[0, 0], List[1, 0], List[2, 0], List[3, 0], List[4, 0], List[5, 0], List[6, 0], List[7, 0], List[8, 0], List[9, 0], List[10, 0]],

	TestID -> "OrderTools 04"
]
