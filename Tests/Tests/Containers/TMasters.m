(* ::Package:: *)

(* ::Section::Closed:: *)
(*Simple DRR check (everything up)*)


VerificationTest[(* 337 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, Times[-1, \[Nu]]]], Rule[J2, Power[7, Times[-1, \[Nu]]]], Rule[J3, Power[5, Times[-1, \[Nu]]]]]], Set[coefs, List[List[Times[1, Power[2, -1]], 0, 0], List[Power[5, Times[-1, \[Nu]]], Times[1, Power[7, -1]], 0], List[Times[Power[Times[2, Power[5, -1]], \[Nu]], Times[1, Power[Power[\[Nu], 3], -1]]], Power[7, Times[-1, \[Nu]]], Times[1, Power[5, -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null,

	TestID -> "TMasters DRR Simple-Up 01"
]

VerificationTest[(* 338 *)
	Map[Function[List[x], Plus[ReplaceAll[List[J1[Plus[x, 1, Times[-1, \[Epsilon]]]], J2[Plus[x, 1, Times[-1, \[Epsilon]]]], J3[Plus[x, 1, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, 1, Times[-1, \[Epsilon]]], 3, 50]], Times[-1, Dot[ReplaceAll[coefs, Rule[\[Nu], Plus[x, Times[-1, \[Epsilon]]]]], ReplaceAll[List[J1[Plus[x, Times[-1, \[Epsilon]]]], J2[Plus[x, Times[-1, \[Epsilon]]]], J3[Plus[x, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, Times[-1, \[Epsilon]]], 3, 50]]]]]], List[Pi, 2, 1, Times[1, Power[2, -1]], Times[1, Power[3, -1]], 0]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, {}, 4, 4, 1], {6,3}],

	TestID -> "TMasters DRR Simple-Up 02"
]

VerificationTest[(* 339 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J3$J2$h", "J1", "J2", "J3", "J"]	,

	TestID -> "TMasters DRR Simple-Up 03"
]


(* ::Section::Closed:: *)
(*Simple DRR check (everything down)*)


VerificationTest[(* 340 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, \[Nu]]], Rule[J2, Power[7, \[Nu]]], Rule[J3, Power[5, \[Nu]]]]], Set[coefs, List[List[2, 0, 0], List[Power[5, \[Nu]], 7, 0], List[Power[5, \[Nu]], Power[7, \[Nu]], 5]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters DRR Simple-Down 01"
]

VerificationTest[(* 341 *)
	Map[Function[List[x], Plus[ReplaceAll[List[J1[Plus[x, 1, Times[-1, \[Epsilon]]]], J2[Plus[x, 1, Times[-1, \[Epsilon]]]], J3[Plus[x, 1, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, 1, Times[-1, \[Epsilon]]], 3, 50]], Times[-1, Dot[ReplaceAll[coefs, Rule[\[Nu], Plus[x, Times[-1, \[Epsilon]]]]], ReplaceAll[List[J1[Plus[x, Times[-1, \[Epsilon]]]], J2[Plus[x, Times[-1, \[Epsilon]]]], J3[Plus[x, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, Times[-1, \[Epsilon]]], 3, 50]]]]]], List[Pi, 2, 1, Times[1, Power[2, -1]], Times[1, Power[3, -1]], 0]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[6, 3]]	,

	TestID -> "TMasters DRR Simple-Down 02"
]

VerificationTest[(* 342 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J3$J2$h", "J1", "J2", "J3", "J"]	,

	TestID -> "TMasters DRR Simple-Down 03"
]


(* ::Section::Closed:: *)
(*Simple DRR check (mixed)*)


VerificationTest[(* 343 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, \[Nu]]], Rule[J2, Power[7, \[Nu]]], Rule[J3, Power[5, \[Nu]]]]], Set[coefs, List[List[2, 0, 0], List[Power[5, \[Nu]], 7, 0], List[Times[Power[Times[5, Power[2, -1]], \[Nu]], Times[1, Power[Power[\[Nu], 3], -1]]], Power[7, \[Nu]], 5]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters DRR Simple-Mixed 01"
]

VerificationTest[(* 344 *)
	Map[Function[List[x], Plus[ReplaceAll[List[J1[Plus[x, 1, Times[-1, \[Epsilon]]]], J2[Plus[x, 1, Times[-1, \[Epsilon]]]], J3[Plus[x, 1, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, 1, Times[-1, \[Epsilon]]], 3, 50]], Times[-1, Dot[ReplaceAll[coefs, Rule[\[Nu], Plus[x, Times[-1, \[Epsilon]]]]], ReplaceAll[List[J1[Plus[x, Times[-1, \[Epsilon]]]], J2[Plus[x, Times[-1, \[Epsilon]]]], J3[Plus[x, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, Times[-1, \[Epsilon]]], 3, 50]]]]]], List[Pi, 2, 1, Times[1, Power[2, -1]], Times[1, Power[3, -1]], 0]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[6, 3]]	,

	TestID -> "TMasters DRR Simple-Mixed 02"
]

VerificationTest[(* 345 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J3$J2$h", "J1", "J2", "J3", "J"]	,

	TestID -> "TMasters DRR Simple-Mixed 03"
]


(* ::Section::Closed:: *)
(*DRR BC5*)


VerificationTest[(* 346 *)
	CompoundExpression[Set[hom, List[Rule[BC51, Times[Gamma[Plus[5, Times[-1, Times[4, \[Nu]]]]], Power[Gamma[Plus[-1, \[Nu]]], 5], Sin[Times[4, Pi, \[Nu]]], Power[Gamma[Plus[-5, Times[5, \[Nu]]]], -1]]], Rule[BC52, Times[Power[Gamma[Plus[\[Nu], -1]], 4], Power[Times[Power[Gamma[Plus[Times[2, \[Nu]], -2]], 2], Gamma[Plus[Times[4, \[Nu]], -5]], Plus[Times[3, \[Nu]], -4]], -1]]], Rule[BC53, Times[Power[Gamma[Plus[\[Nu], -1]], 4], Power[Times[Power[Gamma[Plus[Times[2, \[Nu]], -2]], 2], Gamma[Plus[Times[4, \[Nu]], -5]], Plus[-3, Times[2, \[Nu]]], Sin[Times[Pi, \[Nu]]]], -1]]], Rule[BC54, Times[Gamma[Plus[4, Times[-1, Times[2, \[Nu]]]]], Gamma[Plus[2, Times[-1, Times[2, \[Nu]]]]], Gamma[Plus[Times[3, \[Nu]], -6]], Cos[Times[Pi, \[Nu]]], Power[Times[Gamma[Plus[Times[5, \[Nu]], -8]], Gamma[Plus[1, Times[-1, \[Nu]]]], Gamma[Plus[3, Times[-1, \[Nu]]]]], -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CompoundExpression[Set[mat, Get["ML5"]], CreateMasters["BC5", Part[mat, Span[1, 4], Span[1, 4]], \[Nu]]]], BC5[mSetSolution, hom, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters DRR BC5 01"
]

VerificationTest[(* 347 *)
	Map[Function[List[x], Plus[ReplaceAll[List[BC51[Plus[x, 1, Times[-1, \[Epsilon]]]], BC52[Plus[x, 1, Times[-1, \[Epsilon]]]], BC53[Plus[x, 1, Times[-1, \[Epsilon]]]], BC54[Plus[x, 1, Times[-1, \[Epsilon]]]]], BC5[mEvaluate, All, Plus[x, 1, Times[-1, \[Epsilon]]], 3, 20]], Times[-1, Dot[ReplaceAll[Part[mat, Span[1, 4], Span[1, 4]], Rule[\[Nu], Plus[x, Times[-1, \[Epsilon]]]]], ReplaceAll[List[BC51[Plus[x, Times[-1, \[Epsilon]]]], BC52[Plus[x, Times[-1, \[Epsilon]]]], BC53[Plus[x, Times[-1, \[Epsilon]]]], BC54[Plus[x, Times[-1, \[Epsilon]]]]], BC5[mEvaluate, All, Plus[x, Times[-1, \[Epsilon]]], 3, 20]]]]]], List[1, 2, Pi]],

	ConstantArray[SeriesData[\[Epsilon], 0, {}, 4, 4, 1], {3,4}],

	TestID -> "TMasters DRR BC5 02"
]

VerificationTest[(* 348 *)
	BC5[mPurge]
	,
	List["BC51$h", "BC52$h", "BC52$BC51$adapter", "BC52$BC51$h", "BC53$h", "BC53$BC51$adapter", "BC53$BC51$h", "BC54$h", "BC54$BC51$adapter", "BC54$BC51$h", "BC51", "BC52", "BC53", "BC54", "BC5"]	,

	TestID -> "TMasters DRR BC5 03"
]


(* ::Section::Closed:: *)
(*Order check*)


VerificationTest[(* 349 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, Times[-1, \[Nu]]]], Rule[J2, Power[7, Times[-1, \[Nu]]]], Rule[J3, Power[5, Times[-1, \[Nu]]]]]], Set[coefs, List[List[Times[1, Power[2, -1]], 0, 0], List[Power[5, Times[-1, \[Nu]]], Times[1, Power[7, -1]], 0], List[Times[Power[Times[2, Power[5, -1]], \[Nu]], Times[1, Power[Power[\[Nu], 3], -1]]], Power[7, Times[-1, \[Nu]]], Times[1, Power[5, -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters Order 01"
]

VerificationTest[(* 350 *)
	Map[Function[List[x], Part[ReplaceAll[List[J1[Plus[x, Times[-1, \[Epsilon]]]], J2[Plus[x, Times[-1, \[Epsilon]]]], J3[Plus[x, Times[-1, \[Epsilon]]]]], J[mEvaluate, All, Plus[x, Times[-1, \[Epsilon]]], 3, 30]], All, -2]], List[Pi, 2, 1, Times[1, Power[2, -1]], Times[1, Power[3, -1]], 0]]
	,
	ConstantArray[4, List[6, 3]]	,

	TestID -> "TMasters Order 02"
]

VerificationTest[(* 351 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J3$J2$h", "J1", "J2", "J3", "J"]	,

	TestID -> "TMasters Order 03"
]


(* ::Section::Closed:: *)
(*Export*)


VerificationTest[(* 352 *)
	CompoundExpression[Set[sols, List[Rule[J1, Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[Times[1, Power[2, -1]], \[Nu]], 2], -1]]], Rule[J2, Power[7, Times[-1, \[Nu]]]], Rule[J3, Power[5, Times[-1, \[Nu]]]], Rule[J4, Power[7, Times[-1, \[Nu]]]], Rule[J5, Power[6, Times[-1, \[Nu]]]], Rule[J6, Power[3, Times[-1, \[Nu]]]], Rule[J7, Power[9, Times[-1, \[Nu]]]]]], Set[coefs, List[List[Times[Power[Plus[1, Times[2, \[Nu]]], 2], Power[Times[2, Power[Plus[3, Times[2, \[Nu]]], 2]], -1]], 0, 0, 0, 0, 0, 0], List[0, Times[1, Power[7, -1]], 0, 0, 0, 0, 0], List[0, 0, Times[1, Power[5, -1]], 0, 0, 0, 0], List[0, 0, 0, Times[1, Power[7, -1]], 0, 0, 0], List[Times[1, Power[Power[Plus[\[Nu], Times[3, Power[2, -1]]], 2], -1]], Times[Power[2, \[Nu]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], 0, 0, Times[1, Power[6, -1]], 0, 0], List[0, 0, Times[Power[14, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Times[Power[13, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[7, Power[3, -1]]], 3], -1]], 0, Times[1, Power[3, -1]], 0], List[0, 0, 0, 0, Times[Power[13, \[Nu]], Power[Power[Plus[\[Nu], Times[7, Power[8, -1]]], 2], -1]], Times[Power[15, \[Nu]], Power[Power[Plus[\[Nu], Times[37, Power[27, -1]]], 5], -1]], Times[1, Power[9, -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters Export 01"
]

VerificationTest[(* 353 *)
	CompoundExpression[Set[vals1, Apply[Function[TriangleSumsSeries[Slot[2], List[\[Epsilon], 3], 100]], J[mExport, Plus[2, Times[-1, \[Epsilon]]]], List[1]]], Set[vals2, Apply[Function[TriangleSumsSeries[Slot[2], List[\[Epsilon], 3], 100]], J[mExport, Plus[3, Times[-1, \[Epsilon]]]], List[1]]], Set[vals3, J[mEvaluate, All, Plus[2, Times[-1, \[Epsilon]]], 3, 100]], List[Plus[vals2, Times[-1, Dot[ReplaceAll[coefs, Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], vals1]]], Plus[Map[Last, vals3], Times[-1, vals1]]]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[2, 7]]	,

	TestID -> "TMasters Export 02"
]

VerificationTest[(* 354 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J3$h", "J4$h", "J5$h", "J5$J1$adapter", "J5$J1$h", "J5$J2$adapter", "J5$J2$h", "J6$h", "J6$J3$adapter", "J6$J3$h", "J6$J4$adapter", "J6$J4$h", "J7$h", "J7$J5$adapter", "J7$J5$J1$h", "J7$J5$J2$h", "J7$J5$h", "J7$J6$adapter", "J7$J6$J3$h", "J7$J6$J4$h", "J7$J6$h", "J1", "J2", "J3", "J4", "J5", "J6", "J7", "J"]	,

	TestID -> "TMasters Export 03"
]


(* ::Section::Closed:: *)
(*mAddMasterByDRR*)


VerificationTest[(* 355 *)
	ONew[J, TMasters, List[]]
	,
	J	,

	TestID -> "TMasters mAddMasterByDRR 01"
]

VerificationTest[(* 356 *)
	WasAbort[J[mAddMasterByDRR, Rule[k1, Plus[Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k1], k2]], \[Nu]]]
	,
	True
	,
	{J::BadArgument},

	TestID -> "TMasters mAddMasterByDRR 02"
]

VerificationTest[(* 357 *)
	WasAbort[J[mAddMasterByDRR, Rule[k1, Plus[Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k1], Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k2]]], \[Nu]]]
	,
	True
	,
	{J::BadArgument},

	TestID -> "TMasters mAddMasterByDRR 03"
]

VerificationTest[(* 358 *)
	WasAbort[J[mAddMasterByDRR, Rule[k1, Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k1]], \[Nu]]]
	,
	False	,

	TestID -> "TMasters mAddMasterByDRR 04"
]

VerificationTest[(* 359 *)
	ONew[q, TMaster, List[]]
	,
	q
	,
	{q::NoTHomoInstance},

	TestID -> "TMasters mAddMasterByDRR 05"
]

VerificationTest[(* 360 *)
	WasAbort[J[mAddMasterByDRR, Rule[k3, Plus[Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k3], Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]]], -1]], q]]], \[Nu]]]
	,
	True
	,
	{J::BadDRR},

	TestID -> "TMasters mAddMasterByDRR 06"
]

VerificationTest[(* 361 *)
	WasAbort[J[mAddMasterByDRR, Rule[k1, Times[Times[Power[Plus[-1, \[Nu]], 3], Power[Times[40, Plus[-1, Times[2, \[Nu]]], Plus[-3, Times[4, \[Nu]]], Plus[-1, Times[4, \[Nu]]], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]], Plus[-2, Times[5, \[Nu]]], Plus[-1, Times[5, \[Nu]]]], -1]], k1]], \[Nu]]]
	,
	True
	,
	{J::AlreadyExists},

	TestID -> "TMasters mAddMasterByDRR 07"
]

VerificationTest[(* 362 *)
	CompoundExpression[ODelete[q], J[mPurge]]
	,
	List["k1$h", "k1", "J"]	,

	TestID -> "TMasters mAddMasterByDRR 08"
]


(* ::Section::Closed:: *)
(*mCutZeroBranches*)


VerificationTest[(* 363 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, Times[-1, \[Nu]]]], Rule[J2, 0], Rule[J3, Power[5, Times[-1, \[Nu]]]], Rule[J4, 0], Rule[J5, Power[7, Times[-1, \[Nu]]]], Rule[J6, 0]]], Set[coefs, List[List[Times[1, Power[2, -1]], 0, 0, 0, 0, 0], List[Power[5, Times[-1, \[Nu]]], Times[1, Power[7, -1]], 0, 0, 0, 0], List[Times[Power[Times[2, Power[5, -1]], \[Nu]], Times[1, Power[Power[\[Nu], 3], -1]]], Power[7, Times[-1, \[Nu]]], Times[1, Power[5, -1]], 0, 0, 0], List[Times[1, Power[Power[\[Nu], 5], -1]], Power[3, Times[-1, \[Nu]]], Power[2, Times[-1, \[Nu]]], Times[1, Power[27, -1]], 0, 0], List[Times[Power[22, Times[-1, \[Nu]]], Power[Power[\[Nu], 3], -1]], Power[12, Times[-1, \[Nu]]], Power[35, Times[-1, \[Nu]]], Times[1, Power[Factorial[\[Nu]], -1]], Times[1, Power[7, -1]], 0], List[Times[1, Power[Power[\[Nu], 7], -1]], Times[Power[2, Times[-1, \[Nu]]], Power[Power[\[Nu], 2], -1]], Power[19, Times[-1, \[Nu]]], Times[1, Power[Power[\[Nu], 3], -1]], Power[5, Times[-1, \[Nu]]], Times[1, Power[12, -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters mCutZeroBranches 01"
]

VerificationTest[(* 364 *)
	J[mCutZeroBranches, True]
	,
	List[J3$J2$h, J4$J2$h, J4$J3$J2$h, J5$J2$h, J5$J3$J2$h, J5$J4$J2$h, J5$J4$J3$J2$h, J5$J4$h, J6$J2$h, J6$J3$J2$h, J6$J4$J2$h, J6$J4$J3$J2$h, J6$J4$h, J6$J5$J2$h, J6$J5$J3$J2$h, J6$J5$J4$J2$h, J6$J5$J4$J3$J2$h, J6$J5$J4$h]	,

	TestID -> "TMasters mCutZeroBranches 02"
]

VerificationTest[(* 365 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J4$h", "J4$J1$adapter", "J4$J1$h", "J4$J2$adapter", "J4$J2$J1$h", "J4$J3$adapter", "J4$J3$J1$h", "J4$J3$J2$J1$h", "J4$J3$h", "J5$h", "J5$J1$adapter", "J5$J1$h", "J5$J2$adapter", "J5$J2$J1$h", "J5$J3$adapter", "J5$J3$J1$h", "J5$J3$J2$J1$h", "J5$J3$h", "J5$J4$adapter", "J5$J4$J1$h", "J5$J4$J2$J1$h", "J5$J4$J3$J1$h", "J5$J4$J3$J2$J1$h", "J5$J4$J3$h", "J6$h", "J6$J1$adapter", "J6$J1$h", "J6$J2$adapter", "J6$J2$J1$h", "J6$J3$adapter", "J6$J3$J1$h", "J6$J3$J2$J1$h", "J6$J3$h", "J6$J4$adapter", "J6$J4$J1$h", "J6$J4$J2$J1$h", "J6$J4$J3$J1$h", "J6$J4$J3$J2$J1$h", "J6$J4$J3$h", "J6$J5$adapter", "J6$J5$J1$h", "J6$J5$J2$J1$h", "J6$J5$J3$J1$h", "J6$J5$J3$J2$J1$h", "J6$J5$J3$h", "J6$J5$J4$J1$h", "J6$J5$J4$J2$J1$h", "J6$J5$J4$J3$J1$h", "J6$J5$J4$J3$J2$J1$h", "J6$J5$J4$J3$h", "J6$J5$h", "J1", "J2", "J3", "J4", "J5", "J6", "J"]	,

	TestID -> "TMasters mCutZeroBranches 03"
]


(* ::Section::Closed:: *)
(*mRangeEvaluate*)


VerificationTest[(* 366 *)
	CompoundExpression[Set[sols, List[Rule[J1, Power[2, Times[-1, \[Nu]]]], Rule[J2, Power[7, Times[-1, \[Nu]]]], Rule[J3, Power[5, Times[-1, \[Nu]]]]]], Set[coefs, List[List[Times[1, Power[2, -1]], 0, 0], List[Power[5, Times[-1, \[Nu]]], Times[1, Power[7, -1]], 0], List[Times[Power[Times[2, Power[5, -1]], \[Nu]], Times[1, Power[Power[\[Nu], 3], -1]]], Power[7, \[Nu]], Times[1, Power[5, -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CreateMasters["J", coefs, \[Nu]]], J[mSetSolution, sols, \[Nu]]]
	,
	Null	,

	TestID -> "TMasters mRangeEvaluate 01"
]

VerificationTest[(* 367 *)
	Chop[Plus[Part[J[mRangeEvaluate, All, Plus[2, Times[-1, \[Epsilon]]], 3, -10, 50], All, All, 2], Times[-1, Map[Function[Part[J[mEvaluate, All, Plus[2, Times[-1, Slot[1]], Times[-1, \[Epsilon]]], 3, 50], All, 2]], Range[0, 9]]]], Power[10, -35]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[10, 3]]	,

	TestID -> "TMasters mRangeEvaluate 02"
]

VerificationTest[(* 368 *)
	Chop[Plus[Part[J[mRangeEvaluate, All, Plus[2, Times[-1, \[Epsilon]]], 3, 10, 50], All, All, 2], Times[-1, Map[Function[Part[J[mEvaluate, All, Plus[2, Slot[1], Times[-1, \[Epsilon]]], 3, 50], All, 2]], Range[0, 9]]]], Power[10, -41]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[10, 3]]	,

	TestID -> "TMasters mRangeEvaluate 03"
]

VerificationTest[(* 369 *)
	J[mPurge]
	,
	List["J1$h", "J2$h", "J2$J1$adapter", "J2$J1$h", "J3$h", "J3$J1$adapter", "J3$J1$h", "J3$J2$adapter", "J3$J2$J1$h", "J3$J2$h", "J1", "J2", "J3", "J"]	,

	TestID -> "TMasters mRangeEvaluate 04"
]


(* ::Section::Closed:: *)
(*mParameters*)


VerificationTest[
	mat={{\[Nu]^3/(1+\[Nu])^3,0,0,0},{1/\[Nu]^5,\[Nu]/(1+\[Nu]),0,0},{1/\[Nu]^5,1/\[Nu]^3,\[Nu]^2/(1+\[Nu])^2,0},{1/\[Nu]^5,1/\[Nu]^3,1/\[Nu]^2,\[Nu]^2/(1+\[Nu])^2}};,

	Null,

	TestID -> "TMasters mParameters 01"
]


VerificationTest[
	Block[{Print=##1&},
		CreateMasters["J", mat, \[Nu]];
	],

	Null,

	TestID -> "TMasters mParameters 02"
]


VerificationTest[
	J[mSetSolution, {J1->1/\[Nu]^3,J2->1/\[Nu],J3->1/\[Nu]^2,J4->1/\[Nu]^2}, \[Nu]];,

	Null,

	TestID -> "TMasters mParameters 03"
]


VerificationTest[
	J[mEvaluate, All, 2-\[Epsilon], 3, 50];,

	Null,

	TestID -> "TMasters mParameters 04"
]


VerificationTest[
	J[mParameters, J2$J1$adapter],

	{J2$J1$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}}},

	TestID -> "TMasters mParameters 05"
]


VerificationTest[
	J[mParameters, J4],

	{J4$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,56,82,3}}},

	TestID -> "TMasters mParameters 06"
]


VerificationTest[
	J[mParameters, {J1, J4$J2$J1$h}],

	{J4$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}}},

	TestID -> "TMasters mParameters 07"
]


VerificationTest[
	J[mParameters, {J1, J2}],

	{J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}},J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{91,134,3,148,219,3}},J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}}},

	TestID -> "TMasters mParameters 08"
]


VerificationTest[
	J[mParameters, {J1$h,J2$h}],

	{J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}},J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}}},

	TestID -> "TMasters mParameters 09"
]


VerificationTest[
	J[mParameters],

	{J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}},J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}},J2$J1$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{148,219,3,148,219,3}},J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{91,134,3,148,219,3}},J3$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{91,134,3,91,134,3}},J3$J1$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{91,134,3,91,134,3}},J3$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,91,134,3}},J3$J2$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{91,134,3,91,134,3}},J3$J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,91,134,3}},J3$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,91,134,3}},J4$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,56,82,3}},J4$J1$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,56,82,3}},J4$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J2$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,56,82,3}},J4$J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$adapter->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{56,82,3,56,82,3}},J4$J3$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J2$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}},J4$J3$J2$J1$h->{"-"->{Null,Null,Null,Null,Null,Null},"~"->{Null,Null,Null,Null,Null,Null},"+"->{1,50,3,56,82,3}}},

	TestID -> "TMasters mParameters 10"
]
