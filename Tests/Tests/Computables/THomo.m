(* ::Package:: *)

(* ::Section:: *)
(*Simple*)


VerificationTest[(* 94 *)
	ONew[j, THomo, Times[Power[Plus[1, Times[2, \[Nu]]], 2], Power[Times[2, Power[Plus[3, Times[2, \[Nu]]], 2]], -1]], \[Nu]]
	,
	j,

	TestID -> "THomo Simple 01"
]

VerificationTest[(* 95 *)
	j[mAsymptotics, "+"]
	,
	List[List[Times[1, Power[2, -1]], 2, 0]]	,

	TestID -> "THomo Simple 02"
]

VerificationTest[(* 96 *)
	j[mAsymptotics, "-"]
	,
	List[List[Times[1, Power[2, -1]], 2, 0]]	,

	TestID -> "THomo Simple 03"
]

VerificationTest[(* 97 *)
	CompoundExpression[j[mRequest, None, "+", List[50, 100]], j[mRequest, None, "-", List[50, 100]], j[mSendRequests], j[mSetDim, Plus[2, Times[-1, \[Epsilon]]]]]
	,
	Null	,

	TestID -> "THomo Simple 04"
]

VerificationTest[(* 98 *)
	j[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[]	,

	TestID -> "THomo Simple 05"
]

VerificationTest[(* 99 *)
	j[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[]	,

	TestID -> "THomo Simple 06"
]

VerificationTest[(* 100 *)
	CompoundExpression[j[mOrderRequest, None, "+", 2], j[mOrderRequest, None, "-", 2], j[mSendOrderRequests]]
	,
	Null	,

	TestID -> "THomo Simple 07"
]

VerificationTest[(* 101 *)
	Plus[j[mEvaluate, None, "+"], Times[-1, Table[DREAM`ZeroSeries`ZeroNSeries[Times[ReplaceAll[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]], k]]], Power[ReplaceAll[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], -1]], List[\[Epsilon], 2], 90], List[k, 0, 49]]]]
	,
	ConstantArray[DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 2]], List[50]]	,

	TestID -> "THomo Simple 08"
]

VerificationTest[(* 102 *)
	Plus[j[mEvaluate, None, "-"], Times[-1, Table[DREAM`ZeroSeries`ZeroNSeries[Times[ReplaceAll[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]], k]]], Power[ReplaceAll[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], -1]], List[\[Epsilon], 2], 90], List[k, 0, -49, -1]]]]
	,
	ConstantArray[DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 2]], List[50]]	,

	TestID -> "THomo Simple 09"
]

VerificationTest[(* 103 *)
	CompoundExpression[j[mSetSolution, Times[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Sin[Times[2, Pi, \[Nu]]]], \[Nu]], Plus[j[mSeed], Times[-1, DREAM`ZeroSeries`ZeroSeries[ReplaceAll[Times[Times[Power[2, Times[-1, \[Nu]]], Power[Power[Plus[\[Nu], Times[1, Power[2, -1]]], 2], -1]], Sin[Times[2, Pi, \[Nu]]]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 2]]]]]
	,
	DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 2]]	,

	TestID -> "THomo Simple 10"
]

VerificationTest[(* 104 *)
	ODelete[j]
	,
	Null	,

	TestID -> "THomo Simple 11"
]
