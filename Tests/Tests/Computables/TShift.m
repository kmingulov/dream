(* ::Package:: *)

(* ::Section::Closed:: *)
(*Positive shift*)


VerificationTest[
	{ONew[cert,TCert,(\[Nu]-5)/((\[Nu]-2)(\[Nu]-3)(\[Nu]+5)),\[Nu]], ONew[shift,TShift,cert,2]},

	{cert, shift},

	TestID -> "TShift Positive 01"
]


VerificationTest[
	types={"+","-"};
	l={shift,cert};
	#[mClear]&/@l;
	Function[{type},#[mRequest,None,type,{10,10}]&/@l]/@types;
	#[mSendRequests]&/@l;
	#[mSetDim,2-\[Epsilon]]&/@l;
	Function[{type},#[mOrderRequest,None,type,3]&/@l]/@types;,

	Null,

	TestID -> "TShift Positive 02"
]


VerificationTest[
	cert[DREAM`Hidden`mLowestOrder,#]&/@types,

	{{{1,-1},{2,-2},{4,-1}},{{7,1}}},

	TestID -> "TShift Positive 03"
]


VerificationTest[
	shift[DREAM`Hidden`mLowestOrder,#]&/@types,

	{{{0,-2},{2,-1}},{{0,-2},{1,-1},{2,0},{9,1}}},

	TestID -> "TShift Positive 04"
]


VerificationTest[
	#[mSendOrderRequests]&/@l;
	l1=Join[Reverse@cert[mEvaluate,None,"-"][[2;;]],cert[mEvaluate,None,"+"]];
	l2=Join[Reverse@shift[mEvaluate,None,"-"][[2;;]],shift[mEvaluate,None,"+"]];
	l1[[3;;]]-l2[[;;-3]],

	Join[ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon],{},4,4],7],{0},ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon],{},4,4],9]],

	TestID -> "TShift Positive 05"
]


VerificationTest[
	ODelete[shift,cert];,

	Null,

	TestID -> "TShift Positive 06"
]
