(* ::Package:: *)

(* ::Section:: *)
(*Simple*)


VerificationTest[(* 88 *)
	ONew[func, TFunction, Function[N[Series[Exp[Slot[1]], List[Slot[2], 0, Slot[3]]], Slot[4]]]]
	,
	func,

	TestID -> "TFunction Simple 01"
]

VerificationTest[(* 89 *)
	ONew[J, TMasters, List[]]
	,
	J,

	TestID -> "TFunction Simple 02"
]

VerificationTest[(* 90 *)
	J[mAddMasterByObjects, J1, List[func]]
	,
	Null
	,
	{J1::NoTHomoInstance},

	TestID -> "TFunction Simple 03"
]

VerificationTest[(* 91 *)
	Part[J[mRangeEvaluate, J1, Plus[0, Times[-1, \[Epsilon]]], 3, 10, 100], All, 1, 2]
	,
	Table[N[Series[Exp[Plus[k, Times[-1, \[Epsilon]]]], List[\[Epsilon], 0, 3]], 100], List[k, 0, 9]],

	TestID -> "TFunction Simple 04"
]

VerificationTest[(* 92 *)
	Part[J[mRangeEvaluate, J1, Plus[0, Times[-1, \[Epsilon]]], 3, -10, 100], All, 1, 2]
	,
	Table[N[Series[Exp[Plus[k, Times[-1, \[Epsilon]]]], List[\[Epsilon], 0, 3]], 100], List[k, 0, -9, -1]],

	TestID -> "TFunction Simple 05"
]

VerificationTest[(* 93 *)
	J[mPurge]
	,
	List["func", "J1", "J"],

	TestID -> "TFunction Simple 06"
]
