(* ::Package:: *)

(* ::Section::Closed:: *)
(*Scalar*)


VerificationTest[(* 323 *)
	ONew[u, TUnity]
	,
	u,

	TestID -> "TUnity Scalar 01"
]

VerificationTest[(* 324 *)
	List[u[mAsymptotics, "+"], u[mAsymptotics, "-"]]
	,
	List[List[List[1, 0, 0]], List[List[1, 0, 0]]]	,

	TestID -> "TUnity Scalar 02"
]

VerificationTest[(* 325 *)
	CompoundExpression[u[mRequest, None, "+", List[50, 100]], u[mRequest, None, "-", List[50, 100]], u[mSendRequests], u[mSetDim, Plus[2, Times[-1, \[Epsilon]]]], u[mOrderRequest, None, "+", 3], u[mOrderRequest, None, "-", 3], List[u[DREAM`Hidden`mLowestOrder, "+"], u[DREAM`Hidden`mLowestOrder, "-"]]]
	,
	List[List[], List[]]	,

	TestID -> "TUnity Scalar 03"
]

VerificationTest[(* 326 *)
	u[mSendOrderRequests]
	,
	Null	,

	TestID -> "TUnity Scalar 04"
]

VerificationTest[(* 327 *)
	u[mEvaluate, None, "+"]
	,
	ConstantArray[DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], List[50]]	,

	TestID -> "TUnity Scalar 05"
]

VerificationTest[(* 328 *)
	u[mEvaluate, None, "-"]
	,
	ConstantArray[DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], List[50]]	,

	TestID -> "TUnity Scalar 06"
]

VerificationTest[(* 329 *)
	ODelete[u]
	,
	Null	,

	TestID -> "TUnity Scalar 07"
]


(* ::Section::Closed:: *)
(*Matrix*)


VerificationTest[(* 330 *)
	ONew[u, TUnity, List[3, 5]]
	,
	u	,

	TestID -> "TUnity Matrix 01"
]

VerificationTest[(* 331 *)
	List[u[mAsymptotics, "+"], u[mAsymptotics, "-"]]
	,
	List[List[List[1, 0, 0]], List[List[1, 0, 0]]]	,

	TestID -> "TUnity Matrix 02"
]

VerificationTest[(* 332 *)
	CompoundExpression[u[mRequest, None, "+", List[50, 100]], u[mRequest, None, "-", List[50, 100]], u[mSendRequests], u[mSetDim, Plus[2, Times[-1, \[Epsilon]]]], u[mOrderRequest, None, "+", 3], u[mOrderRequest, None, "-", 3], List[u[DREAM`Hidden`mLowestOrder, "+"], u[DREAM`Hidden`mLowestOrder, "-"]]]
	,
	List[List[], List[]]	,

	TestID -> "TUnity Matrix 03"
]

VerificationTest[(* 333 *)
	u[mSendOrderRequests]
	,
	Null	,

	TestID -> "TUnity Matrix 04"
]

VerificationTest[(* 334 *)
	u[mEvaluate, None, "+"]
	,
	ConstantArray[List[List[DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0, 0, 0], List[0, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0, 0], List[0, 0, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0]], List[50]]	,

	TestID -> "TUnity Matrix 05"
]

VerificationTest[(* 335 *)
	u[mEvaluate, None, "-"]
	,
	ConstantArray[List[List[DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0, 0, 0], List[0, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0, 0], List[0, 0, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 100], 0, 0]], List[50]]	,

	TestID -> "TUnity Matrix 06"
]

VerificationTest[(* 336 *)
	ODelete[u]
	,
	Null	,

	TestID -> "TUnity Matrix 07"
]
