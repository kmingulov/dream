(* ::Package:: *)

(* ::Section:: *)
(*Simple*)


VerificationTest[(* 105 *)
	CompoundExpression[ONew[seed, TExpr, Power[2, \[Nu]], \[Nu]], ONew[rec, TRecurrence, seed, List[1, -2, 0], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence Simple 01"
]

VerificationTest[(* 106 *)
	CompoundExpression[rec[mRequest, None, "+", List[100, 200]], rec[mRequest, None, "-", List[100, 200]], Map[Function[Slot[1][mSendRequests]], List[rec, seed]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[rec, seed]], rec[mOrderRequest, None, "+", 2], rec[mOrderRequest, None, "-", 2], Map[Function[Slot[1][mSendOrderRequests]], List[rec, seed]]]
	,
	List[Null, Null]	,

	TestID -> "TRecurrence Simple 02"
]

VerificationTest[(* 107 *)
	Normal[Plus[ToSD[Part[rec[mEvaluate, rec, "~"], 1]], Times[-1, Power[2, Plus[2, Times[-1, \[Epsilon]]]]]]]
	,
	0	,

	TestID -> "TRecurrence Simple 03"
]

VerificationTest[(* 108 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "+"]], Times[-1, Table[Power[2, Plus[2, Times[-1, \[Epsilon]], k]], List[k, 0, 99]]]]]
	,
	List[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]	,

	TestID -> "TRecurrence Simple 04"
]

VerificationTest[(* 109 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "-"]], Times[-1, Table[Power[2, Plus[2, Times[-1, \[Epsilon]], k]], List[k, 0, -99, -1]]]]]
	,
	List[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]	,

	TestID -> "TRecurrence Simple 05"
]

VerificationTest[(* 110 *)
	ODelete[seed, rec]
	,
	Null	,

	TestID -> "TRecurrence Simple 06"
]


(* ::Section:: *)
(*Fibonacci*)


VerificationTest[(* 111 *)
	CompoundExpression[ONew[func, TFunction, Function[N[Fibonacci[ReplaceAll[Slot[1], Rule[Slot[2], 0]]], Slot[4]]]], ONew[rec, TRecurrence, func, List[1, -1, -1, 0], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence Fibonacci 01"
]

VerificationTest[(* 112 *)
	CompoundExpression[rec[mRequest, None, "+", List[100, 50]], rec[mRequest, None, "-", List[100, 50]], Map[Function[Slot[1][mSendRequests]], List[rec, func]], Map[Function[Slot[1][mSetDim, Plus[0, Times[-1, \[Epsilon]]]]], List[rec, func]], rec[mOrderRequest, None, "+", 2], rec[mOrderRequest, None, "-", 2], Map[Function[Slot[1][mSendOrderRequests]], List[rec, func]]]
	,
	List[Null, Null]	,

	TestID -> "TRecurrence Fibonacci 02"
]

VerificationTest[(* 113 *)
	Plus[Part[rec[mEvaluate, rec, "~"], 1], Times[-1, Fibonacci[0]]]
	,
	0	,

	TestID -> "TRecurrence Fibonacci 03"
]

VerificationTest[(* 114 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "+"]], Times[-1, Map[Fibonacci, Range[0, 99]]]]]
	,
	List[0, 0``50., 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]	,

	TestID -> "TRecurrence Fibonacci 04"
]

VerificationTest[(* 115 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "-"]], Times[-1, Map[Fibonacci, Range[0, -99, -1]]]]]
	,
	List[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]	,

	TestID -> "TRecurrence Fibonacci 05"
]

VerificationTest[(* 116 *)
	ODelete[rec, func]
	,
	Null	,

	TestID -> "TRecurrence Fibonacci 06"
]


(* ::Section:: *)
(*Complex*)


VerificationTest[(* 117 *)
	CompoundExpression[SetDelayed[q[Plus[0, Times[-1, \[Epsilon]]]], Series[\[Epsilon], List[\[Epsilon], 0, 3]]], SetDelayed[q[Plus[1, Times[-1, \[Epsilon]]]], Series[Plus[1, Power[\[Epsilon], 2]], List[\[Epsilon], 0, 3]]], SetDelayed[q[Plus[2, Times[-1, \[Epsilon]]]], Series[Plus[Times[1, Power[\[Epsilon], -1]], \[Epsilon]], List[\[Epsilon], 0, 3]]], SetDelayed[Condition[q[Pattern[\[Nu], Plus[Pattern[n, Blank[]], Times[-1, \[Epsilon]]]]], GreaterEqual[n, 3]], Set[q[\[Nu]], Simplify[Times[Times[-1, Plus[\[Nu], -3]], Plus[Times[Times[\[Nu], Power[Plus[\[Nu], -2], -1]], q[Plus[\[Nu], -1]]], Times[Times[1, Power[Plus[\[Nu], -3], -1]], q[Plus[\[Nu], -2]]], Times[Times[1, Power[Power[Plus[\[Nu], -3], 2], -1]], q[Plus[\[Nu], -3]]], 1]]]]], SetDelayed[Condition[q[Pattern[\[Nu], Plus[Pattern[n, Blank[]], Times[-1, \[Epsilon]]]]], Less[n, 0]], Set[q[\[Nu]], Times[Times[-1, Power[\[Nu], 2]], Plus[Times[Times[1, Power[\[Nu], -1]], q[Plus[\[Nu], 3]]], Times[Times[Plus[\[Nu], 3], Power[Plus[\[Nu], 1], -1]], q[Plus[\[Nu], 2]]], Times[Times[1, Power[\[Nu], -1]], q[Plus[\[Nu], 1]]], 1]]]], ONew[func, TFunction, Function[N[q[Slot[1]], Slot[4]]]], ONew[rec, TRecurrence, func, List[Times[1, Power[\[Nu], -1]], Times[Plus[\[Nu], 3], Power[Plus[\[Nu], 1], -1]], Times[1, Power[\[Nu], -1]], Times[1, Power[Power[\[Nu], 2], -1]], 1], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence Complex 01"
]

VerificationTest[(* 118 *)
	CompoundExpression[rec[mRequest, None, "+", List[100, 200]], rec[mRequest, None, "-", List[100, 200]], Map[Function[Slot[1][mSendRequests]], List[rec, func]], Map[Function[Slot[1][mSetDim, Plus[0, Times[-1, \[Epsilon]]]]], List[rec, func]], rec[mOrderRequest, None, "+", 2], rec[mOrderRequest, None, "-", 2], Map[Function[Slot[1][mSendOrderRequests]], List[rec, func]]]
	,
	List[Null, Null]	,

	TestID -> "TRecurrence Complex 02"
]

VerificationTest[(* 119 *)
	Normal[Plus[ToSD[Part[rec[mEvaluate, rec, "~"], 1]], Times[-1, q[Plus[0, Times[-1, \[Epsilon]]]]]]]
	,
	0	,

	TestID -> "TRecurrence Complex 03"
]

VerificationTest[(* 120 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "+"]], Times[-1, Table[q[Plus[k, Times[-1, \[Epsilon]]]], List[k, 0, 99]]]]]
	,
	ConstantArray[0, 100]	,

	TestID -> "TRecurrence Complex 04"
]

VerificationTest[(* 121 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "-"]], Times[-1, Table[q[Plus[k, Times[-1, \[Epsilon]]]], List[k, 0, -99, -1]]]]]
	,
	ConstantArray[0, 100]	,

	TestID -> "TRecurrence Complex 05"
]

VerificationTest[(* 122 *)
	CompoundExpression[ODelete[rec, func], ClearAll[q]]
	,
	Null	,

	TestID -> "TRecurrence Complex 06"
]


(* ::Section:: *)
(*BC5*)


VerificationTest[(* 123 *)
	CompoundExpression[Set[hom, List[Rule[BC51, 0], Rule[BC52, Times[Power[Gamma[Plus[\[Nu], -1]], 4], Power[Times[Power[Gamma[Plus[Times[2, \[Nu]], -2]], 2], Gamma[Plus[Times[4, \[Nu]], -5]], Plus[Times[3, \[Nu]], -4]], -1]]]]], Block[List[Set[Print, Function[SlotSequence[1]]]], CompoundExpression[Set[mat, Get["ML5"]], CreateMasters["BC5", Part[mat, Span[1, 2], Span[1, 2]], \[Nu]]]], BC5[mSetSolution, hom, \[Nu]]]
	,
	Null	,

	TestID -> "TRecurrence BC5 01"
]

VerificationTest[(* 124 *)
	CompoundExpression[ONew[func, TFunction, Function[ReplaceAll[BC52[Slot[1]], BC5[mEvaluate, BC52, Slot[1], Slot[3], Slot[4]]]]], ONew[rec, TRecurrence, func, List[1, Times[-1, Times[Plus[-1, \[Nu]], Plus[-4, Times[3, \[Nu]]], Power[Times[32, Power[Plus[-1, Times[2, \[Nu]]], 3], Plus[-1, Times[3, \[Nu]]], Plus[-5, Times[4, \[Nu]]], Plus[-3, Times[4, \[Nu]]]], -1]]], Times[Times[Plus[-1, \[Nu]], Plus[77760, Times[-1, Times[578376, \[Nu]]], Times[1767434, Power[\[Nu], 2]], Times[-1, Times[2837213, Power[\[Nu], 3]]], Times[2520671, Power[\[Nu], 4]], Times[-1, Times[1174050, Power[\[Nu], 5]]], Times[223848, Power[\[Nu], 6]]], Power[Times[960, Power[Plus[-1, Times[2, \[Nu]]], 3], Plus[-4, Times[3, \[Nu]]], Plus[-2, Times[3, \[Nu]]], Plus[-1, Times[3, \[Nu]]], Plus[-5, Times[4, \[Nu]]], Power[Plus[-3, Times[4, \[Nu]]], 2], Plus[-4, Times[5, \[Nu]]], Plus[-3, Times[5, \[Nu]]]], -1]], 0]], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence BC5 02"
]

VerificationTest[(* 125 *)
	CompoundExpression[rec[mRequest, None, "+", List[10, 100]], rec[mRequest, None, "-", List[10, 100]], Map[Function[Slot[1][mSendRequests]], List[rec, func]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[rec, func]], rec[mOrderRequest, None, "+", 3], rec[mOrderRequest, None, "-", 3], Map[Function[Slot[1][mSendOrderRequests]], List[rec, func]]]
	,
	List[Null, Null]	,

	TestID -> "TRecurrence BC5 03"
]

VerificationTest[(* 126 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "+"]], Times[-1, Part[BC5[mRangeEvaluate, BC52, Plus[2, Times[-1, \[Epsilon]]], 3, 10, 100], All, 1, 2]]]]
	,
	ConstantArray[0, 10]	,

	TestID -> "TRecurrence BC5 04"
]

VerificationTest[(* 127 *)
	Normal[Plus[Map[ToSD, rec[mEvaluate, None, "-"]], Times[-1, Part[BC5[mRangeEvaluate, BC52, Plus[2, Times[-1, \[Epsilon]]], 3, -10, 100], All, 1, 2]]]]
	,
	ConstantArray[0, 10]	,

	TestID -> "TRecurrence BC5 05"
]

VerificationTest[(* 128 *)
	CompoundExpression[ODelete[rec, func], BC5[mPurge], Null]
	,
	Null	,

	TestID -> "TRecurrence BC5 06"
]


(* ::Section:: *)
(*Sum (up)*)


VerificationTest[(* 129 *)
	CompoundExpression[ONew[seed, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[\[Nu], -1]], \[Nu]], ONew[rec, TRecurrence, seed, List[1, Times[-1, Times[\[Nu], Power[Times[2, Plus[\[Nu], 1]], -1]]], 0], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence SumUp 01"
]

VerificationTest[(* 130 *)
	CompoundExpression[ONew[J, TMasters, List[]], J[mAddMasterByObjects, j1, List[rec]], J[mAddMasterByDRR, Rule[j2, Plus[Times[Times[1, Power[2, -1]], j2], Times[Times[1, Power[Power[\[Nu], 3], -1]], j1]]], \[Nu]]]
	,
	Null
	,
	{j1::NoTHomoInstance},

	TestID -> "TRecurrence SumUp 02"
]

VerificationTest[(* 131 *)
	Normal[Plus[ToSD[ReplaceAll[j2[Plus[2, Times[-1, \[Epsilon]]]], J[mEvaluate, j2, Plus[2, Times[-1, \[Epsilon]]], 3, 100]]], Times[-1, N[Series[ReplaceAll[ReplaceAll[Times[Times[-1, Times[1, Power[3, -1]]], Power[2, Plus[Times[-1, q], Times[-1, \[Nu]]]], PolyGamma[3, Plus[q, \[Nu]]]], Rule[q, 0]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], 100]]]]
	,
	0,

	TestID -> "TRecurrence SumUp 03"
]

VerificationTest[(* 132 *)
	CompoundExpression[J[mPurge], Null]
	,
	Null	,

	TestID -> "TRecurrence SumUp 04"
]


(* ::Section:: *)
(*Sum (down)*)


VerificationTest[(* 133 *)
	CompoundExpression[ONew[seed, TExpr, Times[Power[2, \[Nu]], Power[\[Nu], -1]], \[Nu]], ONew[rec, TRecurrence, seed, List[1, Times[-1, Times[2, \[Nu], Power[Plus[\[Nu], 1], -1]]], 0], \[Nu]]]
	,
	rec	,

	TestID -> "TRecurrence SumDown 01"
]

VerificationTest[(* 134 *)
	CompoundExpression[ONew[J, TMasters, List[]], J[mAddMasterByObjects, j1, List[rec]], J[mAddMasterByDRR, Rule[j2, Plus[Times[Times[1, Power[2, -1]], j2], Times[Times[1, Power[Power[\[Nu], 3], -1]], j1]]], \[Nu]]]
	,
	Null
	,
	{j1::NoTHomoInstance},

	TestID -> "TRecurrence SumDown 02"
]

VerificationTest[(* 135 *)
	Normal[Plus[ToSD[ReplaceAll[j2[Plus[-2, Times[-1, \[Epsilon]]]], J[mEvaluate, j2, Plus[-2, Times[-1, \[Epsilon]]], 3, 100]]], Times[-1, ReplaceAll[Times[Power[2, Plus[-1, \[Nu]]], HurwitzLerchPhi[Times[1, Power[4, -1]], 4, Plus[1, Times[-1, \[Nu]]]]], Rule[\[Nu], Plus[-2, Times[-1, \[Epsilon]]]]]]]],
	0,

	TestID -> "TRecurrence SumDown 03"
]

VerificationTest[(* 136 *)
	CompoundExpression[J[mPurge], Null]
	,
	Null	,

	TestID -> "TRecurrence SumDown 04"
]
