(* ::Package:: *)

(* ::Section::Closed:: *)
(*Up*)


VerificationTest[(* 137 *)
	List[ONew[a, TExpr, Power[2, Times[-1, \[Nu]]], \[Nu]], ONew[h1, THomo, Times[2, Power[3, -1]], \[Nu]], ONew[h2, THomo, Times[1, Power[2, -1]], \[Nu]], ONew[s, TSum, h2, a, h1, "+"]]
	,
	List[a, h1, h2, s]	,

	TestID -> "TSum Up 01"
]

VerificationTest[(* 138 *)
	CompoundExpression[s[mRequest, None, "-", List[50, 100]], s[mRequest, None, "+", List[50, 100]], s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, h1, h2, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, h1, h2, a]]]
	,
	List[Null, Null, Null, Null]	,

	TestID -> "TSum Up 02"
]

VerificationTest[(* 139 *)
	s[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[]	,

	TestID -> "TSum Up 03"
]

VerificationTest[(* 140 *)
	s[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[]	,

	TestID -> "TSum Up 04"
]

VerificationTest[(* 141 *)
	CompoundExpression[s[mOrderRequest, None, "-", 3], s[mOrderRequest, None, "+", 3], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, h1, h2, a]]]
	,
	List[Null, Null, Null, Null]	,

	TestID -> "TSum Up 05"
]

VerificationTest[(* 142 *)
	N[s[mEvaluate, None, "~"], 95]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.7499999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999283483625474081`95., 0.5198603854199589820629240910936324260566251007701914405905100071200452164772710367043974952473140156068359802826605`95., 0.1801698802193255342501634473724993643989573568479545950750740501088745183474379270749805987914726215825557265398735`95., 0.041628081498616184964856697826466318019515810668575831548901480377981565575508877982069484543278075732281240944572`95.], 0, 4]]	,

	TestID -> "TSum Up 06"
]

VerificationTest[(* 143 *)
	Plus[DREAM`ZeroSeries`ToSeriesData[Part[s[mEvaluate, None, "~"], 1]], Times[-1, Sum[Times[Power[2, Plus[-2, \[Epsilon]]], Power[Times[2, Power[3, -1]], k]], List[k, 0, Infinity]]]]
	,
	SeriesData[\[Epsilon], 0, List[], 4, 4, 1]	,

	TestID -> "TSum Up 07"
]

VerificationTest[(* 144 *)
	Chop[Plus[Map[DREAM`ZeroSeries`ToSeriesData, s[mEvaluate, None, "+"]], Times[-1, Table[Sum[Times[Power[2, Plus[-2, \[Epsilon], Times[-1, q]]], Power[Times[2, Power[3, -1]], k]], List[k, q, Infinity]], List[q, 0, 49]]]], Power[10, -100]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[50]]	,

	TestID -> "TSum Up 08"
]

VerificationTest[(* 145 *)
	Chop[Plus[Map[DREAM`ZeroSeries`ToSeriesData, s[mEvaluate, None, "-"]], Times[-1, Table[Sum[Times[Power[2, Plus[-2, \[Epsilon], Times[-1, q]]], Power[Times[2, Power[3, -1]], k]], List[k, q, Infinity]], List[q, 0, -49, -1]]]], Power[10, -100]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], List[50]]	,

	TestID -> "TSum Up 09"
]

VerificationTest[(* 146 *)
	ODelete[h1, h2, s, a]
	,
	Null	,

	TestID -> "TSum Up 10"
]


(* ::Section::Closed:: *)
(*Down*)


VerificationTest[(* 147 *)
	List[ONew[a, TExpr, Power[2, Times[-1, \[Nu]]], \[Nu]], ONew[h1, THomo, 3, \[Nu]], ONew[h2, THomo, Times[1, Power[2, -1]], \[Nu]], ONew[s, TSum, h2, a, h1, "-"]]
	,
	List[a, h1, h2, s]	,

	TestID -> "TSum Down 01"
]

VerificationTest[(* 148 *)
	CompoundExpression[s[mRequest, None, "-", List[50, 130]], s[mRequest, None, "+", List[50, 130]], s[mRequest, None, "~", List[130]], Map[Function[Slot[1][mSendRequests]], List[s, h1, h2, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, h1, h2, a]]]
	,
	List[Null, Null, Null, Null]	,

	TestID -> "TSum Down 02"
]

VerificationTest[(* 149 *)
	s[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[]	,

	TestID -> "TSum Down 03"
]

VerificationTest[(* 150 *)
	s[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[]	,

	TestID -> "TSum Down 04"
]

VerificationTest[(* 151 *)
	CompoundExpression[Map[Function[s[mOrderRequest, None, Slot[1], 3]], List["+", "-", "~"]], Map[Function[Slot[1][mSendOrderRequests]], List[s, h1, h2, a]]]
	,
	List[Null, Null, Null, Null]	,

	TestID -> "TSum Down 05"
]

VerificationTest[(* 152 *)
	Plus[DREAM`ZeroSeries`ToSeriesData[Part[s[mEvaluate, None, "~"], 1]], Times[-1, Times[Power[2, Plus[-2, \[Epsilon]]], Sum[Power[3, k], List[k, Times[-1, Infinity], -1]]]]]
	,
	SeriesData[\[Epsilon], 0, List[], 4, 4, 1]	,

	TestID -> "TSum Down 06"
]

VerificationTest[(* 153 *)
	Plus[Map[DREAM`ZeroSeries`ToSeriesData, s[mEvaluate, None, "+"]], Times[-1, Table[Times[Power[2, Plus[-2, \[Epsilon], Times[-1, n]]], Sum[Power[3, k], List[k, Times[-1, Infinity], Plus[n, -1]]]], List[n, 0, 49]]]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], 50]	,

	TestID -> "TSum Down 07"
]

VerificationTest[(* 154 *)
	Chop[Plus[Map[DREAM`ZeroSeries`ToSeriesData, s[mEvaluate, None, "-"]], Times[-1, Table[Times[Power[2, Plus[-2, \[Epsilon], Times[-1, n]]], Sum[Power[3, k], List[k, Times[-1, Infinity], Plus[n, -1]]]], List[n, 0, -49, -1]]]], Power[10, -110]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], 50]	,

	TestID -> "TSum Down 08"
]

VerificationTest[(* 155 *)
	ODelete[h1, h2, s, a]
	,
	Null	,

	TestID -> "TSum Down 09"
]


(* ::Section::Closed:: *)
(*TUnity*)


VerificationTest[(* 156 *)
	List[ONew[a, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[unity, TUnity], ONew[sum, TSum, unity, a, unity, "+"]]
	,
	List[a, unity, sum]	,

	TestID -> "TSum TUnity 01"
]

VerificationTest[(* 157 *)
	CompoundExpression[sum[mRequest, None, "+", List[50, 100]], sum[mRequest, None, "-", List[50, 100]], Map[Function[Slot[1][mSendRequests]], List[sum, unity, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[sum, unity, a]]]
	,
	List[Null, Null, Null]	,

	TestID -> "TSum TUnity 02"
]

VerificationTest[(* 158 *)
	sum[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[]	,

	TestID -> "TSum TUnity 03"
]

VerificationTest[(* 159 *)
	sum[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[List[0, 0], List[2, -2]]	,

	TestID -> "TSum TUnity 04"
]

VerificationTest[(* 160 *)
	CompoundExpression[sum[mOrderRequest, None, "+", 3], sum[mOrderRequest, None, "-", 3], Map[Function[Slot[1][mSendOrderRequests]], List[sum, unity, a]]]
	,
	List[Null, Null, Null]	,

	TestID -> "TSum TUnity 05"
]

VerificationTest[(* 161 *)
	Chop[Plus[Map[DREAM`ZeroSeries`ToSeriesData, sum[mEvaluate, None, "+"]], Times[-1, Table[Series[Sum[Times[1, Power[Power[Plus[k, Times[-1, \[Epsilon]]], 2], -1]], List[k, q, Infinity]], List[\[Epsilon], 0, 3]], List[q, 2, 51]]]], Power[10, -95]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], 50]	,

	TestID -> "TSum TUnity 06"
]

VerificationTest[(* 162 *)
	Chop[Plus[Map[DREAM`ZeroSeries`ToSeriesData, sum[mEvaluate, None, "-"]], Times[-1, Table[Series[Sum[Times[1, Power[Power[Plus[k, Times[-1, \[Epsilon]]], 2], -1]], List[k, q, Infinity]], List[\[Epsilon], 0, 3]], List[q, 2, -47, -1]]]], Power[10, -95]]
	,
	ConstantArray[SeriesData[\[Epsilon], 0, List[], 4, 4, 1], 50]	,

	TestID -> "TSum TUnity 07"
]

VerificationTest[(* 163 *)
	ODelete[sum, unity, a]
	,
	Null	,

	TestID -> "TSum TUnity 08"
]


(* ::Section::Closed:: *)
(*Geometric (up-1)*)


VerificationTest[(* 164 *)
	List[ONew[a, TExpr, Power[3, Times[-1, \[Nu]]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Geometric Up1 01"
]

VerificationTest[(* 165 *)
	s[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric Up1 02"
]

VerificationTest[(* 166 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	List[Null, Null, Null]	,

	TestID -> "TSum Geometric Up1 03"
]

VerificationTest[(* 167 *)
	DREAM`ZeroSeries`ToSeriesData[Part[s[mEvaluate, None, "~"], 1]]
	,
	N[Series[ReplaceAll[Times[Power[3, Plus[1, Times[-1, \[Nu]]]], Power[2, -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Geometric Up1 04"
]

VerificationTest[(* 168 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Geometric Up1 05"
]


(* ::Section::Closed:: *)
(*Geometric (up-2)*)


VerificationTest[(* 169 *)
	List[ONew[a, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Geometric Up2 01"
]

VerificationTest[(* 170 *)
	s[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric Up2 02"
]

VerificationTest[(* 171 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[Times[1, Power[2, -1]], 2, 0]]	,

	TestID -> "TSum Geometric Up2 03"
]

VerificationTest[(* 172 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	List[Null, Null, Null]	,

	TestID -> "TSum Geometric Up2 04"
]

VerificationTest[(* 173 *)
	DREAM`ZeroSeries`ToSeriesData[Part[s[mEvaluate, None, "~"], 1]]
	,
	N[Series[ReplaceAll[Times[Power[2, Times[-1, \[Nu]]], HurwitzLerchPhi[Times[1, Power[2, -1]], 2, \[Nu]]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Geometric Up2 05"
]

VerificationTest[(* 174 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Geometric Up2 06"
]


(* ::Section::Closed:: *)
(*Geometric (up-3)*)


VerificationTest[(* 175 *)
	List[ONew[a1, TExpr, Power[3, Times[-1, \[Nu]]], \[Nu]], ONew[a2, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"]]
	,
	List[a1, a2, u, s1, s2]	,

	TestID -> "TSum Geometric Up3 01"
]

VerificationTest[(* 176 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric Up3 02"
]

VerificationTest[(* 177 *)
	s2[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric Up3 03"
]

VerificationTest[(* 178 *)
	CompoundExpression[s2[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a1, a2]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a1, a2]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a1, a2]]]
	,
	ConstantArray[Null, 5]	,

	TestID -> "TSum Geometric Up3 04"
]

VerificationTest[(* 179 *)
	DREAM`ZeroSeries`ToSeriesData[Part[s2[mEvaluate, None, "~"], 1]]
	,
	N[Series[Sum[Times[Power[3, Plus[-2, \[Epsilon], Times[-1, k]]], Power[Power[Plus[2, Times[-1, \[Epsilon]], q], 2], -1]], List[q, 0, Infinity], List[k, q, Infinity]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Geometric Up3 05"
]

VerificationTest[(* 180 *)
	ODelete[s2, s1, u, a1, a2]
	,
	Null	,

	TestID -> "TSum Geometric Up3 06"
]


(* ::Section::Closed:: *)
(*Geometric (down)*)


VerificationTest[(* 181 *)
	List[ONew[a, TExpr, Power[3, \[Nu]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "-"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Geometric Down 01"
]

VerificationTest[(* 182 *)
	s[DREAM`Hidden`fType]
	,
	"G(D)"	,

	TestID -> "TSum Geometric Down 02"
]

VerificationTest[(* 183 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[3, 0, 0]]	,

	TestID -> "TSum Geometric Down 03"
]

VerificationTest[(* 184 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]	,

	TestID -> "TSum Geometric Down 04"
]

VerificationTest[(* 185 *)
	ToSD[Part[s[mEvaluate, None, "~"], 1]]
	,
	N[Series[Times[Power[3, Plus[2, Times[-1, \[Epsilon]]]], Power[2, -1]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Geometric Down 05"
]

VerificationTest[(* 186 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Geometric Down 06"
]


(* ::Section::Closed:: *)
(*Factorial 1*)


VerificationTest[(* 187 *)
	List[ONew[a, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Factorial[\[Nu]], -1]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Factorial-1 01"
]

VerificationTest[(* 188 *)
	s[DREAM`Hidden`fType]
	,
	"F"	,

	TestID -> "TSum Factorial-1 02"
]

VerificationTest[(* 189 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[Times[1, Power[2, -1]], 1, 1]]	,

	TestID -> "TSum Factorial-1 03"
]

VerificationTest[(* 190 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]	,

	TestID -> "TSum Factorial-1 04"
]

VerificationTest[(* 191 *)
	ToSD[Part[s[mEvaluate, None, "~"], 1]]
	,
	N[Series[ReplaceAll[Times[Sqrt[E], Plus[Gamma[\[Nu]], Times[-1, Gamma[\[Nu], Times[1, Power[2, -1]]]]], Power[Gamma[\[Nu]], -1]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Factorial-1 05"
]

VerificationTest[(* 192 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Factorial-1 06"
]


(* ::Section::Closed:: *)
(*Factorial-2*)


VerificationTest[(* 193 *)
	List[ONew[a, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Times[Factorial[\[Nu]], Power[\[Nu], 2]], -1]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Factorial-2 01"
]

VerificationTest[(* 194 *)
	s[DREAM`Hidden`fType]
	,
	"F"	,

	TestID -> "TSum Factorial-2 02"
]

VerificationTest[(* 195 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[Times[1, Power[2, -1]], 3, 1]]	,

	TestID -> "TSum Factorial-2 03"
]

VerificationTest[(* 196 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]	,

	TestID -> "TSum Factorial-2 04"
]

VerificationTest[(* 197 *)
	s[mEvaluate, None, "~"]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.033738627932196519334486043451569663383591839698907606829601764598918311127028812709623455661536013728250896123783041499409136342965857`99.39794000867204, 0.088273596713756540862215650243595520381396879382703545513524161374700877328049348003220999938352896155134378266421383473935428993148267`99.39794000867204, 0.117042047461770317492218869578387655708301213380911816856534650068905045007178767050264321985184224630259997766466673310706112547642916`99.39794000867204, 0.106628582325403822310127419101255723710461720215911039750325790932347459428242051308362553400221329914956733363508141205739515479349412`99.39794000867205], 0, 4]]	,

	TestID -> "TSum Factorial-2 05"
]

VerificationTest[(* 198 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Factorial-2 06"
]


(* ::Section::Closed:: *)
(*Factorial-3*)


VerificationTest[(* 199 *)
	List[ONew[a1, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Factorial[\[Nu]], -1]], \[Nu]], ONew[a2, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"]]
	,
	List[a1, a2, u, s1, s2]	,

	TestID -> "TSum Factorial-3 01"
]

VerificationTest[(* 200 *)
	s1[DREAM`Hidden`fType]
	,
	"F"	,

	TestID -> "TSum Factorial-3 02"
]

VerificationTest[(* 201 *)
	s2[DREAM`Hidden`fType]
	,
	"F"	,

	TestID -> "TSum Factorial-3 03"
]

VerificationTest[(* 202 *)
	s1[DREAM`Hidden`fAsym0]
	,
	List[List[Times[1, Power[2, -1]], 1, 1]]	,

	TestID -> "TSum Factorial-3 04"
]

VerificationTest[(* 203 *)
	s2[DREAM`Hidden`fAsym0]
	,
	List[List[Times[1, Power[2, -1]], 3, 1]]	,

	TestID -> "TSum Factorial-3 05"
]

VerificationTest[(* 204 *)
	CompoundExpression[s2[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a1, a2]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a1, a2]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a1, a2]]]
	,
	ConstantArray[Null, 5]	,

	TestID -> "TSum Factorial-3 06"
]

VerificationTest[(* 205 *)
	s2[mEvaluate, None, "~"]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.040008545458639856288961600011804705503134458900076019453864916869118171031828803586337938530924445332439925998068209024461978509220832`99.15490195998571, 0.106940392912863458128005493918883482987930727574703229255625991419230865007545823968170276293734981895659434396029539007857626660539845`99.15490195998572, 0.145521796425577962107629818288304039856653453874039281679489561267996026399104127824325090299306365073209319902612035777945969571254262`99.15490195998571, 0.13670225764126807367513639011028704339167278541796785802802146925847627674463172480057025613889365888557724435636597829866652103387599`99.15490195998572], 0, 4]]	,

	TestID -> "TSum Factorial-3 07"
]

VerificationTest[(* 206 *)
	ODelete[s2, s1, u, a1, a2]
	,
	Null	,

	TestID -> "TSum Factorial-3 08"
]


(* ::Section::Closed:: *)
(*Harmonic*)


VerificationTest[(* 207 *)
	List[ONew[a, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Harmonic 01"
]

VerificationTest[(* 208 *)
	s[DREAM`Hidden`fType]
	,
	"H"	,

	TestID -> "TSum Harmonic 02"
]

VerificationTest[(* 209 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[1, 2, 0]]	,

	TestID -> "TSum Harmonic 03"
]

VerificationTest[(* 210 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]	,

	TestID -> "TSum Harmonic 04"
]

VerificationTest[(* 211 *)
	ToSD[First[s[mEvaluate, None, "~"]]]
	,
	N[Series[PolyGamma[1, Plus[2, Times[-1, \[Epsilon]]]], List[\[Epsilon], 0, 3]], 98]	,

	TestID -> "TSum Harmonic 05"
]

VerificationTest[(* 212 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Harmonic 06"
]


(* ::Section::Closed:: *)
(*Harmonic (alternating)*)


VerificationTest[(* 213 *)
	List[ONew[a, TExpr, Times[Power[-1, \[Nu]], Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[s, TSum, u, a, u, "+"]]
	,
	List[a, u, s]	,

	TestID -> "TSum Harmonic Alternating 01"
]

VerificationTest[(* 214 *)
	s[DREAM`Hidden`fType]
	,
	"H(A)"	,

	TestID -> "TSum Harmonic Alternating 02"
]

VerificationTest[(* 215 *)
	s[DREAM`Hidden`fAsym0]
	,
	List[List[-1, 2, 0]]	,

	TestID -> "TSum Harmonic Alternating 03"
]

VerificationTest[(* 216 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]	,

	TestID -> "TSum Harmonic Alternating 04"
]

VerificationTest[(* 217 *)
	ToSD[First[s[mEvaluate, None, "~"]]]
	,
	N[Series[ReplaceAll[Times[Times[1, Power[4, -1]], Power[-1, \[Nu]], Plus[HurwitzZeta[2, Times[\[Nu], Power[2, -1]]], Times[-1, HurwitzZeta[2, Times[Plus[1, \[Nu]], Power[2, -1]]]]]], Rule[\[Nu], Plus[2, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], 100]	,

	TestID -> "TSum Harmonic Alternating 05"
]

VerificationTest[(* 218 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Harmonic Alternating 06"
]


(* ::Section::Closed:: *)
(*Harmonic-geometric*)


VerificationTest[(* 219 *)
	CompoundExpression[ONew[a1, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[a2, TExpr, Times[Power[2, \[Nu]], Power[\[Nu], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], Null]
	,
	Null	,

	TestID -> "TSum Harmonic-Geometric 01"
]

VerificationTest[(* 220 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Harmonic-Geometric 02"
]

VerificationTest[(* 221 *)
	s2[DREAM`Hidden`fType]
	,
	"HG"	,

	TestID -> "TSum Harmonic-Geometric 03"
]

VerificationTest[(* 222 *)
	CompoundExpression[s2[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a1, a2]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a1, a2]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a1, a2]]]
	,
	ConstantArray[Null, 5]	,

	TestID -> "TSum Harmonic-Geometric 04"
]

VerificationTest[(* 223 *)
	s2[mEvaluate, None, "~"]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.280646201899630851157130245159671990444747305734578335004985774896703892315013278429278504577374992828119397835990899483178312341343899`100., 0.313385479567831160150977166276955593997337982031139079595112730889902704087247827310372007210646984314904066053412945019875777329408717`100., 0.264650522091465277373039880661713284387496506266294035447406191533050998036886010971301434266935199259755425174457098329013185631971735`100., 0.198668717025921271997230406061747556028127584296592055719757642139190489812089066345927480132811167441464151919406899665395631874459891`100.], 0, 4]]	,

	TestID -> "TSum Harmonic-Geometric 05"
]

VerificationTest[(* 224 *)
	ODelete[s2, s1, u, a1, a2]
	,
	Null	,

	TestID -> "TSum Harmonic-Geometric 06"
]


(* ::Section::Closed:: *)
(*Harmonic (fractioness)*)


VerificationTest[(* 225 *)
	CompoundExpression[ONew[a1, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[u, TUnity], ONew[h, THomo, Times[Plus[-7, Times[3, \[Nu]]], Power[Times[3, \[Nu]], -1]], \[Nu]], ONew[s1, TSum, u, u, h, "-"], ONew[s2, TSum, u, a1, s1, "+"], Null]
	,
	Null	,

	TestID -> "TSum Harmonic Fractioness 01"
]

VerificationTest[(* 226 *)
	s1[DREAM`Hidden`fType]
	,
	"H"	,

	TestID -> "TSum Harmonic Fractioness 02"
]

VerificationTest[(* 227 *)
	s2[DREAM`Hidden`fType]
	,
	"H(F)"	,

	TestID -> "TSum Harmonic Fractioness 03"
]

VerificationTest[(* 228 *)
	CompoundExpression[s2[mRequest, None, "~", List[150]], s1[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, h, u, a1]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, h, u, a1]], s2[mOrderRequest, None, "~", 3], s1[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, h, u, a1]]]
	,
	ConstantArray[Null, 5]	,

	TestID -> "TSum Harmonic Fractioness 04"
]

VerificationTest[(* 229 *)
	Chop[s1[mEvaluate, None, "~"], Power[10, -414]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[-0.75`100., 0.75`100., 0, 0], 0, 4]]	,

	TestID -> "TSum Harmonic Fractioness 05"
]

VerificationTest[(* 230 *)
	Chop[Plus[s2[mEvaluate, None, "~"], Times[-1, List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[-0.15036910235007310664324946426693908932601041634245732913502554849437847864296417842242362515850416692818555942772979387193679061385516079155050491411252729588151608587080463`150., 0.12290284896644467674021888463502491612853064777897713727850901702874395523569732713016580091001482737765563526551756235835252806774099500898894348898778498310138029504714285`150., 0.08419331148421158923357526901500999394872759028947272624619828119621282276663522672459408114270623846541065205195306654968704839550209648917396337395353266566131167161481615`150., 0.05540776045106865137401227065643151555355124244459985543717515340410828762652203777863805807721310718820225927964846835077021686606839789346607194877588026271633833411569726`150.], 0, 4]]]], Power[10, -144]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[], 0, 4]]	,

	TestID -> "TSum Harmonic Fractioness 06"
]

VerificationTest[(* 231 *)
	Chop[Plus[s2[mEvaluate, None, "~"], Times[-1, List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[-0.15036910235007310664324946426693908932601041634245732913502554849437847864296417842242362515850416692818555942772979387193679061385516079155050491411252729588151608587080463`150., 0.12290284896644467674021888463502491612853064777897713727850901702874395523569732713016580091001482737765563526551756235835252806774099500898894348898778498310138029504714285`150., 0.08419331148421158923357526901500999394872759028947272624619828119621282276663522672459408114270623846541065205195306654968704839550209648917396337395353266566131167161481615`150., 0.05540776045106865137401227065643151555355124244459985543717515340410828762652203777863805807721310718820225927964846835077021686606839789346607194877588026271633833411569726`150.], 0, 4]]]], Power[10, -144]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[], 0, 4]]	,

	TestID -> "TSum Harmonic Fractioness 07"
]

VerificationTest[(* 232 *)
	ODelete[s2, s1, h, u, a1]
	,
	Null	,

	TestID -> "TSum Harmonic Fractioness 08"
]


(* ::Section::Closed:: *)
(*Harmonic (complex)*)


VerificationTest[(* 233 *)
	ONew[a, TExpr, Times[Power[Exp[Times[I, Times[Pi, Power[7, -1]]]], \[Nu]], Power[Power[\[Nu], 2], -1]], \[Nu]]
	,
	a	,

	TestID -> "TSum Harmonic Complex 01"
]

VerificationTest[(* 234 *)
	ONew[u, TUnity]
	,
	u	,

	TestID -> "TSum Harmonic Complex 02"
]

VerificationTest[(* 235 *)
	ONew[s, TSum, u, a, u, "+"]
	,
	s
	,
	{N::meprec},

	TestID -> "TSum Harmonic Complex 03"
]

VerificationTest[(* 236 *)
	s[DREAM`Hidden`fType]
	,
	"H(C)"	,

	TestID -> "TSum Harmonic Complex 04"
]

VerificationTest[(* 237 *)
	CompoundExpression[s[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s, u, a]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s, u, a]], s[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s, u, a]]]
	,
	ConstantArray[Null, 3]
	,
	{N::meprec, N::meprec},

	TestID -> "TSum Harmonic Complex 05"
]

VerificationTest[(* 238 *)
	Chop[Plus[s[mEvaluate, None, "~"], Times[-1, List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[Complex[0.089348580506206993680963954289651746425081288594684766542334878155980240325007499357660026266852519504328515399511327200585622390070126`99.51477011851456, 0.375740858193359180141832019396617984843065009689250814798435399596174140830894523266570636956813727668942112442277329418248831591129891`100.13857088428345], Complex[0.307021371674422659137374253404744711212436960018856665639563727996421614278913057284806935480930725549574605998572790272400351542176021`100.02800881216886, 0.267297977979682106970853231903698542376987572073614296669476103646698718735344170247664125275070266920357394464049868835831482369223691`99.96783587799396], Complex[0.244501550070088561827830226254256310589102394613149847219860933318256070543202492038356691255528858112830624823348522961645077900244225`100.11832919848203, 0.097730078495832251106598520255208679691894678682345385860409474366064115985052762485611400801381601217309344474485293644673101746644062`99.72007582930199], Complex[0.148226118958772765108394265749980801023642091126475263891546204501511775859470249878440420474572233365373545691580796921756336365561114`100.13727428854094, 0.037166973549554168504524951415415944058649747236477074474513704656022318369860775339996030622822348796622298638230352264607621106613187`99.53650674961573]], 0, 4]]]], Power[10, -96]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0, 0, 0, 0], 0, 4]]
	,
	{N::meprec, N::meprec},

	TestID -> "TSum Harmonic Complex 06"
]

VerificationTest[(* 239 *)
	ODelete[s, u, a]
	,
	Null	,

	TestID -> "TSum Harmonic Complex 07"
]


(* ::Section::Closed:: *)
(*Harmonic (1 to 1)*)


VerificationTest[(* 240 *)
	CompoundExpression[ONew[a1, TExpr, Times[1, Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[a2, TExpr, Times[1, Power[\[Nu], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], Null]
	,
	Null	,

	TestID -> "TSum Harmonic 1-to-1 01"
]

VerificationTest[(* 241 *)
	s1[DREAM`Hidden`fType]
	,
	"H"	,

	TestID -> "TSum Harmonic 1-to-1 02"
]

VerificationTest[(* 242 *)
	s2[DREAM`Hidden`fType]
	,
	"H(1)"	,

	TestID -> "TSum Harmonic 1-to-1 03"
]

VerificationTest[(* 243 *)
	CompoundExpression[s2[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a1, a2]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a1, a2]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a1, a2]]]
	,
	ConstantArray[Null, 5]	,

	TestID -> "TSum Harmonic 1-to-1 04"
]

VerificationTest[(* 244 *)
	s2[mEvaluate, None, "~"]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.759179739470962134327061156376874792311022683474199325848984881313668941169425306539282846598911811056741285186265244647873380665786658`100., 0.55082587010492230667112422063103841604376905976679315633254757558382873681879054366032530491379542638310928959410201817339284057571077`100., 0.384389429285001131199693037706384617322921182276677750184852019139589047434418778905351902922453982818102915168252274463065971788369529`100., 0.259421411902520438889709679078204265558896077276867250443254159274339944044236706833388727506684799721970255839932599465995734739533049`100.], 0, 4]]	,

	TestID -> "TSum Harmonic 1-to-1 05"
]

VerificationTest[(* 245 *)
	ODelete[s2, s1, u, a1, a2]
	,
	Null	,

	TestID -> "TSum Harmonic 1-to-1 06"
]


(* ::Section::Closed:: *)
(*Harmonic parent of HG*)


VerificationTest[(* 246 *)
	CompoundExpression[ONew[a1, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[Power[\[Nu], 2], -1]], \[Nu]], ONew[a2, TExpr, Times[Power[2, \[Nu]], Power[\[Nu], -1]], \[Nu]], ONew[a3, TExpr, Times[1, Power[Power[\[Nu], 3], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], ONew[s3, TSum, u, a3, s2, "+"]]
	,
	s3	,

	TestID -> "TSum Harmonic(HG) 01"
]

VerificationTest[(* 247 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Harmonic(HG) 02"
]

VerificationTest[(* 248 *)
	s2[DREAM`Hidden`fType]
	,
	"HG"	,

	TestID -> "TSum Harmonic(HG) 03"
]

VerificationTest[(* 249 *)
	s3[DREAM`Hidden`fType]
	,
	"H(HG)"	,

	TestID -> "TSum Harmonic(HG) 04"
]

VerificationTest[(* 250 *)
	CompoundExpression[Map[Function[Slot[1][mClear]], List[s3, s2, s1, u, a1, a2, a3]], s3[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s3, s2, s1, u, a1, a2, a3]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s3, s2, s1, u, a1, a2, a3]], s3[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s3, s2, s1, u, a1, a2, a3]], s3[mEvaluate, None, "~"]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.040960827043379349040699023036250481319469267663170926890695489941796347757033971025615870300455444323480074051592954874545307739688312`100., 0.10091824875853659566184607562393067995424407005566507254157656841345769610047342728556737585596122759239497051134894656151675332098608`100., 0.153259470838978737113396865016512495503115230005342465271380072815630345535858863226729625010745309968698802351759819176864074089715232`100., 0.183811793701509712323096804951196911754817689596316025394697154590705669995834380338738654587738610151222661339809372214399816722884519`100.], 0, 4]]	,

	TestID -> "TSum Harmonic(HG) 05"
]

VerificationTest[(* 251 *)
	ODelete[u, s1, s2, s3, a1, a2, a3]
	,
	Null	,

	TestID -> "TSum Harmonic(HG) 06"
]


(* ::Section::Closed:: *)
(*Geometric (up) parent of HG*)


VerificationTest[(* 252 *)
	CompoundExpression[ONew[a1, TExpr, Times[Power[Times[-1, Times[3125, Power[19683, -1]]], \[Nu]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[a2, TExpr, Times[Power[Times[-1, Times[19683, Power[3125, -1]]], \[Nu]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[a3, TExpr, Times[Power[Times[12500, Power[19683, -1]], \[Nu]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], ONew[s3, TSum, u, a3, s2, "+"], Null]
	,
	Null	,

	TestID -> "TSum Geometric(U,HG) 01"
]

VerificationTest[(* 253 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric(U,HG) 02"
]

VerificationTest[(* 254 *)
	s2[DREAM`Hidden`fType]
	,
	"HG"	,

	TestID -> "TSum Geometric(U,HG) 03"
]

VerificationTest[(* 255 *)
	s3[DREAM`Hidden`fType]
	,
	"G(U,HG)"	,

	TestID -> "TSum Geometric(U,HG) 04"
]

VerificationTest[(* 256 *)
	CompoundExpression[Map[Function[Slot[1][mClear]], List[s3, s2, s1, u, a3, a2, a1]], s3[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s3, s2, s1, u, a3, a2, a1]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s3, s2, s1, u, a3, a2, a1]], s3[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s3, s2, s1, u, a3, a2, a1]]]
	,
	List[Null, Null, Null, Null, Null, Null, Null]	,

	TestID -> "TSum Geometric(U,HG) 05"
]

VerificationTest[(* 257 *)
	ToSD[Part[s3[mEvaluate, None, "~"], 1]]
	,
	SeriesData[\[Epsilon], 0, List[0.163094775119008159667787826385112773221980111411229875380526589904968894187733188495284915257736603579866718390367052706287805424681003`99.69897000433602, Complex[0.236186306684309222682452100418335764141119322356920436881893751574740890921712834514799129906746411081407029270151892526394849840564457`99.5019105751176, -1.024754694705510861306299065058926091975692784917059572641180446210166260648882474145038364311980911244885985378664452939929249438249866`99.69897000433602], Complex[-3.00180851310467758423782612126379754357813689671736304075065188642341847570728818054028486608581999364957958708243285216606458340458585`99.6756421055187, -1.484002331915863462118404565893054837849128630413885768680391246366154389121866057127329058426558435241706710600384944667096218500219903`99.69897000433602], Complex[-4.494325238514914562196645390647356684475246084008476940703216902050130022035895039912079819501516229952859643476956945450414898029397261`99.69042348725557, 5.375687884635921952374840039613995219035538992686826295062553823710879422979927780401516204111343278953881448863393495736667100196144487`99.62210617808432]], 0, 4, 1]	,

	TestID -> "TSum Geometric(U,HG) 06"
]

VerificationTest[(* 258 *)
	ODelete[u, s1, s2, s3, a3, a2, a1]
	,
	Null	,

	TestID -> "TSum Geometric(U,HG) 07"
]


(* ::Section::Closed:: *)
(*Geometric (down) parent of HG*)


VerificationTest[(* 259 *)
	CompoundExpression[ONew[a1, TExpr, Times[Power[Times[-1, Times[3125, Power[19683, -1]]], \[Nu]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[a2, TExpr, Times[Power[Times[-1, Times[19683, Power[3125, -1]]], \[Nu]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[a3, TExpr, Times[Power[Times[12500, Power[19683, -1]], Times[-1, \[Nu]]], Times[1, Power[\[Nu], -1]]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], ONew[s3, TSum, u, a3, s2, "-"], Null]
	,
	Null	,

	TestID -> "TSum Geometric(D,HG) 01"
]

VerificationTest[(* 260 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric(D,HG) 02"
]

VerificationTest[(* 261 *)
	s2[DREAM`Hidden`fType]
	,
	"HG"	,

	TestID -> "TSum Geometric(D,HG) 03"
]

VerificationTest[(* 262 *)
	s3[DREAM`Hidden`fType]
	,
	"G(D,HG)"	,

	TestID -> "TSum Geometric(D,HG) 04"
]

VerificationTest[(* 263 *)
	CompoundExpression[Map[Function[Slot[1][mClear]], List[s3, s2, s1, u, a3, a2, a1]], s3[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s3, s2, s1, u, a3, a2, a1]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s3, s2, s1, u, a3, a2, a1]], s3[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s3, s2, s1, u, a3, a2, a1]]]
	,
	List[Null, Null, Null, Null, Null, Null, Null]	,

	TestID -> "TSum Geometric(D,HG) 05"
]

VerificationTest[(* 264 *)
	ToSD[Part[s3[mEvaluate, None, "~"], 1]]
	,
	SeriesData[\[Epsilon], 0, List[-1.`99.15490195998572, Complex[-0.701367553795111584692569502804848373385452085926866365351088569067635768347937969124300740560371723828343603661966210852844677871220742`99.04483432034161, 6.283185307179586476925286766559005768394338798750211641949889184615632812572417997256069650684234135964296173026564613294187689219101164`99.15490195998572], Complex[19.294116427914379152762161705795104853501012301803072120657338737242886961337072463725535261582446117430279373103576385704756829380727873`99.16712986855167, 4.406822308937933325381609443641911021557534073112045437170447519809008052326138675996105032716096140342075961195544239008411775672682739`99.04483432034162], Complex[10.481247537722029200515075556713821345505063612953673369684764814361578127481943337366593436238916616025750025751176157692818475312520612`99.00355514420397, -38.545104374084393673400924701443118372760303577566779808226386923497632204580104824790400828103448727029684730395776659326208528485053081`99.19458247645605], Complex[-49.213601512685830706179728955158834852690700007180156317766361237823113849155663777848150244235975620174870339990693194313931598653765507`99.1745137258482, -7.864163389777144523307130313277742648516715082203990858429892031790084001014592274184275870086186843214842757467989267771517558013571597`98.51697309591236], Complex[15.782045017397555298523993002943986641931362514592481870842313672770711238534694838520862360442431560409277004031586012006081204600142782`98.51649177365911, 19.598933646216134876201536467925710198767825245466988667328811189886210287589744297086429290930168800084458427467693165332819196279071653`98.68884445836709], Complex[-59.450086122416447724476436887057126719726440464560972328446720262313952297068197050675681037579836708971427132342535589556135050982109108`98.95200673038514, -50.022358009796710307451545127046752913435249789711571170820639321410240115490809045036570448703339271454470633368106522183474862729559358`98.80681759259257]], -3, 4, 1]	,

	TestID -> "TSum Geometric(D,HG) 06"
]

VerificationTest[(* 265 *)
	ODelete[List[s3, s2, s1, u, a3, a2, a1]]
	,
	Null	,

	TestID -> "TSum Geometric(D,HG) 07"
]


(* ::Section::Closed:: *)
(*Geometric parent of harmonic*)


VerificationTest[(* 266 *)
	CompoundExpression[ONew[a1, TExpr, Times[1, Power[Power[\[Nu], 3], -1]], \[Nu]], ONew[a2, TExpr, Times[Power[2, Times[-1, \[Nu]]], Power[\[Nu], -1]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "+"], Null]
	,
	Null	,

	TestID -> "TSum Geometric(H) 01"
]

VerificationTest[(* 267 *)
	s1[DREAM`Hidden`fType]
	,
	"H"	,

	TestID -> "TSum Geometric(H) 02"
]

VerificationTest[(* 268 *)
	s2[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Geometric(H) 03"
]

VerificationTest[(* 269 *)
	CompoundExpression[Map[Function[Slot[1][mClear]], List[s2, s1, u, a2, a1]], s2[mRequest, None, "~", List[100]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a2, a1]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a2, a1]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a2, a1]], s2[mEvaluate, None, "~"]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[0.029308106538005216360852652967587147792489491005258828288956288067683795429401082305371398329338069736486710677041647829494696540733571`99.39794000867204, 0.067997901736656093708597990527235863991382177351858342638261491495369781145586563593103847685045515624059001912872388804031362381253368`99.39794000867204, 0.092464762459024498392386823375955372036269136791851630574451425471240211689613561471419595729245034139272831818041661829498244639155547`99.39794000867204, 0.097508400312766174604680361319684856921676876625358640928899170790453042439152329811146152565777258053191476269131278632161042673378413`99.39794000867204], 0, 4]]	,

	TestID -> "TSum Geometric(H) 04"
]

VerificationTest[(* 270 *)
	ODelete[u, s1, s2, a2, a1]
	,
	Null	,

	TestID -> "TSum Geometric(H) 05"
]


(* ::Section::Closed:: *)
(*Obtuse geometric*)


VerificationTest[(* 271 *)
	CompoundExpression[ONew[a1, TExpr, Power[Times[-1, Times[16, Power[27, -1]]], \[Nu]], \[Nu]], ONew[a2, TExpr, Power[Times[-1, Times[3125, Power[1024, -1]]], \[Nu]], \[Nu]], ONew[u, TUnity], ONew[s1, TSum, u, a1, u, "+"], ONew[s2, TSum, u, a2, s1, "-"]]
	,
	s2	,

	TestID -> "TSum Obtuse Geometric 01"
]

VerificationTest[(* 272 *)
	s1[DREAM`Hidden`fType]
	,
	"G(U)"	,

	TestID -> "TSum Obtuse Geometric 02"
]

VerificationTest[(* 273 *)
	s2[DREAM`Hidden`fType]
	,
	"G(D)"	,

	TestID -> "TSum Obtuse Geometric 03"
]

VerificationTest[(* 274 *)
	CompoundExpression[Map[Function[Slot[1][mClear]], List[s2, s1, u, a2, a1]], s2[mRequest, None, "~", List[60]], Map[Function[Slot[1][mSendRequests]], List[s2, s1, u, a2, a1]], Map[Function[Slot[1][mSetDim, Plus[2, Times[-1, \[Epsilon]]]]], List[s2, s1, u, a2, a1]], s2[mOrderRequest, None, "~", 3], Map[Function[Slot[1][mSendOrderRequests]], List[s2, s1, u, a2, a1]], s2[mEvaluate, None, "~"]]
	,
	List[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[Complex[2.540125695010903763879409365584058863677981055750695010903762006314582876576939180687814786117819`58.82470229940586, 0``59.416187113431725], Complex[-1.50494728700295425537896871528334434387868046054561938954074623608654490923659937173578212756534`58.22052728224636, -15.96008044528184595851250592519627573698622381003963943862180828781390918766956`58.82797529028685], Complex[-49.69425370938714613166776250562245387000671792815322068844754841982117614446489`58.81858809261736, 9.45586268177674242422139091402640014934166964657575732510865142680120584399315`58.31203977054621], Complex[29.61842424091867179672080465094559695357490230060768891948839025637617623638121`58.365648297183434, 102.21196448541742409276784612958498629547264872068035047569291975745462904836457`58.80528823723491]], 0, 4]]	,

	TestID -> "TSum Obtuse Geometric 04"
]

VerificationTest[(* 275 *)
	ODelete[s2, s1, u, a2, a1]
	,
	Null	,

	TestID -> "TSum Obtuse Geometric 05"
]


(* ::Section::Closed:: *)
(*mLowestOrder (up)*)


VerificationTest[
	ONew[expr1, TExpr, 1/((-9+k) (-5+k) (-3+k) (-2+k)^3 k (4+k)), k];
	ONew[expr2, TExpr, 1/((-9+k)^3 (-3+k) k (4+k)), k];
	ONew[expr3, TExpr, 1/((-9+k) (-5+k) (-3+k)^3 k), k];
	ONew[sum, TSum, expr1, expr2, expr3, "+"],

	sum,

	TestID -> "TSum mLowestOrder Up 01"
]


VerificationTest[
	types={"+","-"};
	l={sum,expr1,expr2,expr3};
	#[mClear]& /@ l;
	sum[mRequest, None, #1, {15, 100}]& /@ types;
	#[mSendRequests]& /@ l;
	#[mSetDim, 0-\[Epsilon]]& /@ l;
	sum[mOrderRequest, None, #, 3] & /@ types,

	{Null, Null},

	TestID -> "TSum mLowestOrder Up 02"
]


VerificationTest[
	sum[DREAM`Hidden`mLowestOrder, "+"],

	{{0,-4},{1,-3},{2,-6},{3,-4},{4,-3},{5,-4},{6,-3},{9,-4},{10,0}},

	TestID -> "TSum mLowestOrder Up 03"
]


VerificationTest[
	sum[DREAM`Hidden`mLowestOrder, "-"],

	{{0,-4},{1,-3},{4,-4},{5,-3}},

	TestID -> "TSum mLowestOrder Up 04"
]


VerificationTest[
	#[mSendOrderRequests]& /@ l;,

	Null,

	TestID -> "TSum mLowestOrder Up 05"
]


VerificationTest[
	sum[mEvaluate, None, "+"] - (<<"Data/TSum mLowestOrder Up 06"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Up 06"
]


VerificationTest[
	sum[mEvaluate, None, "-"] - (<<"Data/TSum mLowestOrder Up 07"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Up 07"
]


VerificationTest[
	ODelete[sum, expr1, expr2, expr3],

	Null,

	TestID -> "TSum mLowestOrder Up 08"
]


(* ::Section::Closed:: *)
(*mLowestOrder (down)*)


VerificationTest[
	ONew[expr1, TExpr, 1/((-9+k) (-5+k) (-3+k) (-2+k)^3 k (4+k)), k];
	ONew[expr2, TExpr, 1/((-9+k) (-5+k) (-3+k)^3 k), k];
	ONew[expr3, TExpr, 1/((-9+k)^3 (-3+k) k (4+k)), k];
	ONew[sum, TSum, expr1, expr2, expr3, "-"],

	sum,

	TestID -> "TSum mLowestOrder Down 01"
]


VerificationTest[
	types={"+", "-"};
	l={sum, expr1, expr2, expr3};
	#[mClear]& /@ l;
	sum[mRequest, None, #, {15, 100}]& /@ types;
	#[mSendRequests]& /@ l;
	#[mSetDim, -\[Epsilon]]& /@ l;
	sum[mOrderRequest, None, #, 3]& /@ types;,
	
	Null,

	TestID -> "TSum mLowestOrder Down 02"
]


VerificationTest[
	sum[DREAM`Hidden`mLowestOrder, "+"],

	{{0,-1},{2,-4},{3,-2},{4,-3},{5,-4},{6,-3},{9,-4},{10,-3}},

	TestID -> "TSum mLowestOrder Down 03"
]


VerificationTest[
	sum[DREAM`Hidden`mLowestOrder, "-"],

	{{0,-1},{1,0},{4,-1},{5,0}},

	TestID -> "TSum mLowestOrder Down 04"
]


VerificationTest[
	#[mSendOrderRequests] & /@ l;,

	Null,

	TestID -> "TSum mLowestOrder Down 05"
]


VerificationTest[
	sum[mEvaluate, None, "+"] - (<<"Data/TSum mLowestOrder Down 06"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Down 06"
]


VerificationTest[
	sum[mEvaluate, None, "-"] - (<<"Data/TSum mLowestOrder Down 07"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Down 07"
]


VerificationTest[
	ODelete[sum, expr1, expr2, expr3],

	Null,

	TestID -> "TSum mLowestOrder Down 08"
]


(* ::Section::Closed:: *)
(*mLowestOrder (double)*)


VerificationTest[
	ONew[expr1, TExpr, 1/((-9+k) (-3+k) (-2+k)^3 k), k];
	ONew[expr2, TExpr, 1/((-9+k)^3 k (4+k)), k];
	ONew[expr3, TExpr, 2^-k/((-5+k) (-3+k)^3 k), k];
	ONew[sum1, TSum, expr1, expr2, expr3, "+"];
	ONew[unity, TUnity];
	ONew[sum2, TSum, unity, unity, sum1, "+"],

	sum2,

	TestID -> "TSum mLowestOrder Double 01"
]


VerificationTest[
	types = {"+", "-"};
	l = {sum2, unity, sum1, expr1, expr2, expr3};
	#[mClear]& /@ l;
	sum2[mRequest, None, #, {15, 100}]& /@ types;
	#[mSendRequests]& /@ l;
	#[mSetDim, -\[Epsilon]]& /@ l;
	sum2[mOrderRequest, None, #1, 3]& /@ types,

	{Null, Null},

	TestID -> "TSum mLowestOrder Double 02"
]


VerificationTest[
	sum2[DREAM`Hidden`mLowestOrder, "+"],

	{{0,-5},{3,-3},{10,0}},

	TestID -> "TSum mLowestOrder Double 03"
]


VerificationTest[
	sum2[DREAM`Hidden`mLowestOrder, "-"],

	{{0,-5}},

	TestID -> "TSum mLowestOrder Double 04"
]


VerificationTest[
	#[mSendOrderRequests]& /@ l;,

	Null,

	TestID -> "TSum mLowestOrder Double 05"
]


VerificationTest[
	sum2[mEvaluate, None, "+"] - (<<"Data/TSum mLowestOrder Double 06"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Double 06"
]


VerificationTest[
	sum2[mEvaluate, None, "-"] - (<<"Data/TSum mLowestOrder Double 07"),

	ConstantArray[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], {}, 4, 4], 15],

	TestID -> "TSum mLowestOrder Double 07"
]


VerificationTest[
	ODelete[sum2, unity, sum1, expr1, expr2, expr3],

	Null,

	TestID -> "TSum mLowestOrder Double 08"
]


(* ::Section::Closed:: *)
(*fParametersFunction*)


VerificationTest[(* 300 *)
	ONew[u, TUnity],
	u,

	TestID -> "TSum fParametersFunction 01"
]

VerificationTest[(* 301 *)
	ONew[e, TExpr, 1/\[Nu]^3, \[Nu]],
	e,

	TestID -> "TSum fParametersFunction 02"
]

VerificationTest[(* 302 *)
	ONew[s0, TSum, u, e, u, "+"],
	s0,

	TestID -> "TSum fParametersFunction 03"
]

VerificationTest[(* 303 *)
	ONew[s1, TSum, u, u, e, "+", Null, Function[{n,p},{1.63p,1.63p,1.1p}]],
	s1,

	TestID -> "TSum fParametersFunction 04"
]

VerificationTest[(* 304 *)
	ONew[s2, TSum, u, u, e, "+", Null, Function[{n,p},{1.63p,1.1p,1.1p}]],
	s2,

	TestID -> "TSum fParametersFunction 05"
]

VerificationTest[(* 305 *)
	ONew[m, TMasters],
	m,

	TestID -> "TSum fParametersFunction 06"
]

VerificationTest[(* 306 *)
	m[mAddMasterByObjects, S0, {s0}];,
	Null,
	{S0::NoTHomoInstance},

	TestID -> "TSum fParametersFunction 07"
]

VerificationTest[(* 307 *)
	m[mAddMasterByObjects, S1, {s1}];,
	Null,
	{S1::NoTHomoInstance},

	TestID -> "TSum fParametersFunction 08"
]

VerificationTest[(* 308 *)
	m[mAddMasterByObjects, S2, {s2}];,
	Null,
	{S2::NoTHomoInstance},

	TestID -> "TSum fParametersFunction 09"
]

VerificationTest[(* 309 *)
	Normal[m[mEvaluate, S0, Plus[1, Times[-1, \[Epsilon]]], 0, 50][[1,2]] + Zeta[3]],
	0,

	TestID -> "TSum fParametersFunction 10"
]

VerificationTest[(* 310 *)
	Normal[m[mEvaluate, S1, Plus[1, Times[-1, \[Epsilon]]], 0, 50][[1,2]] + Zeta[3]],
	2.52283747307238823936666736109476913921715`14.32196424632638*^-36,

	TestID -> "TSum fParametersFunction 11"
]

VerificationTest[(* 311 *)
	Chop[Normal[m[mEvaluate, S2, Plus[1, Times[-1, \[Epsilon]]], 0, 50][[1,2]] + Zeta[3]], 10^-49],
	0,

	TestID -> "TSum fParametersFunction 12"
]

VerificationTest[(* 312 *)
	m[mPurge];,
	Null,

	TestID -> "TSum fParametersFunction 13"
]
