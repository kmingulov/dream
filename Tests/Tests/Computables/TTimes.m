(* ::Package:: *)

(* ::Section::Closed:: *)
(*Simple*)


VerificationTest[(* 313 *)
	ONew[expr1, TExpr, Times[Power[2, \[Nu]], Power[\[Nu], -1]], \[Nu]]
	,
	expr1	,

	TestID -> "TTimes Simple 01"
]

VerificationTest[(* 314 *)
	ONew[expr2, TExpr, Power[7, Times[-1, \[Nu]]], \[Nu]]
	,
	expr2	,

	TestID -> "TTimes Simple 02"
]

VerificationTest[(* 315 *)
	ONew[expr3, TExpr, Power[\[Nu], -3], \[Nu]]
	,
	expr3	,

	TestID -> "TTimes Simple 03"
]

VerificationTest[(* 316 *)
	ONew[times, TTimes, List[expr1, expr2, expr3]]
	,
	times	,

	TestID -> "TTimes Simple 04"
]

VerificationTest[(* 317 *)
	times[mAsymptotics, "+"]
	,
	List[List[Times[2, Power[7, -1]], 4, 0]]	,

	TestID -> "TTimes Simple 05"
]

VerificationTest[(* 318 *)
	times[mAsymptotics, "-"]
	,
	List[List[Times[2, Power[7, -1]], 4, 0]]	,

	TestID -> "TTimes Simple 06"
]

VerificationTest[(* 319 *)
	CompoundExpression[ONew[J, TMasters, List[]], J[mAddMasterByObjects, J1, List[times]]]
	,
	Null
	,
	{J1::NoTHomoInstance},

	TestID -> "TTimes Simple 07"
]

VerificationTest[(* 320 *)
	Plus[Part[J[mRangeEvaluate, J1, Plus[2, Times[-1, \[Epsilon]]], 3, 10, 50], All, 1, 2], Times[-1, Table[Series[ReplaceAll[Times[Times[1, Power[Power[\[Nu], 4], -1]], Power[Times[2, Power[7, -1]], \[Nu]]], Rule[\[Nu], Plus[2, k, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], List[k, 0, 9]]]]
	,
	ConstantArray[DREAM`ZeroSeries`ToSeriesData[DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 3]]], 10]	,

	TestID -> "TTimes Simple 08"
]

VerificationTest[(* 321 *)
	Plus[Part[J[mRangeEvaluate, J1, Plus[2, Times[-1, \[Epsilon]]], 3, -10, 50], All, 1, 2], Times[-1, Table[Series[ReplaceAll[Times[Times[1, Power[Power[\[Nu], 4], -1]], Power[Times[2, Power[7, -1]], \[Nu]]], Rule[\[Nu], Plus[2, k, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 0, 3]], List[k, 0, -9, -1]]]]
	,
	ConstantArray[DREAM`ZeroSeries`ToSeriesData[DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 3]]], 10]	,

	TestID -> "TTimes Simple 09"
]

VerificationTest[(* 322 *)
	J[mPurge],
	{"expr1", "expr2", "expr3", "times", "J1", "J"},

	TestID -> "TTimes Simple 10"
]
