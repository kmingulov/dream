(* ::Package:: *)

(* ::Section:: *)
(*mLowestOrder*)


VerificationTest[(* 80 *)
	ONew[expr, TExpr, Times[1, Power[Times[k, Plus[k, -3], Plus[k, -5], Power[Plus[k, -2], 3], Plus[k, 4], Plus[k, -9]], -1]], k],
	expr,

	TestID -> "TExpr mLowestOrder 01"
]

VerificationTest[(* 81 *)
	CompoundExpression[expr[mClear], Map[Function[expr[mRequest, None, Slot[1], List[15, 20]]], List["+", "-"]], expr[mSendRequests], expr[mSetDim, Plus[0, Times[-1, \[Epsilon]]]], Map[Function[expr[mOrderRequest, None, Slot[1], 3]], List["+", "-"]]]
	,
	List[Null, Null],

	TestID -> "TExpr mLowestOrder 02"
]

VerificationTest[(* 82 *)
	expr[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[List[0, -1], List[1, 0], List[2, -3], List[3, -1], List[4, 0], List[5, -1], List[6, 0], List[9, -1], List[10, 0]],

	TestID -> "TExpr mLowestOrder 03"
]

VerificationTest[(* 83 *)
	expr[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[List[0, -1], List[1, 0], List[4, -1], List[5, 0]],

	TestID -> "TExpr mLowestOrder 04"
]

VerificationTest[(* 84 *)
	expr[mSendOrderRequests]
	,
	Null,

	TestID -> "TExpr mLowestOrder 05"
]

VerificationTest[(* 85 *)
	expr[mEvaluate, None, "+"]
	,
	Table[DREAM`ZeroSeries`ZeroNSeries[ReplaceAll[Times[1, Power[Times[k, Plus[k, -3], Plus[k, -5], Power[Plus[k, -2], 3], Plus[k, 4], Plus[k, -9]], -1]], Rule[k, Plus[0, Times[-1, \[Epsilon]], q]]], List[\[Epsilon], 3], 20], List[q, 0, 14]],

	TestID -> "TExpr mLowestOrder 06"
]

VerificationTest[(* 86 *)
	expr[mEvaluate, None, "-"]
	,
	Table[DREAM`ZeroSeries`ZeroNSeries[ReplaceAll[Times[1, Power[Times[k, Plus[k, -3], Plus[k, -5], Power[Plus[k, -2], 3], Plus[k, 4], Plus[k, -9]], -1]], Rule[k, Plus[0, Times[-1, \[Epsilon]], Times[-1, q]]]], List[\[Epsilon], 3], 20], List[q, 0, 14]],

	TestID -> "TExpr mLowestOrder 07"
]

VerificationTest[(* 87 *)
	ODelete[expr]
	,
	Null,

	TestID -> "TExpr mLowestOrder 08"
]
