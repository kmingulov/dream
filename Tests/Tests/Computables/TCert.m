(* ::Package:: *)

(* ::Section:: *)
(*Simple*)


VerificationTest[
	ONew[cert, TCert, 2, \[Nu]]
	,
	cert,

	TestID -> "TCert Simple 01"
]

VerificationTest[
	cert[mAsymptotics, "+"]
	,
	List[List[2, 0, 0]],

	TestID -> "TCert Simple 02"
]

VerificationTest[
	cert[mAsymptotics, "-"]
	,
	List[List[2, 0, 0]],

	TestID -> "TCert Simple 03"]

VerificationTest[
	CompoundExpression[cert[mRequest, None, "+", List[50, 100]], cert[mRequest, None, "-", List[50, 100]], cert[mSendRequests], cert[mSetDim, Plus[2, Times[-1, \[Epsilon]]]]]
	,
	Null,

	TestID -> "TCert Simple 04"
]

VerificationTest[
	cert[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[],

	TestID -> "TCert Simple 05"
]

VerificationTest[
	cert[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[],

	TestID -> "TCert Simple 06"
]

VerificationTest[
	CompoundExpression[cert[mOrderRequest, None, "+", 3], cert[mOrderRequest, None, "-", 3], cert[mSendOrderRequests]]
	,
	Null,

	TestID -> "TCert Simple 07"
]

VerificationTest[
	Plus[cert[mEvaluate, None, "+"], Times[-1, Table[DREAM`ZeroSeries`ZeroNSeries[Power[2, k], List[\[Epsilon], 3], 100], List[k, 0, 49]]]]
	,
	Table[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[], 4, 4], List[k, 0, 49]],

	TestID -> "TCert Simple 08"
]

VerificationTest[
	Plus[cert[mEvaluate, None, "-"], Times[-1, Table[DREAM`ZeroSeries`ZeroNSeries[Power[2, k], List[\[Epsilon], 3], 100], List[k, 0, -49, -1]]]]
	,
	Table[DREAM`ZeroSeries`ZeroSeriesData[\[Epsilon], List[], 4, 4], List[k, 0, 49]],

	TestID -> "TCert Simple 09"
]

VerificationTest[
	ODelete[cert]
	,
	Null,

	TestID -> "TCert Simple 10"
]


(* ::Section:: *)
(*Matrix*)


VerificationTest[
	ONew[cert, TCert, List[List[0, 1], List[Times[-1, Times[4, x, Plus[1, Times[4, x]], Power[Times[3, Plus[-1, x], Plus[-5, Times[4, x]]], -1]]], Times[-1, Times[Plus[-9, Times[28, x], Times[-1, Times[28, Power[x, 2]]]], Power[Times[3, Plus[-1, x], Plus[-5, Times[4, x]]], -1]]]]], x]
	,
	cert,

	TestID -> "TCert Matrix 01"
]

VerificationTest[
	cert[mAsymptotics, "+"]
	,
	List[List[Times[4, Power[3, -1]], Times[-1, Times[5, Power[4, -1]]], 0]],

	TestID -> "TCert Matrix 02"
]

VerificationTest[
	cert[mAsymptotics, "-"]
	,
	List[List[1, Times[-1, Times[5, Power[4, -1]]], 0]],

	TestID -> "TCert Matrix 03"
]

VerificationTest[
	CompoundExpression[cert[mClear], cert[mRequest, None, "+", List[10, 100]], cert[mSendRequests], cert[mSetDim, Plus[2, Times[-1, \[Epsilon]]]], cert[mOrderRequest, None, "+", 3], cert[mSendOrderRequests]]
	,
	Null,

	TestID -> "TCert Matrix 04"
]

VerificationTest[
	Normal[ReplaceAll[Plus[cert[mEvaluate, None, "+"], Times[-1, FoldList[Function[Dot[Slot[2], Slot[1]]], IdentityMatrix[2], Table[DREAM`ZeroSeries`ZeroNSeries[ReplaceAll[List[List[0, 1], List[Times[-1, Times[4, x, Plus[1, Times[4, x]], Power[Times[3, Plus[-1, x], Plus[-5, Times[4, x]]], -1]]], Times[-1, Times[Plus[-9, Times[28, x], Times[-1, Times[28, Power[x, 2]]]], Power[Times[3, Plus[-1, x], Plus[-5, Times[4, x]]], -1]]]]], Rule[x, Plus[2, k, Times[-1, \[Epsilon]]]]], List[\[Epsilon], 3], 100], List[k, 0, 8]]]]], RuleDelayed[Pattern[zsd, Blank[DREAM`ZeroSeries`ZeroSeriesData]], DREAM`ZeroSeries`ToSeriesData[zsd]]]]
	,
	List[List[List[0, 0], List[0, 0]], List[List[0, 0``100.], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]], List[List[0, 0], List[0, 0]]],

	TestID -> "TCert Matrix 05"
]

VerificationTest[
	ODelete[cert]
	,
	Null,

	TestID -> "TCert Matrix 06"
]


(* ::Section:: *)
(*mLowestOrder*)


VerificationTest[
	ONew[cert, TCert, Times[k, Plus[k, 1], Power[Times[Plus[k, 3], Plus[k, -3], Plus[k, 4]], -1]], k]
	,
	cert,

	TestID -> "TCert mLowestOrder 01"
]

VerificationTest[
	CompoundExpression[cert[mClear], Map[Function[cert[mRequest, None, Slot[1], List[10, 20]]], List["+", "-"]], cert[mSendRequests], cert[mSetDim, Plus[0, Times[-1, \[Epsilon]]]], Map[Function[cert[mOrderRequest, None, Slot[1], 3]], List["+", "-"]]]
	,
	List[Null, Null],

	TestID -> "TCert mLowestOrder 02"
]

VerificationTest[(* 74 *)
	cert[DREAM`Hidden`mLowestOrder, "+"]
	,
	List[List[1, 1], List[4, 0]],

	TestID -> "TCert mLowestOrder 03"
]

VerificationTest[(* 75 *)
	cert[DREAM`Hidden`mLowestOrder, "-"]
	,
	List[List[1, -1], List[3, 0], List[4, 1]],

	TestID -> "TCert mLowestOrder 04"
]

VerificationTest[(* 76 *)
	cert[mSendOrderRequests]
	,
	Null,

	TestID -> "TCert mLowestOrder 05"
]

VerificationTest[(* 77 *)
	Plus[cert[mEvaluate, None, "+"], Times[-1, FoldList[Times, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 20], Table[DREAM`ZeroSeries`ZeroNSeries[ReplaceAll[Times[k, Plus[k, 1], Power[Times[Plus[k, 3], Plus[k, -3], Plus[k, 4]], -1]], Rule[k, Plus[0, Times[-1, \[Epsilon]], q]]], List[\[Epsilon], 4], 20], List[q, 0, 8]]]]]
	,
	Table[DREAM`ZeroSeries`ZeroSeries[0, List[\[Epsilon], 3]], 10],

	TestID -> "TCert mLowestOrder 06"
]

VerificationTest[(* 78 *)
	Map[Function[Normal[DREAM`ZeroSeries`ToSeriesData[Slot[1]]]], Plus[cert[mEvaluate, None, "-"], Times[-1, FoldList[Times, DREAM`ZeroSeries`ZeroNSeries[1, List[\[Epsilon], 3], 20], Table[DREAM`ZeroSeries`ZeroNSeries[ReplaceAll[Power[Times[k, Plus[k, 1], Power[Times[Plus[k, 3], Plus[k, -3], Plus[k, 4]], -1]], -1], Rule[k, Plus[-1, Times[-1, \[Epsilon]], Times[-1, q]]]], List[\[Epsilon], 4], 20], List[q, 0, 8]]]]]]
	,
	Table[0, 10],

	TestID -> "TCert mLowestOrder 07"
]

VerificationTest[(* 79 *)
	ODelete[cert]
	,
	Null,

	TestID -> "TCert mLowestOrder 08"
]
